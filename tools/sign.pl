#!/usr/bin/perl

use strict;

sub execute($) {

  my $cmd = shift;
  print($cmd . "\n");
  open(CMD, "$cmd |") or die $!;
  while(<CMD>) { print; }
  close(CMD);
}

my $packageFile = "MoneyTracker.apk";
my $unalignedFile = "MoneyTracker-release-unaligned.apk";
my $unsignedFile = "MoneyTracker-release-unsigned.apk";

execute("jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 -keystore tools/jtb-release-key.keystore bin/$unsignedFile release");
execute("jarsigner -verify -verbose bin/$unsignedFile");
execute("mv bin/$unsignedFile bin/$unalignedFile");
execute("zipalign -v 4 bin/$unalignedFile bin/$packageFile");
execute("rm -f bin/$unalignedFile");

