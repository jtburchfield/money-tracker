#!/usr/bin/perl

use strict;
use Cwd;
#use Getopt::Long;

my $dir = ".";
my $workspace = "..";
my $output = "${workspace}/moneytrackerfree";
my $version = "";
my $home = getcwd;

open(FILE, "AndroidManifest.xml");
my @data = <FILE>;
close(FILE);

my $isMoneyTracker = 0;
foreach (@data) {
	chomp($_);
    if(m/package="org\.jtb\.moneytracker"/) {
	    $isMoneyTracker = 1;
    }
    if(m/android:versionName=/) {
    	s/ >//;
    	s/"//g;
    	$version = (split("=", $_))[1];
    }
}

if(!$isMoneyTracker) {
    print "Error: Must run this script from within Money Tracker home directory.\n";
    exit;
}

sub execute($) {

    my $cmd = shift;
    print($cmd . "\n");
    open(CMD, "$cmd |") or die $!;
    while(<CMD>) { print; }
    close(CMD);
}

sub build {
	if( -e "build.xml" ) {
		execute("ant clean");
		execute("rm -f build.xml");
	}
	
	execute("android update project -p .");
	execute("find . -name build.xml -exec perl -pi -e 's/MainActivity/MoneyTracker/g' {} \\;");
	execute("ant release");

	execute("tools/sign.pl");	
}

#-----------------------------------------------------------
# First Build Paid Version
#-----------------------------------------------------------
build();

execute("cp bin/MoneyTracker.apk ${home}/release/MoneyTracker-${version}.apk");

#-----------------------------------------------------------
# Build Free Version
#-----------------------------------------------------------

my @filesToCopy = qw(AndroidManifest.xml .classpath libs .project project.properties res src tools);

execute("rm -rf $output");
execute("mkdir -p $output");

foreach (@filesToCopy) {
    execute("cp -rf $_ $output");
}

execute("find $output -type f -exec perl -pi -e 's/org\.jtb\.moneytracker/org\.jtb\.moneytrackerfree/g' {} \\;");
execute("find $output -type f -exec perl -pi -e 's/PAID_APP = false/PAID_APP = false/g' {} \\;");
execute("find $output -type f -exec perl -pi -e 's/android:label=\"Money Tracker\"/android:label=\"Money Tracker Free\"/g' {} \\;");
execute("find $output -name \"strings\.xml\" -exec perl -pi -e 's/moneytrackerfree/moneytracker/' {} \\;");
execute("find $output -name \"styles\.xml\" -exec perl -pi -e 's/gone/visible/' {} \\;");
execute("mv $output/src/org/jtb/moneytracker $output/src/org/jtb/moneytrackerfreefree");

chdir($output) or die "unable to chdir to $output";

build();

execute("cp bin/MoneyTracker.apk ${home}/release/MoneyTrackerFree-${version}.apk");

execute("rm -rf $output");





