ECLIPSE ANDROID PROJECT IMPORT SUMMARY
======================================

Manifest Merging:
-----------------
Your project uses libraries that provide manifests, and your Eclipse
project did not explicitly turn on manifest merging. In Android Gradle
projects, manifests are always merged (meaning that contents from your
libraries' manifests will be merged into the app manifest. If you had
manually copied contents from library manifests into your app manifest
you may need to remove these for the app to build correctly.

Ignored Files:
--------------
The following files were *not* copied into the new Gradle project; you
should evaluate whether these are still needed in your project and if
so manually move them:

* .gitignore
* .metadata/
* .metadata/.plugins/
* .metadata/.plugins/org.eclipse.core.resources/
* .metadata/.plugins/org.eclipse.core.resources/.root/
* .metadata/.plugins/org.eclipse.core.resources/.root/.indexes/
* .metadata/.plugins/org.eclipse.core.resources/.root/.indexes/properties.version
* .metadata/.plugins/org.eclipse.core.runtime/
* .metadata/.plugins/org.eclipse.core.runtime/.settings/
* .metadata/.plugins/org.eclipse.core.runtime/.settings/org.eclipse.core.resources.prefs
* README
* backlog.txt
* build.xml
* gimp/
* gimp/down.xcf
* gimp/ic_home.xcf
* gimp/up.xcf
* proguard-project.txt
* release/
* release/MoneyTracker-2.0.apk
* release/MoneyTracker-2.1.apk
* release/MoneyTracker-2.2.1.apk
* release/MoneyTracker-2.2.2.apk
* release/MoneyTracker-2.2.3.apk
* release/MoneyTracker-2.2.4.apk
* release/MoneyTracker-2.2.apk
* release/MoneyTrackerFree-2.0.apk
* release/MoneyTrackerFree-2.1.apk
* release/MoneyTrackerFree-2.2.1.apk
* release/MoneyTrackerFree-2.2.2.apk
* release/MoneyTrackerFree-2.2.3.apk
* release/MoneyTrackerFree-2.2.4.apk
* release/MoneyTrackerFree-2.2.apk
* release/images/
* release/images/MoneyTracker1.png
* release/images/MoneyTracker2.png
* release/images/MoneyTracker3.png
* release/images/MoneyTracker4.png
* release/images/MoneyTracker5.png
* release/images/MoneyTracker6.png
* release/images/MoneyTrackerFeatureGraphic.png
* release/images/MoneyTrackerFeatureGraphic_640.100.png
* release/images/Screenshot_2013-12-15-15-08-20.png
* release/images/Screenshot_2013-12-15-15-13-18.png
* release/images/Screenshot_2013-12-15-15-13-28.png
* release/images/Screenshot_2013-12-15-15-13-36.png
* release/images/Screenshot_2013-12-15-15-13-56.png
* release/images/Screenshot_2013-12-15-15-14-52.png
* release/images/Screenshot_2013-12-15-15-15-04.png
* test/
* test/.classpath
* test/.gitignore
* test/.project
* test/proguard-project.txt
* test/project.properties
* tools/
* tools/build.pl
* tools/jtb-release-key.keystore
* tools/sign.pl

Replaced Jars with Dependencies:
--------------------------------
The importer recognized the following .jar files as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the .jar file in your project was of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the jar replacement in the import wizard and try again:

android-support-v4.jar => com.android.support:support-v4:19.1.0
android-support-v7-appcompat.jar => com.android.support:appcompat-v7:19.1.0

Replaced Libraries with Dependencies:
-------------------------------------
The importer recognized the following library projects as third party
libraries and replaced them with Gradle dependencies instead. This has
the advantage that more explicit version information is known, and the
libraries can be updated automatically. However, it is possible that
the source files in your project were of an older version than the
dependency we picked, which could render the project not compileable.
You can disable the library replacement in the import wizard and try
again:

android-support-v7-appcompat => [com.android.support:appcompat-v7:19.1.0]
appcompat-v7 => [com.android.support:appcompat-v7:18.0.0]
google-play-services_lib => [com.google.android.gms:play-services:+]

Moved Files:
------------
Android Gradle projects use a different directory structure than ADT
Eclipse projects. Here's how the projects were restructured:

* AndroidManifest.xml => app/src/main/AndroidManifest.xml
* libs/mobclix.jar => app/libs/mobclix.jar
* lint.xml => app/lint.xml
* res/ => app/src/main/res/
* src/ => app/src/main/java/
* src/org/jtb/moneytracker/data/budget/budget_notes.txt => app/src/main/resources/org/jtb/moneytracker/data/budget/budget_notes.txt
* test/res/ => app/src/androidTest/res/
* test/src/ => app/src/androidTest/java/

Next Steps:
-----------
You can now build the project. The Gradle project needs network
connectivity to download dependencies.

Bugs:
-----
If for some reason your project does not build, and you determine that
it is due to a bug or limitation of the Eclipse to Gradle importer,
please file a bug at http://b.android.com with category
Component-Tools.

(This import summary is for your information only, and can be deleted
after import once you are satisfied with the results.)
