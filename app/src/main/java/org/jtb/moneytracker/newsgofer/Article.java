package org.jtb.moneytracker.newsgofer;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

public class Article {

    private String mTitle = "";
    private String mUpdated = "";
    private Bitmap mImage = null;
    private String mSource = "";
    private String mUrl = "";

    public static Article create(JSONObject jsonObject) {

	Article article = new Article();
	InputStream imageStream = null;

	if (jsonObject != null) {
	    try {
		imageStream = new URL(jsonObject.getString("image")).openStream();
		article.mImage = BitmapFactory.decodeStream(imageStream);
		article.mTitle = jsonObject.getString("title");
		article.mUrl = jsonObject.getString("url");
		article.mUpdated = jsonObject.getString("updated");
		article.mSource = jsonObject.getString("source");

	    } catch (MalformedURLException e) {
		e.printStackTrace();
	    } catch (IOException e) {
		e.printStackTrace();
	    } catch (JSONException e) {
		e.printStackTrace();
	    }
	}

	return article;
    }

    public static Article[] create(JSONArray jsonArray) {

	Article[] articles = new Article[jsonArray.length()];

	for (int x = 0; x < jsonArray.length(); ++x) {

	    try {
		articles[x] = create(jsonArray.getJSONObject(x));

	    } catch (JSONException e) {
		e.printStackTrace();
	    }
	}

	return articles;
    }

    private Article() {
	mTitle = "";
	mUpdated = "";
	mImage = null;
    }

    public String getTitle() {
	return mTitle;
    }

    public String getUpdated() {
	return mUpdated;
    }

    public Bitmap getImage() {
	return mImage;
    }

    public String getSource() {
	return mSource;
    }
    
    public String getUrl() {
	return mUrl;
    }
}
