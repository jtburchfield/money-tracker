/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

package org.jtb.moneytracker.widget;

import android.appwidget.AppWidgetManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

/**
 * 
 * @author jason
 */
public class MoneyTrackerBroadcastReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d("MoneyTrackerBroadcastReceiver", "intent=" + intent);

        AppWidgetManager gm = AppWidgetManager.getInstance(context);
        MoneyTrackerAppWidgetProvider.updateAppWidget(context, gm);

        // For our example, we'll also update all of the widgets when the
        // timezone
        // changes, or the user or network sets the time.
        // String action = intent.getAction();
        // if (action.equals(AppWidgetManager.ACTION_APPWIDGET_UPDATE)) {
        // AppWidgetManager gm = AppWidgetManager.getInstance(context);
        // ArrayList<Integer> appWidgetIds = new ArrayList<Integer>();
        // ArrayList<String> texts = new ArrayList<String>();

        // MoneyTrackerAppWidgetConfigure.loadAllTitlePrefs(context,
        // appWidgetIds, texts);

        // final int N = appWidgetIds.size();
        // for (int i=0; i<N; i++) {
        // MoneyTrackerAppWidgetProvider.updateAppWidget(context, gm,
        // appWidgetIds);
        // }
        // }
    }
}
