/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

package org.jtb.moneytracker.widget;

import org.jtb.moneytracker.R;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;

/**
 * 
 * @author jason
 */
@SuppressLint("Registered")
public class MoneyTrackerAppWidgetConfigure extends Activity {

    @Override
    protected void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setResult(RESULT_CANCELED);
        setContentView(R.layout.appwidget_configure);
    }
}
