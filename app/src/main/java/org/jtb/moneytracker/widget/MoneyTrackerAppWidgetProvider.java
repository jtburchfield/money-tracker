/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

package org.jtb.moneytracker.widget;

import java.text.NumberFormat;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.ui.MainActivity;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.RemoteViews;

/**
 * 
 * @author jason
 */
public class MoneyTrackerAppWidgetProvider extends AppWidgetProvider {

    // log tag
    private static final String TAG = "MoneyTrackerWidgetProvider";

    public static final String URI_SCHEME = "moneytracker_widget";

    private static PendingIntent pendingIntent;

    // private static PendingIntent withdrawalIntent;

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // final int N = appWidgetIds.length;

        // Global.setWidgetPreferences(context, appWidgetIds);

        // Perform this loop procedure for each App Widget that belongs to this
        // provider
        // for (int i = 0; i < N; i++) {
        // int appWidgetId = appWidgetIds[i];
        //
        // // Create an Intent to launch MainActivity
        Intent intent = new Intent(context, MainActivity.class);
        pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);

        // Tell the AppWidgetManager to perform an update on the current App
        // Widget
        updateAppWidget(context, appWidgetManager);
        // }
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // super.onReceive(context, intent);

        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        int[] ids = intent.getIntArrayExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS);

        this.onUpdate(context, manager, ids);
        Log.d("MoneyTrackerBroadcastReceiver", "intent=" + intent);
    }

    @Override
    public void onEnabled(Context context) {
        Log.d(TAG, "onEnabled");
        // When the first widget is created, register for the TIMEZONE_CHANGED
        // and TIME_CHANGED
        // broadcasts. We don't want to be listening for these if nobody has our
        // widget active.
        // This setting is sticky across reboots, but that doesn't matter,
        // because this will
        // be called after boot if there is a widget instance for this provider.
    }

    @Override
    public void onDisabled(Context context) {
        // When the first widget is created, stop listening for the
        // TIMEZONE_CHANGED and
        // TIME_CHANGED broadcasts.
        Log.d(TAG, "onDisabled");
    }

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager) {

        // Restore preferences
        // SharedPreferences settings =
        // context.getSharedPreferences(Global.PREFS_FILE, 0);
        // String name = settings.getString(Global.PREF_SELECTED_ACCOUNT,
        // Global.DEFAULT_ACCOUNT);
        //
        // DatabaseManager db = new DatabaseManager(context);
        // db.open();
        // Account acct = db.getAccount(name);

        String name = "";
        String balance = "";
        Account acct = MoneyTrackerController.getWidgetAccount(context);

        if (acct != null) {
            name = acct.getName();
            balance = NumberFormat.getCurrencyInstance().format(acct.getBalance());
        }
        // db.close();

        // Intent withdrawal = new Intent(context, TransactionActivity.class);
        // withdrawal.putExtra(Transaction.TRANSACTION_TYPE_STRING,
        // Transaction.TRANSACTION_CREDIT);
        // withdrawal.putExtra(Account.ACCOUNT_STRING, name);
        // withdrawalIntent = PendingIntent.getActivity(context, 0, withdrawal,
        // 0);

        // Get the layout for the App Widget and attach an on-click listener to
        // the button
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.appwidget_provider);
        views.setTextViewText(R.id.widgetAccountText, name);
        views.setTextViewText(R.id.widgetBalanceText, balance);
        views.setOnClickPendingIntent(R.id.widgetImage, pendingIntent);
        views.setOnClickPendingIntent(R.id.widgetAccountText, pendingIntent);
        views.setOnClickPendingIntent(R.id.widgetBalanceText, pendingIntent);
        // views.setOnClickPendingIntent(R.id.widgetWithdrawalButton,
        // withdrawalIntent);

        ComponentName moneyWidget = new ComponentName(context, MoneyTrackerAppWidgetProvider.class);
        appWidgetManager.updateAppWidget(moneyWidget, views);
    }
}
