/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.jtb.moneytracker.ui.schedule;

import java.util.ArrayList;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.ui.GenericDialog;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;

/**
 * 
 * @author jason
 */
public class ScheduleWeeklyView extends ScheduleView {

	private Button mWeeksButton;
	private CheckBox mSundayCheckBox;
	private CheckBox mMondayCheckBox;
	private CheckBox mTuesdayCheckBox;
	private CheckBox mWednesdayCheckBox;
	private CheckBox mThursdayCheckBox;
	private CheckBox mFridayCheckBox;
	private CheckBox mSaturdayCheckBox;

	public ScheduleWeeklyView(Context context, AttributeSet attrs) {
		super(context, attrs);

		inflate(context, R.layout.schedule_weekly, this);

		// Set the months spinner.
		mWeeksButton = (Button) findViewById(R.id.weeks);
		mWeeksButton.setText("1");

		ArrayAdapter<String> monthsAdapter = new ArrayAdapter<String>(context,
				android.R.layout.simple_dropdown_item_1line);

		for (Integer x = 1; x <= 12; x++) {
			monthsAdapter.add(x.toString());
		}

		mWeeksButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				displayWeeksDialog();
			}
		});

		// Set the sunday check box.
		mSundayCheckBox = (CheckBox) findViewById(R.id.sunday);

		// Set the monday check box.
		mMondayCheckBox = (CheckBox) findViewById(R.id.monday);

		// Set the tuesday check box.
		mTuesdayCheckBox = (CheckBox) findViewById(R.id.tuesday);

		// Set the wednesday check box.
		mWednesdayCheckBox = (CheckBox) findViewById(R.id.wednesday);

		// Set the thursday check box.
		mThursdayCheckBox = (CheckBox) findViewById(R.id.thursday);

		// Set the friday check box.
		mFridayCheckBox = (CheckBox) findViewById(R.id.friday);

		// Set the saturday check box.
		mSaturdayCheckBox = (CheckBox) findViewById(R.id.saturday);
	}

	@Override
	public String getData() {

		checkSelected();

		String data = mWeeksButton.getText().toString();
		data += SEPARATOR + ((mSundayCheckBox.isChecked()) ? "1" : "0");
		data += SEPARATOR + ((mMondayCheckBox.isChecked()) ? "1" : "0");
		data += SEPARATOR + ((mTuesdayCheckBox.isChecked()) ? "1" : "0");
		data += SEPARATOR + ((mWednesdayCheckBox.isChecked()) ? "1" : "0");
		data += SEPARATOR + ((mThursdayCheckBox.isChecked()) ? "1" : "0");
		data += SEPARATOR + ((mFridayCheckBox.isChecked()) ? "1" : "0");
		data += SEPARATOR + ((mSaturdayCheckBox.isChecked()) ? "1" : "0");

		return data;
	}

	@Override
	public void setData(String data) {

		if (!data.equals("")) {
			try {

				String[] myData = data.split(SEPARATOR);

				mWeeksButton.setText(myData[0]);
				mSundayCheckBox.setChecked((myData[1].equals("1")) ? true
						: false);
				mMondayCheckBox.setChecked((myData[2].equals("1")) ? true
						: false);
				mTuesdayCheckBox.setChecked((myData[3].equals("1")) ? true
						: false);
				mWednesdayCheckBox.setChecked((myData[4].equals("1")) ? true
						: false);
				mThursdayCheckBox.setChecked((myData[5].equals("1")) ? true
						: false);
				mFridayCheckBox.setChecked((myData[6].equals("1")) ? true
						: false);
				mSaturdayCheckBox.setChecked((myData[7].equals("1")) ? true
						: false);
			} catch (Exception e) {
				Log.e(ScheduleWeeklyView.this.toString(),
						"[ScheduleWeeklyView - setData]: " + e.getMessage());
			}
		}
	}

	private void checkSelected() {
		if (!mSundayCheckBox.isChecked() && !mMondayCheckBox.isChecked()
				&& !mTuesdayCheckBox.isChecked()
				&& !mWednesdayCheckBox.isChecked()
				&& !mThursdayCheckBox.isChecked()
				&& !mFridayCheckBox.isChecked()
				&& !mSaturdayCheckBox.isChecked()) {

			mSundayCheckBox.setChecked(true);
		}
	}

	private void displayWeeksDialog() {

		ArrayList<String> data = new ArrayList<String>();
		for (Integer x = 1; x <= 12; x++) {
			data.add(x.toString());
		}

		GenericDialog dialog = new GenericDialog(getContext());
		dialog.setTitle("Number of weeks");
		dialog.setData(data);
		dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

			@Override
			public void onClick(String text, String tag) {
				mWeeksButton.setText(text);
			}
		});
		dialog.display();
	}
}
