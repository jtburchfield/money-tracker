package org.jtb.moneytracker.ui.account;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.controller.MoneyTrackerEvents;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.NewestFirstTransactionSort;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.account.TransactionSort;
import org.jtb.moneytracker.ui.GenericDialog;
import org.jtb.moneytracker.ui.GenericDialog.onGenericDialogClickListener;

import java.text.NumberFormat;
import java.util.ArrayList;

public class TransactionListFragment extends Fragment {

    private static final int MENU_DELETE_ALL = 0;
    private static final int MENU_SORT = 1;

    private static final String SORT_NEWEST_FIRST = "Newest First";
    private static final String SORT_OLDEST_FIRST = "Oldest First";

    private MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private FragmentActivity mActivity;
    private ArrayList<Transaction> mTransactions;
    private int mSelectedItem = 0;
    private TransactionsListAdapter mTransactionsAdapter;
    private Transaction mSelectedTransaction;
    private ListView mTransactionsListView;
    private Account mMainAccount;
    private ArrayList<Account> mSubAccounts;
    private TransactionSort mTransactionSort;
    private TextView mSubAccountNumber;
    private TextView mSubAccountBalance;
    private LinearLayout mSubAccountLayout;

    private Handler mHandler = new Handler() {

        public void handleMessage(Message message) {

            switch (message.what) {

                case MoneyTrackerEvents.TRANSACTION_CREATED:
                case MoneyTrackerEvents.TRANSACTION_DELETED:
                case MoneyTrackerEvents.TRANSACTION_UPDATED:
                    updateTransactions();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        // setContentView(R.layout.transaction_list_view);
        mController.register(mHandler);
        createTransactionSort(mController.getTransactionSort());
        mMainAccount = mController.getAccount(getArguments().getInt("Account"));

        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.transaction_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mActivity.setTitle("Transactions - " + mMainAccount.getName());

        mController.showAd(view);

        mTransactionsListView = (ListView) view.findViewById(R.id.transactionList);
        mSubAccountLayout = (LinearLayout) view.findViewById(R.id.subAccountLayout);
        mSubAccountNumber = (TextView) view.findViewById(R.id.subAccountNumber);
        mSubAccountBalance = (TextView) view.findViewById(R.id.subAccountBalance);

        updateTransactions();

        mTransactionsListView.setOnItemLongClickListener(new OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                transactionLongClicked(position);
                return true;
            }
        });

        mTransactionsListView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                transactionClicked(position);
            }
        });

        mSubAccountLayout.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showSubAccountsDialog();
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();

        menu.add(0, MENU_DELETE_ALL, 0, "Delete All Transactions");
        menu.getItem(0).setIcon(android.R.drawable.ic_menu_delete);

        menu.add(1, MENU_SORT, 1, "Sort By");
        menu.getItem(1).setIcon(android.R.drawable.ic_menu_rotate);

        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case MENU_DELETE_ALL:
                showDeleteAllDialog();
                return true;

            case MENU_SORT:
                createSortTransactionsDialog();
                return true;
        }
        return false;
    }

    private void transactionLongClicked(int position) {

        mSelectedItem = position;

        if (mSelectedItem >= mTransactionsListView.getAdapter().getCount()) {
            mSelectedItem = 0;
        }

        showDeleteTransactionDialog();
    }

    private void transactionClicked(int position) {

        mSelectedItem = position;

        if (mSelectedItem >= mTransactionsListView.getAdapter().getCount()) {
            mSelectedItem = 0;
        }

        editTransaction();
    }

    private void createTransactionSort(final String text) {

        if (text.equals(SORT_NEWEST_FIRST)) {
            mTransactionSort = new NewestFirstTransactionSort();
        } else {
            mTransactionSort = new TransactionSort();
        }
    }

    private void showSubAccountsDialog() {

        AccountListDialog dialog = new AccountListDialog(mActivity);
        dialog.setTitle("Select Sub Account");
        dialog.addAll(mSubAccounts);
        dialog.addAccountSelectedListener(new AccountSelectedListener() {

            @Override
            public void accountSelected(Account account) {
                mController.showTransactionListFragment(mActivity, account);
            }
        });
        dialog.show();
    }

    private void showDeleteTransactionDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Delete?");
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setMessage("Delete Transaction?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                mController.deleteTransaction(mTransactions.get(mSelectedItem));
                updateTransactions();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }

    // @Override
    // protected void onActivityResult(int requestCode, int resultCode, Intent
    // data) {
    // super.onActivityResult(requestCode, resultCode, data);
    //
    // if (resultCode == RESULT_OK) {
    //
    // mController.deleteTransaction(mSelectedTransaction);
    // updateTransactions();
    // }
    // }

    private void createSortTransactionsDialog() {

        ArrayList<String> data = new ArrayList<String>();
        data.add(SORT_NEWEST_FIRST);
        data.add(SORT_OLDEST_FIRST);

        GenericDialog dialog = new GenericDialog(mActivity);
        dialog.setTitle("Sort By");
        dialog.setData(data);
        dialog.setClickListener(new onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                createTransactionSort(text);
                mController.setTransactionSort(text);
                updateTransactions();
            }
        });
        dialog.display();
    }

    private void editTransaction() {

        mSelectedTransaction = mTransactions.get(mSelectedItem);
        Account creditAccount = mController.getAccount(mSelectedTransaction.getCreditAccountId());

        // Assume the main account is the debit account.
        int transactionType = Transaction.TRANSACTION_TYPE_DEBIT;

        // Check to see if the main account is the credit account.
        if (mMainAccount.getId() == creditAccount.getId()) {
            transactionType = Transaction.TRANSACTION_TYPE_CREDIT;
        }

        // Start the new edit transaction activity.
        mController.showTransactionFragment(mActivity, mMainAccount, mSelectedTransaction, transactionType);
    }

    /**
     * Gets the transactions from the controller and populates the list view.
     */
    private void updateTransactions() {
        try {

            int id = mMainAccount.getId();
            mTransactions = mController.getTransactions(id);
            mTransactionsAdapter = new TransactionsListAdapter(mActivity, id, mTransactions, mTransactionSort);
            mTransactionsListView.setAdapter(mTransactionsAdapter);

            mSubAccounts = mMainAccount.getSubAccounts();

            if (mSubAccounts.size() > 0) {
                mSubAccountLayout.setVisibility(View.VISIBLE);
            } else {
                mSubAccountLayout.setVisibility(View.GONE);
            }

            mSubAccountNumber.setText(Integer.toString(mSubAccounts.size()));

            double balance = 0;
            for (Account a : mSubAccounts) {
                balance += a.getBalance();
            }
            mSubAccountBalance.setText(NumberFormat.getCurrencyInstance().format(balance));
        } catch (Exception ex) {
            Log.e("OOPS", ex.getMessage());
        }
    }

    private void showDeleteAllDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Delete?");
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setMessage("Delete All Transactions?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                deleteAllTransactions();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
        ;
    }

    /**
     * Deletes all transactions dealing with this account.
     */
    private void deleteAllTransactions() {

        for (int x = 0; x < mTransactions.size(); x++) {
            mController.deleteTransaction(mTransactions.get(x));
        }

        updateTransactions();
    }
}
