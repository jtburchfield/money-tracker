/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.ui.account;

import java.util.ArrayList;
import java.util.List;

import org.jtb.moneytracker.R;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ScrollView;

/**
 * 
 * @author jason
 */
public class AccountTypeDialog {

    private Context mContext;
    private Dialog mAccountTypeListDialog;
    private AccountTypeSelectedListener mAccountTypeChangeListener;
    private List<String> mAccountTypeList = new ArrayList<String>();

    public AccountTypeDialog(final Context context, final AccountTypeSelectedListener accountTypeChangeListener) {

	mContext = context;
	mAccountTypeChangeListener = accountTypeChangeListener;
    }

    public AccountTypeDialog(final Context activity) {

	mContext = activity;
    }

    public void show() {

	createAccountTypeListDialog();
	mAccountTypeListDialog.show();
    }

    public void setAccountTypeSelectedListener(final AccountTypeSelectedListener listener) {

	mAccountTypeChangeListener = listener;
    }

    public void addAccountClass(String accountClass) {

	mAccountTypeList.add(accountClass);
    }

    public void addAccountClass(List<String> accountClass) {

	for (String x : accountClass) {
	    addAccountClass(x);
	}
    }

    private void createAccountTypeListDialog() {

	AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
	builder.setTitle("Select Account Type");
	builder.setCancelable(true);

	AccountTypesListView view = new AccountTypesListView(mContext);
	view.addAccountClass(mAccountTypeList);
	view.setOnAccountClickListener(new SelectAccountClickListener(view));

	ScrollView scrollView = new ScrollView(mContext);
	scrollView.addView(view);

	builder.setView(scrollView);
	builder.setIcon(R.drawable.money_tracker_main);

	mAccountTypeListDialog = builder.create();
    }

    private class SelectAccountClickListener implements OnClickListener {

	private final AccountTypesListView mAccountTypeListView;

	SelectAccountClickListener(AccountTypesListView view) {

	    mAccountTypeListView = view;
	}

	public void onClick(View v) {

	    String type = mAccountTypeListView.getClassTypeName(v);
	    mAccountTypeChangeListener.accountTypeChanged(type);
	    AccountTypeDialog.this.mAccountTypeListDialog.dismiss();
	}
    }
}
