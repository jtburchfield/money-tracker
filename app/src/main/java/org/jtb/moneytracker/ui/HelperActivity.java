package org.jtb.moneytracker.ui;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager.OnBackStackChangedListener;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import org.jtb.moneytracker.R;


public class HelperActivity extends AppCompatActivity implements OnBackStackChangedListener {

    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.helper_activity);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        try {
            getSupportFragmentManager().addOnBackStackChangedListener(this);

            Bundle bundle = getIntent().getExtras();
            Class fragmentClass = (Class) bundle.getSerializable("fragment");

            Fragment fragment = (Fragment) fragmentClass.newInstance();
            fragment.setArguments(bundle.getBundle("bundle"));

            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment, fragment)
                    .addToBackStack(null)
                    .commit();

        } catch (Exception e) {

        }
    }

    @Override
    public void onBackStackChanged() {
        int count = getSupportFragmentManager().getBackStackEntryCount();
        if (count == 0) {
            finish();
        }
    }
}
