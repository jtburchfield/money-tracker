package org.jtb.moneytracker.ui.schedule;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.schedule.DailyScheduledTransaction;
import org.jtb.moneytracker.data.schedule.MonthlyScheduledTransaction;
import org.jtb.moneytracker.data.schedule.ScheduledTransaction;
import org.jtb.moneytracker.data.schedule.SemiMonthlyScheduledTransaction;
import org.jtb.moneytracker.data.schedule.WeeklyScheduledTransaction;
import org.jtb.moneytracker.ui.GenericDialog;
import org.jtb.moneytracker.ui.GenericDialog.onGenericDialogClickListener;
import org.jtb.moneytracker.ui.account.AccountSelectDialog;
import org.jtb.moneytracker.ui.account.AccountSelectedListener;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class ScheduleFragment extends Fragment {

    private static final int SCHEDULE_FREQEUNCY_DAILY = 0;
    private static final int SCHEDULE_FREQEUNCY_WEEKLY = 1;
    private static final int SCHEDULE_FREQEUNCY_SEMI_MONTHLY = 2;
    private static final int SCHEDULE_FREQEUNCY_MONTHLY = 3;

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private Button mMainAccountButton;
    private Button mSecondaryAccountButton;
    private Button mTransactionTypeButton;
    private Button mFrequencyTypeButton;
    private Button mDateButton;
    private EditText mAmountText;
    private EditText mDescriptionText;
    private Button mCreateButton;
    private CheckBox mCreateCheckBox;

    private LinearLayout mScheduleDaysView;
    private ScheduleView mTempDaysView;

    private Integer mCreateDays;
    private Integer mTransactionType;
    private Integer mScheduleFrequency;
    private Calendar mCalendar = Calendar.getInstance();

    private ScheduledTransaction mUpdateTransaction;
    private Account mMainAccount;
    private Account mSecondaryAccount;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.schedule_transaction, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        getActivity().setTitle("Schedule Transaction");

        mController.showAd(view);
        mMainAccount = mController.getSelectedAccount();
        mTransactionType = Transaction.TRANSACTION_TYPE_DEBIT;
        mScheduleFrequency = SCHEDULE_FREQEUNCY_DAILY;
        mCreateDays = 0;

        Bundle data = getArguments();

        if (data != null) {
            mUpdateTransaction = mController.getScheduledTransaction(data.getLong("Transaction"));
        }

        mMainAccountButton = (Button) getView().findViewById(R.id.scheduleMainAccount);
        mSecondaryAccountButton = (Button) getView().findViewById(R.id.scheduleAccount);
        mDateButton = (Button) getView().findViewById(R.id.scheduleDateButton);
        mAmountText = (EditText) getView().findViewById(R.id.scheduleAmountText);
        mDescriptionText = (EditText) getView().findViewById(R.id.scheduleDescriptionText);
        mTransactionTypeButton = (Button) getView().findViewById(R.id.transactionType);
        mFrequencyTypeButton = (Button) getView().findViewById(R.id.scheduleFrequency);
        mScheduleDaysView = (LinearLayout) getView().findViewById(R.id.scheduleView);
        mCreateCheckBox = (CheckBox) getView().findViewById(R.id.scheduleCreate);
        mCreateButton = (Button) getView().findViewById(R.id.createDays);

        mAmountText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        // Set the input filter for the amount text box
        InputFilter[] filters = new InputFilter[2];
        filters[0] = DigitsKeyListener.getInstance(false, true);
        filters[1] = new InputFilter.LengthFilter(10);

        mAmountText.getText().setFilters(filters);

        // Set the date button click listener
        mDateButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showDatePickerDialog();
            }
        });

        // set the account selected listener
        mMainAccountButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View arg0) {
                showMainAccountSelectDialog();
            }
        });

        // Set the secondary account listener
        mSecondaryAccountButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showSecondaryAccountSelectDialog();
            }
        });

        // Add items to create days box
        mCreateButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayCreateDaysDialog();
            }
        });

        // Add click listener for frequency type.
        mFrequencyTypeButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayFrequencyTypesDialog();
            }
        });

        // Add click listener for transaction type.
        mTransactionTypeButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayTransactionTypeDialog();
            }
        });

        // Add default data to input boxes
        if (mUpdateTransaction == null) {

            // Get the current date.
            mCalendar = Calendar.getInstance();
            updateDisplay();

            // Populate the Main Account button
            mMainAccountButton.setText(mMainAccount.getTypeName() + ":" + mMainAccount.getName());

            // Populate the Secondary Account button
            setSecondaryAccount();

            // Populate transaction type box
            mTransactionTypeButton.setText(getString(R.string.depositText));

            // Populate the frequency type box
            mFrequencyTypeButton.setText(getFrequencyTypes().get(0));

            // Populate the create days box
            mCreateButton.setText(mCreateDays.toString());

            createDaysView("");
        } else { // Add data from selected transaction in input boxes
            setDataInFields();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.edit_schedule_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_done:
                save();
                return true;
        }
        return false;
    }

    private void showMainAccountSelectDialog() {

        AccountSelectDialog dialog = new AccountSelectDialog(getActivity(), new AccountSelectedListener() {

            @Override
            public void accountSelected(Account account) {
                mMainAccount = account;
                mMainAccountButton.setText(mMainAccount.getTypeName() + ":" + mMainAccount.getName());
                updateTransactionType();
                setSecondaryAccount();
            }
        });
        dialog.addAccountClass(AssetAccount.class.toString());
        dialog.addAccountClass(BankAccount.class.toString());
        dialog.addAccountClass(ExpenseAccount.class.toString());
        dialog.addAccountClass(IncomeAccount.class.toString());
        dialog.addAccountClass(LiabilityAccount.class.toString());
        dialog.show();

    }

    private void showSecondaryAccountSelectDialog() {

        AccountSelectDialog dialog = new AccountSelectDialog(getActivity(), new AccountSelectedListener() {

            @Override
            public void accountSelected(Account account) {
                mSecondaryAccount = account;
                mSecondaryAccountButton.setText(mSecondaryAccount.getTypeName() + ":" + mSecondaryAccount.getName());
            }
        });
        dialog.addAccountClass(AssetAccount.class.toString());
        dialog.addAccountClass(BankAccount.class.toString());
        dialog.addAccountClass(ExpenseAccount.class.toString());
        dialog.addAccountClass(IncomeAccount.class.toString());
        dialog.addAccountClass(LiabilityAccount.class.toString());
        dialog.removeAccount(mMainAccount);
        dialog.show();
    }

    private void showDatePickerDialog() {
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), dateSetListener, mCalendar.get(Calendar.YEAR),
                mCalendar.get(Calendar.MONTH), mCalendar.get(Calendar.DAY_OF_MONTH));
        dialog.show();
    }

    protected void updateTransactionType() {

        ArrayList<String> types = getTransactionTypes();

        if (types.size() >= 2) {
            if (mTransactionType == Transaction.TRANSACTION_TYPE_DEBIT) {
                mTransactionTypeButton.setText(types.get(0));
            } else {
                mTransactionTypeButton.setText(types.get(1));
            }
        }
    }

    protected ArrayList<String> getTransactionTypes() {

        ArrayList<String> types = new ArrayList<String>();
        String acctType = mMainAccount.getTypeName();

        if (acctType.equalsIgnoreCase("Liability")) {
            types.add(getString(R.string.paymentText));
            types.add(getString(R.string.chargeText));
        } else if (acctType.equalsIgnoreCase("Bank") || acctType.equalsIgnoreCase("Asset")) {
            types.add(getString(R.string.depositText));
            types.add(getString(R.string.withdrawalText));
        } else if (acctType.equalsIgnoreCase("Expense")) {
            types.add(getString(R.string.expenseText));
            types.add(getString(R.string.rebateText));
        }

        return types;
    }

    // protected ArrayList<Account> getSecondaryAccounts() {
    //
    // ArrayList<Account> accounts =
    // mController.getRootAccounts(AssetAccount.class);
    // accounts.addAll(mController.getRootAccounts(BankAccount.class));
    // accounts.addAll(mController.getRootAccounts(LiabilityAccount.class));
    // accounts.addAll(mController.getRootAccounts(ExpenseAccount.class));
    // accounts.addAll(mController.getRootAccounts(IncomeAccount.class));
    //
    // return accounts;
    // }

    protected void setSecondaryAccount() {

        List<Account> accountList = getMainAccounts();

        int x = 0;
        while (x < accountList.size() && accountList.get(x).getId() == mMainAccount.getId()) {
            x++;
        }

        Account secondary = accountList.get(x);
        mSecondaryAccount = secondary;
        mSecondaryAccountButton.setText(secondary.getTypeName() + ":" + secondary.getName());
    }

    protected List<Account> getMainAccounts() {

        List<Account> accounts = mController.getRootAccounts(AssetAccount.class.toString());
        accounts.addAll(mController.getRootAccounts(BankAccount.class.toString()));
        accounts.addAll(mController.getRootAccounts(LiabilityAccount.class.toString()));

        return accounts;
    }

    protected void displayTransactionTypeDialog() {

        ArrayList<String> data = getTransactionTypes();
        ArrayList<String> tags = new ArrayList<String>();

        tags.add(String.valueOf(Transaction.TRANSACTION_TYPE_DEBIT));
        tags.add(String.valueOf(Transaction.TRANSACTION_TYPE_CREDIT));

        GenericDialog dialog = new GenericDialog(getActivity());
        dialog.setTitle("Transaction Type");
        dialog.setData(data);
        dialog.setTags(tags);
        dialog.setClickListener(new onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {

                mTransactionType = Integer.valueOf(tag);
                mTransactionTypeButton.setText(text);
            }
        });
        dialog.display();
    }

    protected void displayFrequencyTypesDialog() {

        ArrayList<String> data = new ArrayList<String>();
        data.addAll(getFrequencyTypes());

        ArrayList<String> types = new ArrayList<String>();
        types.add(String.valueOf(SCHEDULE_FREQEUNCY_DAILY));
        types.add(String.valueOf(SCHEDULE_FREQEUNCY_WEEKLY));
        types.add(String.valueOf(SCHEDULE_FREQEUNCY_SEMI_MONTHLY));
        types.add(String.valueOf(SCHEDULE_FREQEUNCY_MONTHLY));

        GenericDialog dialog = new GenericDialog(getActivity());
        dialog.setTitle("Frequency Types");
        dialog.setData(data);
        dialog.setTags(types);
        dialog.setClickListener(new onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                mScheduleFrequency = Integer.valueOf(tag);
                mFrequencyTypeButton.setText(text);
                createDaysView("");
            }
        });
        dialog.display();
    }

    protected void displayCreateDaysDialog() {

        ArrayList<String> data = new ArrayList<String>();
        for (Integer x = 0; x < 31; x++) {
            data.add(x.toString());
        }

        GenericDialog dialog = new GenericDialog(getActivity());
        dialog.setTitle("Create Days");
        dialog.setData(data);
        dialog.setClickListener(new onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                mCreateDays = Integer.valueOf(text);
                mCreateButton.setText(text);
            }
        });
        dialog.display();
    }

    private void updateTransaction() {

        ScheduledTransaction t = createTransaction();

        mController.updateScheduledTransaction(t);
    }

    /**
     * Adds a new transaction to the database.
     */
    private void addNewTransaction() {

        ScheduledTransaction t = createTransaction();
        t.updateDate();

        mController.insertScheduledTransaction(t);
    }

    private void save() {

        if (mUpdateTransaction == null) {
            addNewTransaction();
        } else {
            updateTransaction();
        }
        getActivity().getFragmentManager().popBackStack();
        // ((MainActivity) getActivity()).closeKeyboard();
    }

    /**
     * The callback received when the user "sets" the date in the dialog.
     */
    private final OnDateSetListener dateSetListener = new OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
            mCalendar = Calendar.getInstance();
            mCalendar.set(Calendar.YEAR, year);
            mCalendar.set(Calendar.MONTH, monthOfYear);
            mCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDisplay();
        }
    };

    // updates the date we display in the TextView
    private void updateDisplay() {
        // Get the current date.
        mDateButton.setText(new StringBuilder().append(mCalendar.get(Calendar.MONTH) + 1).append("-")
                .append(mCalendar.get(Calendar.DAY_OF_MONTH)).append("-").append(mCalendar.get(Calendar.YEAR))
                .append(" "));
    }

    public static ArrayList<String> getFrequencyTypes() {

        ArrayList<String> types = new ArrayList<String>();
        types.add("Daily");
        types.add("Weekly");
        types.add("Semi-Monthly");
        types.add("Monthly");
        return types;
    }

    private void createDaysView(String data) {

        mScheduleDaysView.removeAllViews();
        mTempDaysView = null;

        switch (mScheduleFrequency) {
            case SCHEDULE_FREQEUNCY_DAILY:
                mTempDaysView = new ScheduleDaysView(getActivity(), null);
                break;

            case SCHEDULE_FREQEUNCY_WEEKLY:
                mTempDaysView = new ScheduleWeeklyView(getActivity(), null);
                break;

            case SCHEDULE_FREQEUNCY_SEMI_MONTHLY:
                mTempDaysView = new ScheduleSemiMonthlyView(getActivity(), null);
                break;

            case SCHEDULE_FREQEUNCY_MONTHLY:
                mTempDaysView = new ScheduleMonthlyView(getActivity(), null);
                break;
        }

        mTempDaysView.setData(data);
        mScheduleDaysView.addView(mTempDaysView);
    }

    /**
     * Sets all data that is passed in to edit an already created transaction.
     */
    private void setDataInFields() {

        // mSettingData = true;

        try {
            // String[] date =
            // mUpdateTransaction.getDate().toString().split("-");
            // mMonth = Integer.parseInt(date[0]);
            // mDay = Integer.parseInt(date[1]);
            // mYear = Integer.parseInt(date[2]);

            Date date = mUpdateTransaction.getDate();
            mCalendar.setTime(date);

            updateDisplay();
        } catch (Exception e) {
            // Log.e(this.getLocalClassName(),
            // "Unable to parse date in setDataInFields");
        }

        // Get the main account
        mMainAccount = mController.getAccount(mUpdateTransaction.getMainAccountId());
        mMainAccountButton.setText(mMainAccount.getTypeName() + ":" + mMainAccount.getName());

        // Get the secondary account
        mSecondaryAccount = mController.getAccount(mUpdateTransaction.getSecondaryAccountId());
        mSecondaryAccountButton.setText(mSecondaryAccount.getTypeName() + ":" + mSecondaryAccount.getName());

        // Get the Transaction type
        mTransactionType = mUpdateTransaction.getTransactionType();

        String text1 = "";
        String text2 = "";

        if (mMainAccount.getClass() == BankAccount.class || mMainAccount.getClass() == AssetAccount.class) {
            text1 = getString(R.string.depositText);
            text2 = getString(R.string.withdrawalText);
        } else if (mMainAccount.getClass() == LiabilityAccount.class) {
            text1 = getString(R.string.paymentText);
            text2 = getString(R.string.chargeText);
        } else if (mMainAccount.getClass() == ExpenseAccount.class) {
            text1 = getString(R.string.expenseText);
            text2 = getString(R.string.rebateText);
        }

        if (mTransactionType == Transaction.TRANSACTION_TYPE_DEBIT) {
            mTransactionTypeButton.setText(text1);
        } else {
            mTransactionTypeButton.setText(text2);
        }

        // Get number of days to create
        mCreateDays = mUpdateTransaction.getCreateDays();
        mCreateButton.setText(mCreateDays.toString());

        // Get if create in advance
        mCreateCheckBox.setEnabled(mUpdateTransaction.getAutoCreate());

        // Get the amount
        Formatter f = new Formatter(Locale.US);
        f.format("%.2f", mUpdateTransaction.getAmount());
        mAmountText.setText(f.toString());
        f.close();

        // Get the description
        mDescriptionText.setText(mUpdateTransaction.getDescription());

        // Get the frequency type
        mFrequencyTypeButton.setText(mUpdateTransaction.getFrequencyName());

        if (mUpdateTransaction.getClass() == DailyScheduledTransaction.class) {
            mScheduleFrequency = SCHEDULE_FREQEUNCY_DAILY;
        } else if (mUpdateTransaction.getClass() == WeeklyScheduledTransaction.class) {
            mScheduleFrequency = SCHEDULE_FREQEUNCY_WEEKLY;
        } else if (mUpdateTransaction.getClass() == SemiMonthlyScheduledTransaction.class) {
            mScheduleFrequency = SCHEDULE_FREQEUNCY_SEMI_MONTHLY;
        } else if (mUpdateTransaction.getClass() == MonthlyScheduledTransaction.class) {
            mScheduleFrequency = SCHEDULE_FREQEUNCY_MONTHLY;
        }

        createDaysView(mUpdateTransaction.getFrequency());
    }

    private ScheduledTransaction createTransaction() {

        ScheduledTransaction scheduledTransaction = null;

        // Get the date.
        Date date = null;
        try {
            date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse(mDateButton.getText().toString());
        } catch (Exception ex) {
            // Log.e(this.getLocalClassName(),
            // "Unable to parse date in createTransaction");
        }

        // Get the amount.
        String amount = mAmountText.getText().toString().trim();
        if (amount.equals("") || amount.equals(".")) {
            amount = "0";
        }

        // Get the description.
        String description = mDescriptionText.getText().toString();

        switch (mScheduleFrequency) {

            case SCHEDULE_FREQEUNCY_DAILY:
                scheduledTransaction = new DailyScheduledTransaction(mMainAccount.getId(), mSecondaryAccount.getId(),
                        mTransactionType, mTempDaysView.getData(), date, description, Double.parseDouble(amount), 1,
                        mCreateCheckBox.isChecked() ? 1 : 0, mCreateDays, 0);
                break;

            case SCHEDULE_FREQEUNCY_WEEKLY:
                scheduledTransaction = new WeeklyScheduledTransaction(mMainAccount.getId(), mSecondaryAccount.getId(),
                        mTransactionType, mTempDaysView.getData(), date, description, Double.parseDouble(amount), 1,
                        mCreateCheckBox.isChecked() ? 1 : 0, mCreateDays, 0);
                break;

            case SCHEDULE_FREQEUNCY_SEMI_MONTHLY:
                scheduledTransaction = new SemiMonthlyScheduledTransaction(mMainAccount.getId(), mSecondaryAccount.getId(),
                        mTransactionType, mTempDaysView.getData(), date, description, Double.parseDouble(amount), 1,
                        mCreateCheckBox.isChecked() ? 1 : 0, mCreateDays, 0);
                break;

            case SCHEDULE_FREQEUNCY_MONTHLY:
                scheduledTransaction = new MonthlyScheduledTransaction(mMainAccount.getId(), mSecondaryAccount.getId(),
                        mTransactionType, mTempDaysView.getData(), date, description, Double.parseDouble(amount), 1,
                        mCreateCheckBox.isChecked() ? 1 : 0, mCreateDays, 0);
                break;
        }

        if (mUpdateTransaction != null) {
            scheduledTransaction.setId(mUpdateTransaction.getId());
        }

        return scheduledTransaction;
    }
}
