package org.jtb.moneytracker.ui.account;

public interface TransactionRequest {

    public static final int DEPOSIT = 0;
    public static final int WITHDRAWAL = 1;
}
