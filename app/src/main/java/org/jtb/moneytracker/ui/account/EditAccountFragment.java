/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.ui.account;

import org.jtb.moneytracker.controller.MoneyTrackerController;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

/**
 * 
 * @author jason
 */
public class EditAccountFragment extends AccountFragment {

    private MoneyTrackerController mController = MoneyTrackerController.getInstance();

    @Override
    public void onCreate(Bundle icicle) {
	super.onCreate(icicle);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
	super.onViewCreated(view, savedInstanceState);

	mAccount = mController.getAccount(getArguments().getInt("Account"));

	setAccountTypesEnabled(false);
	setData();
	getActivity().setTitle("Edit Account");
    }

    @Override
    protected boolean onDoneClicked() {

	String name = mAccount.getName();
	if (name != null && name.equals("") == false) {

	    mController.updateAccount(mAccount);
	    Toast.makeText(getActivity(), "Account Updated...", Toast.LENGTH_LONG).show();
	    return true;
	} else {
	    showAccountNameErrorDialog();
	    return false;
	}
    }
}
