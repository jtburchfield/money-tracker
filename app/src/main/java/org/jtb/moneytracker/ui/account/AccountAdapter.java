/**
 * 
 */
package org.jtb.moneytracker.ui.account;

import java.util.ArrayList;

import org.jtb.moneytracker.data.account.Account;

import android.content.Context;
import android.graphics.Color;

/**
 * @author jason
 * 
 */
public class AccountAdapter {

    private final Context context;
    private final AccountListView accountsListView;
    private int selectedIndex;

    public AccountAdapter(Context parent) {
        this.context = parent;

        ArrayList<Account> accountList = new ArrayList<Account>();
        this.accountsListView = new AccountListView(this.context, accountList);
        this.accountsListView.setTextColor(Color.BLACK);

        this.selectedIndex = 0;
    }

    /**
     * Adds an account to the adapter.
     * 
     * @param account
     *            the Account to add
     */
    public void add(Account account) {

        this.accountsListView.add(account);
    }

    public void addAll(ArrayList<Account> accountList) {

        for (int x = 0; x < accountList.size(); x++) {

            this.accountsListView.add(accountList.get(x));
        }
    }

    /**
     * Deletes all accounts associated with this adapter.
     */
    public void clear() {

        this.accountsListView.clear();
    }

    /**
     * Gets an Account at the given view position
     * 
     * @param position
     *            - position of the account in the list
     * @return Account
     */
    // public Account getAccount(int position) {
    //
    // Account a = null;
    //
    // if (this.accountsListView.getChildCount() > 0
    // && position < this.accountsListView.getChildCount()) {
    //
    // a = this.accountsListView.getAccount(position);
    // }
    // return a;
    // }

    /**
     * Gets the currently selected account.
     * 
     * @return Account
     */
    public Account getSelectedAccount() {

        Account a = null;

        if (this.accountsListView.getChildCount() > 0 && this.selectedIndex < this.accountsListView.getChildCount()) {

            a = this.accountsListView.getAccount(this.selectedIndex);
        }
        return a;
    }

    /**
     * Sets the currently selected account.
     * 
     * @param account
     *            - The account to set as currently selected. Does not change if
     *            null.
     */
    // public void setSelectedAccount(Account account) {
    //
    // if (account != null) {
    // for (int x = 0; x < this.accountsListView.getChildCount(); x++) {
    // if (this.accountsListView.getAccount(x).getId() == account.getId()) {
    // this.selectedIndex = x;
    // }
    // }
    // }
    // }

    public int getCount() {

        return this.accountsListView.getChildCount();
    }

    // public AccountListView getView() {
    //
    // return this.accountsListView;
    // }

    public AccountListView copyView() {

        return this.accountsListView;// new AccountListView(this.context,
                                     // this.accountsListView);
    }

    /**
     * This method gets the name of each parent account down to this account and
     * creates a string of each name separated by the character provided.
     * 
     * @param separator
     *            - character to separate names by
     * @return the full name
     */
    public String getFullName(char separator) {

        String name = this.getSelectedAccount().getTypeName();// .getTypeString();
        // DatabaseManager db = new DatabaseManager(this.context);
        // db.open();

        // Account acct =
        // db.getAccount(this.getSelectedAccount().getParentId());
        //
        // while (acct != null) {
        //
        // name = name + separator + acct.getName();
        // acct = db.getAccount(acct.getParentId());
        // }
        //
        // db.close();

        name += separator;
        return name;
    }
}
