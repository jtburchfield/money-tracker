package org.jtb.moneytracker.ui.account;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.controller.MoneyTrackerEvents;
import org.jtb.moneytracker.data.account.Account;

import java.util.Collections;
import java.util.List;

public class AccountListFragment extends Fragment {

    private MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private AccountListView mAccountsView;
    private Account mSelectedAccount;
    private String mAccountType;

    private final Handler mHandler = new Handler() {

        public void handleMessage(Message message) {

            switch (message.what) {

                case MoneyTrackerEvents.ACCOUNT_UPDATED:
                case MoneyTrackerEvents.ACCOUNT_DELETED:
                case MoneyTrackerEvents.TRANSACTION_DELETED:
                    updateAccountsView();
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mController.register(mHandler);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.accounts_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        getActivity().setTitle("Account Balances");

        mController.showAd(view);

        mAccountsView = new AccountListView(getView().getContext(), getAccounts());
        mAccountsView.setOnAccountLongClickListener(new OnLongClickListener() {

            @Override
            public boolean onLongClick(View v) {
                mSelectedAccount = mAccountsView.getAccount(v);
                showOptionsDialog();
                return false;
            }
        });

        mAccountsView.setOnAccountClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mSelectedAccount = mAccountsView.getAccount(v);
                showTransactions();
            }
        });

        mAccountsView.setOnCategoryClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                mSelectedAccount = mAccountsView.getAccount(v);
                showAccounts();
            }
        });

        LinearLayout accountLayout = (LinearLayout) getView().findViewById(R.id.list);

        accountLayout.removeAllViewsInLayout();
        accountLayout.addView(mAccountsView);
    }

    private void updateAccountsView() {

        mAccountsView.removeAccount(mSelectedAccount);
    }

    private void showConfirmDeleteDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Delete " + mSelectedAccount.getName() + "?");
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setMessage("All transactions using this account will also " + "be deleted.\n\n"
                + "Are you sure you want to delete this account? ");

        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {

                deleteAccount(mSelectedAccount);
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }

    private void showDeleteSelectedErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Error");
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setMessage("This is the currently selected account on the main display. "
                + "This account cannot be deleted.");

        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }

    /**
     * This method gets accounts using the id provided.
     *
     * @return List of accounts.
     */
    private List<Account> getAccounts() {

        List<Account> accountsList = null;
        String accountTypeString = getArguments().getString("AccountType");

        if (accountTypeString != null) {
            mAccountType = accountTypeString;
            accountsList = mController.getRootAccounts(mAccountType);
        } else {
            accountsList = mController.getAccount(getArguments().getInt("ParentId")).getSubAccounts();
        }

        Collections.sort(accountsList, new AccountSort());

        return accountsList;
    }

    private void deleteAccount(Account account) {

        mController.deleteAccount(account);
    }

    private void showOptionsDialog() {

        if (mSelectedAccount != null) {
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Select an option...");
            builder.setIcon(R.drawable.money_tracker_main);

            String[] items = new String[2];
            items[0] = "Edit Account";
            items[1] = "Delete Account";

            builder.setItems(items, new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface arg0, int arg1) {
                    if (arg1 == 0) {
                        editAccount();
                    } else if (arg1 == 1) {
                        processDeleteAccount();
                    }
                }
            });

            builder.create().show();
        }
    }

    private void editAccount() {

        mController.showEditAccountFragment(getActivity(), mSelectedAccount);
    }

    private boolean processDeleteAccount() {

        Account mainAccount = mController.getSelectedAccount();

        if (mSelectedAccount.getName().equals(mainAccount.getName())) {
            showDeleteSelectedErrorDialog();
        } else {
            showConfirmDeleteDialog();
        }

        return true;
    }

    private void showTransactions() {

        if (mSelectedAccount != null) {
            mController.showTransactionListFragment(getActivity(), mSelectedAccount);
        }
    }

    private void showAccounts() {

        if (mSelectedAccount != null) {
            mController.showAccountListFragment(getActivity(), mSelectedAccount.getId());
        }
    }

    private class AccountSort implements java.util.Comparator<Account> {

        public int compare(Account one, Account two) {
            return one.getName().compareTo(two.getName());
        }
    }
}
