/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

package org.jtb.moneytracker.ui.schedule;

import java.util.ArrayList;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.ui.GenericDialog;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * 
 * @author jason
 */
public class ScheduleSemiMonthlyView extends ScheduleView {

    private Button mMonthsButton;
    private Button mDay1Button;
    private Button mDay2Button;

    public ScheduleSemiMonthlyView(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context, R.layout.schedule_semi_monthly, this);

        // Set the months spinner.
        mMonthsButton = (Button) findViewById(R.id.months);
        mMonthsButton.setText("1");
        mMonthsButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayMonthsDialog();
            }
        });

        // Set day 1 spinner.
        mDay1Button = (Button) findViewById(R.id.day1);
        mDay1Button.setText("1");
        mDay1Button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayDay1Dialog();
            }
        });

        // Set day 2 spinner.
        mDay2Button = (Button) findViewById(R.id.day2);
        mDay2Button.setText("2");
        mDay2Button.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayDay2Dialog();
            }
        });
    }

    @Override
    public String getData() {

        String data = mMonthsButton.getText().toString();
        data += SEPARATOR + mDay1Button.getText().toString();
        data += SEPARATOR + mDay2Button.getText().toString();

        return data;
    }

    @Override
    public void setData(String data) {

        if (!data.equals("")) {
            try {

                String[] myData = data.split(SEPARATOR);

                mMonthsButton.setText(myData[0]);
                mDay1Button.setText(myData[1]);
                mDay2Button.setText(myData[2]);
            }
            catch (Exception e) {
				Log.e(ScheduleSemiMonthlyView.this.toString(),
                		"[ScheduleSemiMonthlyView - setData]: " + e.getMessage());
            }
        }
    }

    private void displayMonthsDialog() {

        ArrayList<String> data = new ArrayList<String>();
        for (Integer x = 1; x <= 12; x++) {
            data.add(x.toString());
        }

        GenericDialog dialog = new GenericDialog(getContext());
        dialog.setTitle("Months");
        dialog.setData(data);
        dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                mMonthsButton.setText(text);
            }
        });
        dialog.display();
    }

    private void displayDay1Dialog() {

        ArrayList<String> data = new ArrayList<String>();
        for (Integer x = 1; x <= 29; x++) {
            data.add(x.toString());
        }

        GenericDialog dialog = new GenericDialog(getContext());
        dialog.setTitle("Day 1");
        dialog.setData(data);
        dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                mDay1Button.setText(text);
                Integer i1 = Integer.valueOf(text);
                Integer i2 = Integer.valueOf(mDay2Button.getText().toString());
                if (i1 >= i2) {
                    String text2 = String.valueOf(i1 + 1);
                    mDay2Button.setText(text2);
                }
            }
        });
        dialog.display();
    }

    private void displayDay2Dialog() {

        ArrayList<String> data = new ArrayList<String>();
        for (Integer x = Integer.valueOf(mDay1Button.getText().toString()) + 1; x <= 31; x++) {
            data.add(x.toString());
        }

        GenericDialog dialog = new GenericDialog(getContext());
        dialog.setTitle("Day 2");
        dialog.setData(data);
        dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                mDay2Button.setText(text);
            }
        });
        dialog.display();
    }
}
