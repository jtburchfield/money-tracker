/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.ui.account;

import java.util.ArrayList;
import java.util.List;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * 
 * @author jason
 */
public class AccountTypesListView extends LinearLayout {

    private ArrayList<String> mAccountShortTypes = new ArrayList<String>();
    //private ArrayList<String> mAccountTypes = new ArrayList<String>();

    /**
     * Constructor
     * 
     * @param context
     *            the view's context
     * @param parent
     *            the parent of the view
     * @param acctList
     *            the list of accounts that the view should display
     */
    public AccountTypesListView(Context context) {
        super(context);

        setOrientation(LinearLayout.VERTICAL);
        LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                        android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        setLayoutParams(params);
    }

    public void addAccountClass(String className) {

        String shortName = MoneyTrackerController.getInstance().getShortAccountTypeName(className);
        //mAccountTypes.add(accountClass.getName());
        mAccountShortTypes.add(shortName);
        addAccountTypeView(shortName);
    }

    public void addAccountClass(List<String> classList) {

        for (String x : classList) {
            addAccountClass(x);
        }
    }

    /**
     * @param account
     */
    private void addAccountTypeView(String type) {

        // create a view
        LinearLayout view = (LinearLayout) View.inflate(getContext(), R.layout.account_type_row, null);

        // Get the account view from the view.
        LinearLayout accountView = (LinearLayout) view.findViewById(R.id.accountTypeView);

        // Set the index value to match accountList.
        accountView.setTag(getChildCount());

        // Get the Account Name TextView object and set data from account.
        TextView accountTypeView = (TextView) view.findViewById(R.id.accountType);
        accountTypeView.setText(type);

        // add the view to the main view
        addView(view);
    }

    void setOnAccountClickListener(final OnClickListener listener) {

        for (int index = 0; index < getChildCount(); index++) {

            // get the item at this index in the view
            View view = getChildAt(index);

            // get the account view for this row
            LinearLayout accountView = (LinearLayout) view.findViewById(R.id.accountTypeView);

            // set the on click listener for the account view
            accountView.setOnClickListener(listener);
        }
    }

    String getClassTypeName(View v) {

        String typeName = "";

        try {
            typeName = mAccountShortTypes.get((Integer) v.getTag());
        }
        catch (Exception ex) {
            // TODO log exception
        }

        return typeName;
    }
}
