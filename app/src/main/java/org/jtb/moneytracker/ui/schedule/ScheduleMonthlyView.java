/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

package org.jtb.moneytracker.ui.schedule;

import java.util.ArrayList;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.ui.GenericDialog;

import android.content.Context;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.Button;

/**
 * 
 * @author jason
 */
public class ScheduleMonthlyView extends ScheduleView {

	private Button mMonthButton;
	private Button mDayButton;

	public ScheduleMonthlyView(Context context, AttributeSet attrs) {
		super(context, attrs);

		inflate(context, R.layout.schedule_monthly, this);

		mMonthButton = (Button) findViewById(R.id.months);
		mMonthButton.setText("1");
		mMonthButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				displayMonthsDialog();
			}
		});

		mDayButton = (Button) findViewById(R.id.day);
		mDayButton.setText("1");
		mDayButton.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				displayDayDialog();
			}
		});

	}

	@Override
	public String getData() {

		String data = mMonthButton.getText().toString();
		data += SEPARATOR + mDayButton.getText().toString();
		return data;
	}

	@Override
	public void setData(String data) {

		if (!data.equals("")) {
			try {
				String[] myData = data.split(SEPARATOR);

				mMonthButton.setText(myData[0]);
				mDayButton.setText(myData[1]);
			} catch (Exception e) {
				Log.e(ScheduleMonthlyView.this.toString(),
						"[ScheduleMonthlyView - setData]: " + e.getMessage());
			}
		}
	}

	private void displayMonthsDialog() {

		ArrayList<String> data = new ArrayList<String>();
		for (Integer x = 1; x <= 12; x++) {
			data.add(x.toString());
		}

		GenericDialog dialog = new GenericDialog(getContext());
		dialog.setTitle("Month");
		dialog.setData(data);
		dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

			@Override
			public void onClick(String text, String tag) {
				mMonthButton.setText(text);
			}
		});
		dialog.display();
	}

	private void displayDayDialog() {

		ArrayList<String> data = new ArrayList<String>();
		for (Integer x = 1; x <= 31; x++) {
			data.add(x.toString());
		}

		GenericDialog dialog = new GenericDialog(getContext());
		dialog.setTitle("Day");
		dialog.setData(data);
		dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

			@Override
			public void onClick(String text, String tag) {
				mDayButton.setText(text);
			}
		});
		dialog.display();
	}
}
