package org.jtb.moneytracker.ui.account;

public interface AccountTypeSelectedListener {

    public void accountTypeChanged(String className);
}
