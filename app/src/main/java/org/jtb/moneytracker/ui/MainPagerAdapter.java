package org.jtb.moneytracker.ui;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.jtb.moneytracker.ui.account.AccountTypesFragment;
import org.jtb.moneytracker.ui.budget.BudgetFragment;
import org.jtb.moneytracker.ui.news.NewsListFragment;
import org.jtb.moneytracker.ui.schedule.ScheduleListFragment;

/**
 * Created by jason on 12/10/15.
 */
public class MainPagerAdapter extends FragmentPagerAdapter {

    private String[] mPageTitles = {"Home", "Accounts", "Schedule", "Budget", "News"};

    public MainPagerAdapter(FragmentManager manager) {
        super(manager);
    }

    @Override
    public Fragment getItem(int position) {

        Fragment fragment = null;

        switch (position) {
            case 0:
                fragment = new HomeFragment();
                break;

            case 1:
                fragment = new AccountTypesFragment();
                break;

            case 2:
                fragment = new ScheduleListFragment();
                break;

            case 3:
                fragment = new BudgetFragment();
                break;

            case 4:
                fragment = new NewsListFragment();
                break;
        }

        return fragment;
    }

    @Override
    public int getCount() {
        return mPageTitles.length;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return mPageTitles[position];
    }
}
