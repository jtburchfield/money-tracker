/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.jtb.moneytracker.ui.schedule;

import java.util.ArrayList;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.ui.GenericDialog;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

/**
 * 
 * @author jason
 */
public class ScheduleDaysView extends ScheduleView {

    private Button mDaysButton;
    
    public ScheduleDaysView(Context context, AttributeSet attrs) {
        super(context, attrs);

        inflate(context, R.layout.schedule_days, this);

        // Set the days adapter
        mDaysButton = (Button) findViewById(R.id.days);

        mDaysButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                displayDaysDialog();
            }
        });

        mDaysButton.setText("1");
    }

    @Override
    public String getData() {
        return mDaysButton.getText().toString();
    }

    @Override
    public void setData(String data) {

        if (!data.equals("")) {
            try {
                mDaysButton.setText(data);
            }
            catch (Exception e) {
            }
        }
    }

    private void displayDaysDialog() {

        ArrayList<String> data = new ArrayList<String>();
        for (Integer x = 1; x <= 30; x++) {
            data.add(x.toString());
        }

        GenericDialog dialog = new GenericDialog(getContext());
        dialog.setTitle("Number of Days");
        dialog.setData(data);
        dialog.setClickListener(new GenericDialog.onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                mDaysButton.setText(text);
            }
        });
        dialog.display();
    }
}
