/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */

package org.jtb.moneytracker.ui.schedule;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.CreditAccount;
import org.jtb.moneytracker.data.account.DebitAccount;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.schedule.ScheduledTransaction;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * 
 * @author jason
 */
public class ScheduledTransactionsListAdapter extends ArrayAdapter<ScheduledTransaction> {

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private final Context context;
    private final ArrayList<ScheduledTransaction> transactions;

    public ScheduledTransactionsListAdapter(Context context, int accountId, ArrayList<ScheduledTransaction> transactions) {
	super(context, accountId, transactions);
	this.context = context;
	this.transactions = transactions;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

	View v = convertView;
	if (v == null) {

	    v = LayoutInflater.from(context).inflate(R.layout.scheduled_transaction_row, parent, false);
	}

	ScheduledTransaction t = this.transactions.get(position);

	if (t != null) {

	    try {
		TextView dateView = (TextView) v.findViewById(R.id.date);
		TextView debitAccountView = (TextView) v.findViewById(R.id.debitAccount);
		TextView creditAccountView = (TextView) v.findViewById(R.id.creditAccount);
		TextView amountView = (TextView) v.findViewById(R.id.amount);
		TextView typeView = (TextView) v.findViewById(R.id.transactionType);
		TextView freqeuncyView = (TextView) v.findViewById(R.id.frequency);
		TextView frequencyDaysView = (TextView) v.findViewById(R.id.frequencyDays);
		TextView frequencyNum = (TextView) v.findViewById(R.id.frequencyNum);

		Account mainAccount = mController.getAccount(t.getMainAccountId());
		Account secondaryAccount = mController.getAccount(t.getSecondaryAccountId());

		dateView.setText(new SimpleDateFormat(MoneyTrackerController.getDateFormat()).format(t.getDate()));

		if (t.getTransactionType() == Transaction.TRANSACTION_TYPE_DEBIT && mainAccount instanceof DebitAccount) {

		    typeView.setText(R.string.depositText);
		} else if (t.getTransactionType() == Transaction.TRANSACTION_TYPE_DEBIT
			&& mainAccount instanceof CreditAccount) {

		    typeView.setText(R.string.paymentText);
		} else if (t.getTransactionType() == Transaction.TRANSACTION_TYPE_CREDIT
			&& mainAccount instanceof DebitAccount) {

		    typeView.setText(R.string.withdrawalText);
		} else if (t.getTransactionType() == Transaction.TRANSACTION_TYPE_CREDIT
			&& mainAccount instanceof CreditAccount) {

		    typeView.setText(R.string.chargeText);
		}

		debitAccountView.setText(mainAccount.getTypeName() + ":" + mainAccount.getName());

		creditAccountView.setText(secondaryAccount.getTypeName() + ":" + secondaryAccount.getName());

		amountView.setText(NumberFormat.getCurrencyInstance().format(t.getAmount()));

		frequencyDaysView.setText(t.getFrequencyDays());
		freqeuncyView.setText(t.getFrequencyName());

		String num = t.getFrequencyNum();
		if (num.equals("") == false) {
		    num = " x " + num;
		}
		frequencyNum.setText(num);

		dateView.setTextSize(12);
		typeView.setTextSize(12);
		debitAccountView.setTextSize(12);
		creditAccountView.setTextSize(12);
		freqeuncyView.setTextSize(12);
		frequencyDaysView.setTextSize(12);
		frequencyNum.setTextSize(12);
		amountView.setTextSize(14);
	    } catch (Exception ex) {

	    }
	}
	return v;
    }
}
