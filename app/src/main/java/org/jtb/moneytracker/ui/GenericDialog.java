package org.jtb.moneytracker.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.jtb.moneytracker.R;

import java.util.ArrayList;

public class GenericDialog {

    private String mTitle = "";
    private Context mContext = null;
    private ArrayList<String> mData = new ArrayList<>();
    private ArrayList<String> mTags = new ArrayList<>();
    private onGenericDialogClickListener mClickListener = null;
    private Dialog mDialog = null;

    public GenericDialog(final Context context) {

        mContext = context;
    }

    /**
     * Sets the title of the dialog.
     * 
     * @param title
     * 
     */
    public void setTitle(final String title) {

        mTitle = title;
    }

    /**
     * Set the data to be displayed in the dialog.
     * 
     * @param data
     * 
     */
    public void setData(final ArrayList<String> data) {

        mData = data;
    }

    /**
     * Set a tag that corresponds to each data element from setData
     * 
     * @param tags
     */
    public void setTags(final ArrayList<String> tags) {

        mTags = tags;
    }

    /**
     * Set the click listener to be called when an item is clicked.
     * 
     * @param clickListener
     */
    public void setClickListener(final onGenericDialogClickListener clickListener) {

        mClickListener = clickListener;
    }

    /**
     * Display the dialog.
     */
    public void display() {

    	create();
        mDialog.show();
    }
    
    public Dialog create() {
    	
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mTitle);
        builder.setCancelable(true);
        builder.setIcon(R.drawable.money_tracker_main);

        ScrollView scrollView = new ScrollView(mContext);
        LinearLayout mainLayout = new LinearLayout(mContext);
        mainLayout.setOrientation(LinearLayout.VERTICAL);
        LayoutParams mainParams = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                        android.view.ViewGroup.LayoutParams.MATCH_PARENT);
        mainLayout.setLayoutParams(mainParams);
        scrollView.setLayoutParams(mainParams);
        LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                        android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        params.leftMargin = 10;

        if (mData != null) {
            for (Integer x = 0; x < mData.size(); x++) {
                LinearLayout textLayout = new LinearLayout(mContext);
                textLayout.setLayoutParams(params);
                textLayout.setTag(x.toString());
                textLayout.layout(15, 5, 0, 0);
                textLayout.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        LinearLayout layout = (LinearLayout) v;
                        String text = (String) ((TextView) layout.getChildAt(0)).getText();
                        String tag = (String) ((TextView) layout.getChildAt(0)).getTag();
                        onRowClick(text, tag);
                        mDialog.cancel();
                    }
                });
                TextView text = (TextView) View.inflate(mContext, R.layout.text_row, null);// new
                                                                                           // TextView(mContext);
                text.setTextSize(20);
                text.setText(mData.get(x));
                if (x < mTags.size()) {
                    text.setTag(mTags.get(x));
                }
                text.setLayoutParams(params);
                textLayout.addView(text);
                mainLayout.addView(textLayout);
                mainLayout.addView(View.inflate(mContext, R.layout.divider, null));
            }
        }

        scrollView.addView(mainLayout);
        builder.setView(scrollView);
        mDialog = builder.create();
        return mDialog;
    }

    private void onRowClick(String text, String tag) {

        mClickListener.onClick(text, tag);
    }

    public interface onGenericDialogClickListener {

        public void onClick(String text, String tag);
    }
}
