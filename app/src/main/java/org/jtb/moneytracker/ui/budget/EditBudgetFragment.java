package org.jtb.moneytracker.ui.budget;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.data.budget.BudgetAccount;
import org.jtb.moneytracker.ui.GenericDialog;
import org.jtb.moneytracker.ui.GenericDialog.onGenericDialogClickListener;
import org.jtb.moneytracker.ui.account.AccountSelectDialog;
import org.jtb.moneytracker.ui.account.AccountSelectedListener;

import java.text.NumberFormat;
import java.util.ArrayList;

public class EditBudgetFragment extends Fragment {

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();
    private Budget mBudget = null;

    // Gui Components
    private EditText mBudgetNameText;
    private TextView mBudgetedText;
    private Button mAddAccountButton;

    private int mSelectedAccountIndex = -1;

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.budget_edit, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        getActivity().setTitle("Edit Budget");

        mController.showAd(view);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mBudgetNameText = (EditText) view.findViewById(R.id.budgetName);
        mBudgetedText = (TextView) view.findViewById(R.id.budgeted);
        mAddAccountButton = (Button) view.findViewById(R.id.addAccount);

        Bundle inputData = getArguments();

        if (inputData != null) {
            Long budgetId = inputData.getLong("BudgetId");
            mBudget = mController.getBudget(budgetId);
        } else {
            mBudget = new Budget("New Budget");
            getActivity().setTitle("New Budget");
        }

        if (mBudget.getId() == 1L) {
            mBudgetNameText.setEnabled(false);
            mAddAccountButton.setVisibility(View.INVISIBLE);
        }

        mBudgetNameText.setTextColor(Color.parseColor("#ffffff"));

        mBudgetNameText.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mBudget.setName(mBudgetNameText.getText().toString());
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        mAddAccountButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                EditBudgetFragment.this.addAccount();
            }
        });

        setData();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.create_account_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_done:
                save();
                return true;
        }
        return false;
    }

    private void setData() {

        mBudgetNameText.setText(mBudget.getName());
        mBudgetedText.setText(NumberFormat.getCurrencyInstance().format(mBudget.getBudgeted()));

        LinearLayout accountLayout = (LinearLayout) getActivity().findViewById(R.id.accountList);
        accountLayout.removeAllViews();

        // Create account list
        ArrayList<BudgetAccount> budgetAccountList = mBudget.getAccounts();
        for (Integer x = 0; x < budgetAccountList.size(); x++) {

            BudgetAccount b = budgetAccountList.get(x);
            LinearLayout layout = (LinearLayout) View.inflate(getActivity(), R.layout.budget_account_edit, null);
            TextView accountNameText = (TextView) layout.findViewById(R.id.accountName);
            EditText accountBudgetedText = (EditText) layout.findViewById(R.id.budgeted);
            accountNameText.setText(b.getName());
            accountBudgetedText.setText(NumberFormat.getCurrencyInstance().format(b.getBudgeted()));
            accountBudgetedText.setTag(x);
            accountBudgetedText.setOnKeyListener(new OnKeyListener() {

                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    EditText textView = (EditText) v;
                    String text = textView.getText().toString();
                    text = text.replaceAll("\\$", "");
                    text = text.replaceAll(",", "");
                    Double budgeted = (text.equals("") ? 0d : Double.parseDouble(text));
                    Integer i = (Integer) textView.getTag();
                    mBudget.getAccounts().get(i).setBudgeted(budgeted);
                    mBudgetedText.setText(NumberFormat.getCurrencyInstance().format(mBudget.getBudgeted()));
                    return false;
                }
            });

            layout.setTag(x);
            layout.setOnLongClickListener(new OnLongClickListener() {

                @Override
                public boolean onLongClick(View v) {
                    if (mBudget.getId() != 1L) {
                        mSelectedAccountIndex = (Integer) v.getTag();
                        showOptionsDialog();
                    }
                    return false;
                }
            });

            accountLayout.addView(layout);
        }
    }

    private void save() {

        mBudget.setName(mBudgetNameText.getText().toString());
        mBudget.save();

        getActivity().getFragmentManager().popBackStack();
    }

    private void addAccount() {

        AccountSelectDialog accountDialog = new AccountSelectDialog(getActivity(), new AccountSelectedListener() {

            @Override
            public void accountSelected(Account account) {
                BudgetAccount budgetAccount = new BudgetAccount(account, 0d);
                EditBudgetFragment.this.mBudget.add(budgetAccount);
                EditBudgetFragment.this.setData();
            }
        });

        accountDialog.addAccountClass(mController.getAccountTypes());
        accountDialog.show();
    }

    private void showOptionsDialog() {

        GenericDialog dialog = new GenericDialog(getActivity());
        dialog.setTitle("Account Options");
        ArrayList<String> data = new ArrayList<String>();
        data.add("Delete");
        dialog.setData(data);
        dialog.setClickListener(new onGenericDialogClickListener() {

            @Override
            public void onClick(String text, String tag) {
                if (text.equals("Delete")) {
                    showDeleteAccountConfirmDialog();
                }
            }
        });

        dialog.create().show();
    }

    private void showDeleteAccountConfirmDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Are you sure you want to delete this account?");

        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                mBudget.remove(mBudget.getAccounts().get(mSelectedAccountIndex));
                EditBudgetFragment.this.setData();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }
}
