package org.jtb.moneytracker.ui.account;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;

public class AccountTypesFragment extends Fragment {

    private ListView mAaccountTypesView;
    private AccountTypesListAdapter mListAdapter;

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_types_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        getActivity().setTitle("Account Types");

        mController.showAd(view);

        mListAdapter = new AccountTypesListAdapter(getView().getContext());

        mAaccountTypesView = (ListView) getView().findViewById(R.id.accountTypesList);
        mAaccountTypesView.setAdapter(this.mListAdapter);
        mAaccountTypesView.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String acctType = (String) mListAdapter.getItem(position);

                mController.showAccountListFragment(getActivity(), acctType);
            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.account_types_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_new:
                mController.showCreateAccountFragment(getActivity());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    // private class NewAccountClickListener implements OnClickListener {
    //
    // public void onClick(View v) {
    //
    // MoneyTrackerController.getInstance().startCreateAccountActivity(
    // AccountTypesActivity.this);
    // }
    // }

}
