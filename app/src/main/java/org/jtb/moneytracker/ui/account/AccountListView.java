/**
 *
 */
package org.jtb.moneytracker.ui.account;

import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.data.account.Account;

import java.text.NumberFormat;
import java.util.List;


public final class AccountListView extends ScrollView {

    private final LinearLayout mainLayout;
    private final List<Account> accountList;
    private final Context context;

    /**
     * Constructor
     *
     * @param context  the view's context
     * @param accountList the list of accounts that the view should display
     */
    public AccountListView(Context context, List<Account> accountList) {
        super(context);

        this.context = context;

        this.mainLayout = new LinearLayout(this.context);
        this.mainLayout.setOrientation(LinearLayout.VERTICAL);

        LayoutParams params = new LayoutParams(android.view.ViewGroup.LayoutParams.MATCH_PARENT,
                android.view.ViewGroup.LayoutParams.WRAP_CONTENT);
        this.setLayoutParams(params);

        // set the account list
        this.accountList = accountList;

        // for each account in the list
        for (int x = 0; x < this.accountList.size(); x++) {

            this.addAccountView(this.accountList.get(x));
        }

        this.addView(this.mainLayout);
    }

    private void update() {

        this.removeAllViews();
        this.mainLayout.removeAllViews();

        // for each account in the list
        for (int x = 0; x < this.accountList.size(); x++) {

            this.addAccountView(this.accountList.get(x));
        }

        this.addView(this.mainLayout);
    }

    /**
     * @param account
     */
    private void addAccountView(Account account) {

        // create a view
        LinearLayout view = (LinearLayout) View.inflate(this.context, R.layout.account_row, null);

        // Get the account view from the view.
        LinearLayout accountView = (LinearLayout) view.findViewById(R.id.accountView);

        // Set the index value to match this.accountList.
        accountView.setTag(this.mainLayout.getChildCount());

        // Get the Account Name TextView object and set data from account.
        TextView accountNameView = (TextView) view.findViewById(R.id.accountName);
        // accountNameView.setText(account.getTypeName() + ":" +
        // account.getName());
        accountNameView.setText(account.getName());

        // Get the Account Balance TextView object and set data from
        // account.
        TextView accountBalanceView = (TextView) view.findViewById(R.id.accountBalance);
        double balance = account.getBalance() + this.getSubAccountBalance(account);
        accountBalanceView.setText(NumberFormat.getCurrencyInstance().format(balance));

        // Check to see if the account is a category.
        if (account.isCategory()) {

            // Get the image to display a category.
            ImageView categoryImage = (ImageView) view.findViewById(R.id.categoryImage);
            LinearLayout categoryView = (LinearLayout) view.findViewById(R.id.categoryView);

            // Set the index value to match this.accountList.
            categoryView.setTag(this.mainLayout.getChildCount());

            // Make the category selection arrow visible.
            categoryImage.setVisibility(View.VISIBLE);
        }

        // add the view to the main view
        this.mainLayout.addView(view);
    }

    /**
     * @param acct
     * @return
     */
    private double getSubAccountBalance(Account acct) {

        // TODO::
        // ArrayList<Account> accounts = dbManager.getSubAccounts(acct.getId());

        // double balance = 0;
        // for (Account a : accounts) {
        // balance += a.getBalance();
        // }

        // return balance;
        return 0;
    }

    /**
     * This method sets the listener to be fired when an Account is clicked.
     *
     * @param listener - the listener to fire.
     */
    public void setOnAccountClickListener(final OnClickListener listener) {

        for (int index = 0; index < this.mainLayout.getChildCount(); index++) {

            // get the item at this index in the view
            View view = this.mainLayout.getChildAt(index);

            // get the account view for this row
            LinearLayout accountView = (LinearLayout) view.findViewById(R.id.accountView);

            // set the on click listener for the account view
            accountView.setOnClickListener(listener);
        }
    }

    /**
     * This method sets the listener to be fired when an Account is long
     * clicked.
     *
     * @param listener the listener to fire.
     */
    public void setOnAccountLongClickListener(final OnLongClickListener listener) {

        for (int index = 0; index < this.mainLayout.getChildCount(); index++) {

            // get the item at this index in the view
            View view = this.mainLayout.getChildAt(index);

            // get the account view for this row
            LinearLayout accountView = (LinearLayout) view.findViewById(R.id.accountView);

            // set the on click listener for the account view
            accountView.setOnLongClickListener(listener);
        }
    }

    /**
     * This method sets the listener to be fired when an Category is clicked.
     *
     * @param listener the listener to fire.
     */
    public void setOnCategoryClickListener(final OnClickListener listener) {

        for (int index = 0; index < this.mainLayout.getChildCount(); index++) {

            // get the item at this index in the view
            View view = this.mainLayout.getChildAt(index);

            // get the category view for this row
            LinearLayout categoryView = (LinearLayout) view.findViewById(R.id.categoryView);

            // set the on click listener for the account view
            categoryView.setOnClickListener(listener);
        }
    }

    /**
     * This method gets the Account represented by an Account view.
     *
     * @param accountView The view.
     * @return Account
     */
    public Account getAccount(View accountView) {

        Account selectedAccount = null;

        try {
            Integer index = (Integer) accountView.getTag();

            selectedAccount = this.accountList.get(index);
        } catch (Exception ex) {
            // TODO add log call
        }

        return selectedAccount;
    }

    /**
     * This method gets the Account at the position in the list.
     *
     * @param position - The index of the desired account.
     * @return Account
     */
    public Account getAccount(int position) {

        Account selectedAccount = null;

        try {
            selectedAccount = this.accountList.get(position);
        } catch (Exception ex) {
            // TODO add log call
        }

        return selectedAccount;
    }

    /**
     * Removes an account from the view
     *
     * @param account - The Account to remove
     */
    public void removeAccount(Account account) {

        this.accountList.remove(account);
        update();
    }

    public void setTextColor(int color) {

        for (int index = 0; index < this.mainLayout.getChildCount(); index++) {

            // get the item at this index in the view
            View view = this.mainLayout.getChildAt(index);

            // Get the Account Name TextView object and set data from account.
            TextView accountNameView = (TextView) view.findViewById(R.id.accountName);
            accountNameView.setTextColor(color);

            // Get the Account Balance TextView object and set data from
            // account.
            TextView accountBalanceView = (TextView) view.findViewById(R.id.accountBalance);
            accountBalanceView.setTextColor(color);

        }
    }

    public int size() {
        return this.accountList.size();
    }

    /**
     * Clears all accounts in this view
     */
    public void clear() {

        this.accountList.clear();
        this.removeAllViews();
    }

    /**
     * Adds an account to the Account view
     *
     * @param acct - The account to view
     */
    public void add(Account acct) {

        this.accountList.add(acct);
        this.addAccountView(acct);
    }
}
