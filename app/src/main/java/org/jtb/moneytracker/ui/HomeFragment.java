package org.jtb.moneytracker.ui;

import android.app.AlertDialog;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.method.LinkMovementMethod;
import android.text.util.Linkify;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.controller.MoneyTrackerEvents;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.account.TransactionSort;
import org.jtb.moneytracker.ui.account.AccountListDialog;
import org.jtb.moneytracker.ui.account.AccountSelectedListener;
import org.jtb.moneytracker.ui.account.AccountTypeDialog;
import org.jtb.moneytracker.ui.account.AccountTypeSelectedListener;
import org.jtb.moneytracker.ui.account.TransactionsListAdapter;

import java.lang.ref.WeakReference;
import java.text.NumberFormat;
import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private final static int MENU_RATE = 0;
    private final static int MENU_ABOUT = 1;

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private Button mSelectedAccountButton;
    private TextView mDisplayTextView;
    private Account mSelectedAccount;
    private ListView mTransactionList;
    private Toolbar mToolbar;

    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        // Thread scheduledTransactionThread = new Thread(new Runnable() {
        //
        // @Override
        // public void run() {
        //
        // mController.autoCreateScheduled();
        // mController.requestNotifyScheduled();
        // }
        // });
        //
        // scheduledTransactionThread.start();
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.home, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        getActivity().setTitle("Home");

        mController.register(new HomeHandler(this));
        mController.showAd(view);

        mToolbar = (Toolbar) view.findViewById(R.id.toolbar_bottom);
        mToolbar.inflateMenu(R.menu.main_activity_menu);
        mToolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                return false;
            }
        });


        //mDepositButton = (Button) view.findViewById(R.id.depositButton);
//        mDepositButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                mController.showTransactionFragment(mSelectedAccount,
//                        Transaction.TRANSACTION_TYPE_DEBIT, getActivity());
//            }
//        });

//        mWithdrawalButton = (Button) view.findViewById(R.id.withdrawalButton);
//        mWithdrawalButton.setOnClickListener(new OnClickListener() {
//
//            @Override
//            public void onClick(View v) {
//                mController.showTransactionFragment(mSelectedAccount,
//                        Transaction.TRANSACTION_TYPE_CREDIT, getActivity());
//            }
//        });

        mSelectedAccount = mController.getSelectedAccount();
        mSelectedAccountButton = (Button) view.findViewById(R.id.selectedAccount);
        mSelectedAccountButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showSelectAccountDialog();
            }
        });

        mDisplayTextView = (TextView) view.findViewById(R.id.displayTextView);
        mTransactionList = (ListView) view.findViewById(R.id.transactionList);

        displaySelectedAccount();
        displayTransactions();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        menu.add(0, MENU_RATE, 0, "Rate");
        menu.add(0, MENU_ABOUT, 1, "About");
        menu.getItem(0).setIcon(android.R.drawable.btn_star);
        menu.getItem(1).setIcon(android.R.drawable.ic_menu_help);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == MENU_RATE) {
            mController.startRateActivity(getActivity());
            return true;
        } else if (item.getItemId() == MENU_ABOUT) {
            showAboutDialog();
            return true;
        }

        return false;
    }

    /**
     * Updates the displayed balance of the currently selected account
     */
    protected void displaySelectedAccount() {

        try {
            mSelectedAccountButton.setText(mSelectedAccount.getTypeName() + ":" + mSelectedAccount.getName());
            mDisplayTextView.setText(NumberFormat.getCurrencyInstance().format(mSelectedAccount.getBalance()));

            String debitButtonText = "";
            String creditButtonText = "";

            if (mSelectedAccount.getClass() == AssetAccount.class || mSelectedAccount.getClass() == BankAccount.class) {
                debitButtonText = "Deposit";
                creditButtonText = "Withdrawal";
            } else if (mSelectedAccount.getClass() == ExpenseAccount.class) {
                debitButtonText = "Expense";
                creditButtonText = "Rebate";
            } else if (mSelectedAccount.getClass() == IncomeAccount.class) {
                debitButtonText = "Charge";
                creditButtonText = "Income";
            } else if (mSelectedAccount.getClass() == LiabilityAccount.class) {
                debitButtonText = "Payment";
                creditButtonText = "Charge";
            }

            //mDepositButton.setText(debitButtonText);
            //mWithdrawalButton.setText(creditButtonText);
        } catch (Exception ex) {

        }
    }

    private void displayTransactions() {
        ArrayList<Transaction> transactions = mController.getTransactions(mSelectedAccount.getId());
        TransactionsListAdapter adapter = new TransactionsListAdapter(getContext(),
                mSelectedAccount.getId(), transactions, new TransactionSort());
        mTransactionList.setAdapter(adapter);
    }

    protected void showSelectAccountDialog() {

        AccountTypeDialog dialog = new AccountTypeDialog(getView().getContext());
        dialog.addAccountClass(AssetAccount.class.toString());
        dialog.addAccountClass(BankAccount.class.toString());
        dialog.addAccountClass(ExpenseAccount.class.toString());
        dialog.addAccountClass(IncomeAccount.class.toString());
        dialog.addAccountClass(LiabilityAccount.class.toString());
        dialog.setAccountTypeSelectedListener(new AccountTypeSelectedListener() {

            @Override
            public void accountTypeChanged(String accountClass) {

                AccountListDialog dialog = new AccountListDialog(getView().getContext());
                dialog.setTitle("Select Account");
                dialog.addAll(mController.getRootAccounts(accountClass));
                dialog.addAccountSelectedListener(new AccountSelectedListener() {

                    @Override
                    public void accountSelected(Account account) {
                        mController.setSelectedAccount(account);
                    }
                });
                dialog.show();
            }
        });
        dialog.show();
    }

    /**
     * Creates the about dialog.
     *
     * @return The dialog.
     */
    private void showAboutDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        View view = View.inflate(getActivity(), R.layout.about_dialog, null);
        TextView googleWebsite = (TextView) view.findViewById(R.id.googleWebsite);
        TextView facebookWebsite = (TextView) view.findViewById(R.id.facebookWebsite);

        try {
            String appVersion = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0).versionName;
            TextView version = (TextView) view.findViewById(R.id.version);
            version.setText("Version " + appVersion);
        } catch (NameNotFoundException e) {
            Log.v(HomeFragment.this.toString(), e.getMessage());
        }

        SpannableString googleText = new SpannableString(getText(R.string.googleUrl));
        Linkify.addLinks(googleText, Linkify.WEB_URLS);

        googleWebsite.setText(googleText);
        googleWebsite.setMovementMethod(LinkMovementMethod.getInstance());

        SpannableString webText = new SpannableString(getText(R.string.facebookUrl));
        Linkify.addLinks(webText, Linkify.WEB_URLS);

        facebookWebsite.setText(webText);
        facebookWebsite.setMovementMethod(LinkMovementMethod.getInstance());

        builder.setTitle("About Money Tracker");
        builder.setCancelable(true);
        builder.setView(view);
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setPositiveButton(android.R.string.ok, null);

        builder.create().show();
    }
    // private void updateNewsArticles(ArrayList<Article> articleList) {
    //
    // mNewsView.removeAllViews();
    //
    // View view = View.inflate(this, R.layout.news_article, null);
    // ImageView imageView = (ImageView) view.findViewById(R.id.image);
    // TextView titleView = (TextView) view.findViewById(R.id.title);
    // TextView updatedView = (TextView) view.findViewById(R.id.updated);
    //
    // for(Article a : articleList) {
    //
    // imageView.setImageBitmap(a.getImage());
    // titleView.setText(a.getTitle());
    // updatedView.setText(a.getUpdated());
    //
    // mNewsView.addView(view);
    // }
    // }

    private static class HomeHandler extends Handler {

        private final WeakReference<HomeFragment> mFragment;

        public HomeHandler(HomeFragment homeFragment) {
            mFragment = new WeakReference<HomeFragment>(homeFragment);
        }

        @Override
        public void handleMessage(Message message) {

            switch (message.what) {

                case MoneyTrackerEvents.MAIN_ACCOUNT:
                    mFragment.get().mSelectedAccount = (Account) message.obj;
                    mFragment.get().displaySelectedAccount();
                    mFragment.get().displayTransactions();
                    break;

                case MoneyTrackerEvents.TRANSACTION_CREATED:
                case MoneyTrackerEvents.TRANSACTION_DELETED:
                case MoneyTrackerEvents.TRANSACTION_UPDATED:
                    mFragment.get().mSelectedAccount = mFragment.get().mController.getSelectedAccount();
                    mFragment.get().displaySelectedAccount();
                    mFragment.get().displayTransactions();
                    break;

                case MoneyTrackerEvents.NOTIFY_SCHEDULED:
                    //mNotifyScheduled.setText(((Integer) message.obj).toString()
                    //        + " scheduled transaction(s) need to be created.");
                    break;

                case MoneyTrackerEvents.NOTIFY_SCHEDULED_CREATED:
                    //mNotifyCreated.setText(((Integer) message.obj).toString() + " transaction(s) "
                    //        + " automatically created.");
                    break;

                case MoneyTrackerEvents.NEWS_UPDATED:
                    // updateNewsArticles((ArrayList<Article>) message.obj);
                    break;
            }
        }
    }
}
