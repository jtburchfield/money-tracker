/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.jtb.moneytracker.ui.account;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.CreditAccount;
import org.jtb.moneytracker.data.account.DebitAccount;
import org.jtb.moneytracker.data.account.NewestFirstTransactionSort;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.account.TransactionSort;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Locale;

/**
 * @author jason
 */
public class TransactionsListAdapter extends ArrayAdapter<Transaction> {

    private final int mAccountId;
    private final ArrayList<Transaction> mTransactions;
    private final ArrayList<Double> mBalances;

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    public TransactionsListAdapter(Context context, int accountId, ArrayList<Transaction> transactions,
                                   TransactionSort transactionSort) {
        super(context, accountId, transactions);

        mAccountId = accountId;
        mTransactions = transactions;
        mBalances = new ArrayList<>();

        Account acct = mController.getAccount(mAccountId);
        Collections.sort(mTransactions, new TransactionSort());

        double totalSum = 0;
        for (int x = 0; x < mTransactions.size(); x++) {
            Transaction transaction = mTransactions.get(x);
            if (transaction != null) {
                if (transaction.getCreditAccountId() == mAccountId) {
                    if (acct instanceof CreditAccount) {
                        totalSum += transaction.getAmount();
                    } else {
                        totalSum -= transaction.getAmount();
                    }
                } else {
                    if (acct instanceof DebitAccount) {
                        totalSum += transaction.getAmount();
                    } else {
                        totalSum -= transaction.getAmount();
                    }
                }
            }

            mBalances.add(totalSum);
        }

        if (transactionSort instanceof NewestFirstTransactionSort) {
            Collections.reverse(mTransactions);
            Collections.reverse(mBalances);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {
            v = LayoutInflater.from(getContext()).inflate(R.layout.transaction_row, parent, false);
        }

        Transaction t = mTransactions.get(position);

        if (t != null) {
            TextView dateView = (TextView) v.findViewById(R.id.dateText);
            TextView accountView = (TextView) v.findViewById(R.id.accountText);
            TextView descView = (TextView) v.findViewById(R.id.descText);
            TextView amountView = (TextView) v.findViewById(R.id.amountText);
            TextView balanceView = (TextView) v.findViewById(R.id.balanceText);
            ImageView imageView = (ImageView) v.findViewById(R.id.icon);

            dateView.setText(new SimpleDateFormat(MoneyTrackerController.getDateFormat(), Locale.US)
                    .format(t.getDate()));
            descView.setText(t.getDescription());

            Account account = mController.getAccount(mAccountId);
            String accountName;
            Account debitAcct = mController.getAccount(t.getDebitAccountId());
            Account creditAcct = mController.getAccount(t.getCreditAccountId());

            if (t.getCreditAccountId() == mAccountId) {
                if (account instanceof CreditAccount) {
                    amountView.setText(NumberFormat.getCurrencyInstance().format(t.getAmount()));
                    accountName = debitAcct.getTypeName() + ":" + debitAcct.getName();
                    accountView.setText(accountName);
                    imageView.setImageResource(R.drawable.up);
                    balanceView.setText(NumberFormat.getCurrencyInstance().format(mBalances.get(position)));
                } else {
                    amountView.setText("(" + NumberFormat.getCurrencyInstance().format(t.getAmount()) + ")");
                    amountView.setTextColor(Color.RED);
                    accountName = debitAcct.getTypeName() + ":" + debitAcct.getName();
                    accountView.setText(accountName);
                    imageView.setImageResource(R.drawable.down);
                    balanceView.setText(NumberFormat.getCurrencyInstance().format(mBalances.get(position)));
                }
            } else {
                if (account instanceof DebitAccount) {
                    amountView.setText(NumberFormat.getCurrencyInstance().format(t.getAmount()));
                    accountName = creditAcct.getTypeName() + ":" + creditAcct.getName();
                    accountView.setText(accountName);
                    imageView.setImageResource(R.drawable.up);
                    balanceView.setText(NumberFormat.getCurrencyInstance().format(mBalances.get(position)));
                } else {
                    amountView.setText("(" + NumberFormat.getCurrencyInstance().format(t.getAmount()) + ")");
                    amountView.setTextColor(Color.RED);
                    accountName = creditAcct.getTypeName() + ":" + creditAcct.getName();
                    accountView.setText(accountName);
                    imageView.setImageResource(R.drawable.down);
                    balanceView.setText(NumberFormat.getCurrencyInstance().format(mBalances.get(position)));
                }
            }

            // dateView.setTextSize(12);
            // accountView.setTextSize(12);
            // descView.setTextSize(12);
            // amountView.setTextSize(12);
            // balanceView.setTextSize(14);

            try {

                if (mBalances.get(position) < 0) {
                    balanceView.setTextColor(Color.RED);
                }
                // else {
                // balanceView.setTextColor(Color.WHITE);
                // }
            } catch (Exception e) {
                Log.e(toString(), e.getMessage());
            }
            // balanceView.setBackgroundColor(Color.GREEN);
        }

        return v;
    }
}
