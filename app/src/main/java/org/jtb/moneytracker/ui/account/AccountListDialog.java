/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.ui.account;

import java.util.ArrayList;
import java.util.List;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.data.account.Account;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;

/**
 * 
 * @author jason
 */
public class AccountListDialog {

    private final Context mContext;
    private final List<Account> mAccountList;
    private final List<AccountSelectedListener> mChangeListeners;
    private Dialog mAccountListDialog;
    private String mTitle;

    public AccountListDialog(Context context) {

        mContext = context;
        mAccountList = new ArrayList<Account>();
        mChangeListeners = new ArrayList<AccountSelectedListener>();
    }

    public void setTitle(String title) {

        mTitle = title;
    }

    public void show() {

        createAccountListDialog();
        mAccountListDialog.show();
    }

    public void add(Account account) {

        mAccountList.add(account);
    }

    public void addAll(List<Account> accounts) {

        mAccountList.addAll(accounts);
    }

    public void delete(Account account) {

        for (int x = 0; x < mAccountList.size(); x++) {
            if (mAccountList.get(x).getId() == account.getId()) {
                mAccountList.remove(mAccountList.get(x));
            }
        }
    }
    
    public void deleteAll(List<Account> accounts) {
        
        for(Account a : accounts) {
            delete(a);
        }
    }

    public void addAccountSelectedListener(AccountSelectedListener listener) {

        mChangeListeners.add(listener);
    }

    private void updateAccount(Account account) {

        notifyAccountSelected(account);
        mAccountListDialog.cancel();
    }

    private void notifyAccountSelected(Account account) {

        for (int i = 0; i < mChangeListeners.size(); ++i) {

            mChangeListeners.get(i).accountSelected(account);
        }
    }

    private void selectCategory(Account category) {

        if(category != null) {
            mAccountListDialog.cancel();
            mAccountList.clear();
            mAccountList.addAll(category.getSubAccounts());
            createAccountListDialog();
            mAccountListDialog.show();
        }
    }

    private void createAccountListDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle(mTitle);
        builder.setCancelable(true);

        AccountListView view = new AccountListView(mContext, mAccountList);

        view.setOnAccountClickListener(new UpdateSelectedAccountClickListener(view));
        view.setOnCategoryClickListener(new CategoryClickListener(view));

        builder.setView(view);
        builder.setIcon(R.drawable.money_tracker_main);

        mAccountListDialog = builder.create();
    }

    /**
     * 
     * @author jason
     */
    private class UpdateSelectedAccountClickListener implements OnClickListener {

        private final AccountListView accountListView;

        UpdateSelectedAccountClickListener(AccountListView view) {

            accountListView = view;
        }

        public void onClick(View v) {

            Account account = accountListView.getAccount(v);
            AccountListDialog.this.updateAccount(account);
        }
    }

    /**
     * Used to handle when a category is clicked
     */
    private class CategoryClickListener implements OnClickListener {

        private final AccountListView accountListView;

        public CategoryClickListener(AccountListView view) {

            accountListView = view;
        }

        public void onClick(View v) {

            Account account = accountListView.getAccount(v);
            AccountListDialog.this.selectCategory(account);
        }
    }
}
