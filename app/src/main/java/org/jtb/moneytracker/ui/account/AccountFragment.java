package org.jtb.moneytracker.ui.account;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AccountFactory;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;

import java.util.List;

/**
 * @author jason
 */
public abstract class AccountFragment extends Fragment {

    protected MoneyTrackerController mController = MoneyTrackerController.getInstance();

    protected Account mAccount;

    private Button mAccountTypesButton;
    private EditText mAccountNameText;
    private Button mCategoryButton;
    private Button mClearCategoryButton;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.account_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mController.showAd(view);
        mAccount = new AssetAccount("", 0.0);

        mAccountTypesButton = (Button) view.findViewById(R.id.newAccountType);
        mAccountTypesButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showAccountTypeDialog();
            }
        });

        mCategoryButton = (Button) view.findViewById(R.id.parentAccount);
        mCategoryButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showCategoryDialog();
            }
        });

        mClearCategoryButton = (Button) view.findViewById(R.id.clear);
        mClearCategoryButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                clearCategory();
            }
        });

        mAccountNameText = (EditText) view.findViewById(R.id.newAccountName);
        mAccountNameText.addTextChangedListener(new TextWatcher() {

            public void afterTextChanged(Editable arg0) {
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
                mAccount.setName(mAccountNameText.getText().toString());
            }
        });

        // Populate the parent account
        setAccountType(AssetAccount.class.toString());
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

        menu.clear();
        inflater.inflate(R.menu.create_account_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_save:
                if (onDoneClicked()) {
                    getActivity().getFragmentManager().popBackStack();
                    //((MainActivity) getActivity()).closeKeyboard();
                }
                return true;

            case R.id.action_cancel:
                getActivity().getFragmentManager().popBackStack();
                //((MainActivity) getActivity()).closeKeyboard();
                break;
        }

        return false;
    }

    protected boolean onDoneClicked() {
        return false;
    }

    protected void setData() {

        setAccountType(mAccount.getClass().toString(), false);
        mAccountNameText.setText(mAccount.getName());
    }

    protected void setAccountTypesEnabled(boolean enable) {

        mAccountTypesButton.setEnabled(enable);
    }

    protected void setAccountType(String type) {

        setAccountType(type, true);
    }

    protected void setCategory(Account category) {

        if (category == null) {
            mAccount.setParentId(0);
            mCategoryButton.setText("None");
            mClearCategoryButton.setVisibility(View.GONE);
        } else {
            mAccount.setParentId(category.getId());
            mCategoryButton.setText(category.getName());
            mClearCategoryButton.setVisibility(View.VISIBLE);
        }
    }

    protected void clearCategory() {

        setCategory(null);
    }

    protected void showAccountNameErrorDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setTitle("Error");
        builder.setMessage("Account Name field cannot be empty.");
        builder.setCancelable(false);
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });
        builder.create().show();
    }

    private void setAccountType(String className, boolean createNew) {

        if (createNew) {
            try {
                mAccount = AccountFactory.getInstance().createAccount(mAccountNameText.getText().toString(), 0,
                        className);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }

        mAccountTypesButton.setText(MoneyTrackerController.getInstance().getShortAccountTypeName(className));
        setCategory(mController.getAccount(mAccount.getParentId()));
    }

    private void showAccountTypeDialog() {

        AccountTypeDialog dialog = new AccountTypeDialog(getActivity(), new AccountTypeSelectedListener() {

            @Override
            public void accountTypeChanged(String className) {
                setAccountType(className);
            }
        });
        dialog.addAccountClass(AssetAccount.class.toString());
        dialog.addAccountClass(BankAccount.class.toString());
        dialog.addAccountClass(ExpenseAccount.class.toString());
        dialog.addAccountClass(IncomeAccount.class.toString());
        dialog.addAccountClass(LiabilityAccount.class.toString());
        dialog.show();
    }

    private void showCategoryDialog() {

        List<Account> accounts = MoneyTrackerController.getInstance().getRootAccounts(mAccount.getClass().toString());

        AccountListDialog dialog = new AccountListDialog(getActivity());
        dialog.setTitle("Select Category");
        dialog.addAll(accounts);
        dialog.delete(mAccount);
        dialog.addAccountSelectedListener(new AccountSelectedListener() {

            public void accountSelected(Account account) {
                setCategory(account);
            }
        });

        dialog.show();
    }
}
