package org.jtb.moneytracker.ui.account;

import android.app.DatePickerDialog;
import android.app.DatePickerDialog.OnDateSetListener;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.InputFilter;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView.BufferType;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;
import org.jtb.moneytracker.data.account.Transaction;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Formatter;
import java.util.List;
import java.util.Locale;

public class TransactionFragment extends Fragment {

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private Button mSecondaryAccountButton;
    private Button mDateButton;
    private EditText mAmountText;
    private EditText mDescriptionText;
    private Account mSelectedAccount;
    private Account mSecondaryAccount;
    private int mTransactionType;
    private long mTransactionId;
    private Transaction mTransaction;
    private int mMonth;
    private int mDay;
    private int mYear;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.transaction_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mController.showAd(view);

        mSelectedAccount = mController.getAccount(getArguments().getInt("Account"));
        mTransactionId = getArguments().getLong("Transaction", -1);
        mTransactionType = getArguments().getInt("TransactionType");

        // Set the title text
        if (mTransactionType == Transaction.TRANSACTION_TYPE_DEBIT) {

            if (mSelectedAccount.getClass() == LiabilityAccount.class) {
                getActivity().setTitle(getString(R.string.paymentText) + " - " + mSelectedAccount.getName());
            } else {
                getActivity().setTitle(getString(R.string.depositText) + " - " + mSelectedAccount.getName());
            }
        } else {
            if (mSelectedAccount.getClass() == LiabilityAccount.class) {
                getActivity().setTitle(getString(R.string.chargeText) + " - " + mSelectedAccount.getName());
            } else {
                getActivity().setTitle(getString(R.string.withdrawalText) + " - " + mSelectedAccount.getName());
            }
        }

        mSecondaryAccountButton = (Button) view.findViewById(R.id.accountButton);
        mSecondaryAccountButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showSecondaryAccountDialog();
            }
        });

        mAmountText = (EditText) view.findViewById(R.id.transactionAmountText);
        mAmountText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);

        // Set the input filter for the amount text box
        InputFilter[] filters = new InputFilter[2];
        filters[0] = DigitsKeyListener.getInstance(false, true);
        filters[1] = new InputFilter.LengthFilter(10);

        mAmountText.getText().setFilters(filters);

        mDescriptionText = (EditText) view.findViewById(R.id.transactionDescriptionText);

        mDateButton = (Button) view.findViewById(R.id.transactionDateButton);
        mDateButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        List<Account> accounts = mController.getRootAccounts(ExpenseAccount.class.toString());
        accounts.remove(mSelectedAccount.getName());
        setSecondaryAccount(accounts.get(0));

        Calendar calendar = Calendar.getInstance();

        // Set the title for an edit transaction.
        // Get the transaction and set the information on display.
        if (isEditTransaction()) {

            getActivity().setTitle("Edit " + getActivity().getTitle());

            mTransaction = mController.getTransaction(mTransactionId);

            StringBuilder sb = new StringBuilder();
            Formatter f = new Formatter(sb, Locale.US);
            f.format("%.2f", mTransaction.getAmount());
            mAmountText.setText(f.toString(), BufferType.EDITABLE);
            f.close();

            mDescriptionText.setText(mTransaction.getDescription());

            if (mTransactionType == Transaction.TRANSACTION_TYPE_DEBIT) {
                setSecondaryAccount(mController.getAccount(mTransaction.getCreditAccountId()));
            } else {
                setSecondaryAccount(mController.getAccount(mTransaction.getDebitAccountId()));
            }

            Date date = mTransaction.getDate();
            calendar.setTime(date);
        }

        mMonth = calendar.get(Calendar.MONTH) + 1;
        mDay = calendar.get(Calendar.DAY_OF_MONTH);
        mYear = calendar.get(Calendar.YEAR);

        // Display the current date.
        updateDateDisplay();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.transaction_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle presses on the action bar items
        switch (item.getItemId()) {
            case R.id.action_save:
                saveTransaction();
                return true;
            case R.id.action_cancel:
                closeFragment();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setSecondaryAccount(Account account) {

        String text = account.getTypeName() + ":" + account.getName();
        mSecondaryAccount = account;
        mSecondaryAccountButton.setText(text);
    }

    private void showSecondaryAccountDialog() {

        AccountSelectDialog dialog = new AccountSelectDialog(getActivity(), new AccountSelectedListener() {

            @Override
            public void accountSelected(Account account) {
                setSecondaryAccount(account);
            }
        });
        dialog.addAccountClass(AssetAccount.class.toString());
        dialog.addAccountClass(BankAccount.class.toString());
        dialog.addAccountClass(ExpenseAccount.class.toString());
        dialog.addAccountClass(IncomeAccount.class.toString());
        dialog.addAccountClass(LiabilityAccount.class.toString());
        dialog.removeAccount(mSelectedAccount);
        dialog.show();
    }

    // updates the date we display in the TextView
    private void updateDateDisplay() {

        // Month is 0 based so add 1
        mDateButton.setText(new StringBuilder().append(mMonth).append("-").append(mDay).append("-").append(mYear)
                .append(" "));
    }

    private void saveTransaction() {

        if (mAmountText.getText() != null && !mAmountText.getText().toString().equals("")
                && !mAmountText.getText().toString().equals(".")) {

            String amount = mAmountText.getText().toString();

            int dotPos = amount.indexOf(".");
            if (dotPos != -1 && dotPos < amount.length() - 3) {
                amount = amount.substring(0, dotPos + 3);
            }

            int debitAccountId;
            int creditAccountId;

            if (mTransactionType == Transaction.TRANSACTION_TYPE_DEBIT) {

                debitAccountId = mSelectedAccount.getId();
                creditAccountId = mSecondaryAccount.getId();
            } else {

                debitAccountId = mSecondaryAccount.getId();
                creditAccountId = mSelectedAccount.getId();
            }

            Date date = null;
            try {
                date = new SimpleDateFormat(MoneyTrackerController.getDateFormat(), Locale.US)
                        .parse(mDateButton.getText().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }

            Transaction transaction = new Transaction(debitAccountId, creditAccountId,
                    Double.valueOf(amount), mDescriptionText.getText().toString(), date);

            if (isEditTransaction()) {
                mController.updateTransaction(mTransaction, transaction);
            } else {
                mController.insertTransaction(transaction);
            }
        }

        closeFragment();
    }

    private void closeFragment() {
        getActivity().getSupportFragmentManager().popBackStack();
        //((MainActivity) getActivity()).closeKeyboard();
    }

    private void showDatePickerDialog() {

        DatePickerDialog dialog = new DatePickerDialog(getActivity(), new OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                mYear = year;
                mMonth = monthOfYear + 1;
                mDay = dayOfMonth;
                updateDateDisplay();
            }
        }, mYear, mMonth - 1, mDay);

        dialog.show();
    }

    private boolean isEditTransaction() {

        return mTransactionId != -1;
    }
}
