package org.jtb.moneytracker.ui.budget;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.data.budget.BudgetAccount;
import org.jtb.moneytracker.data.budget.BudgetListener;
import org.jtb.moneytracker.ui.GenericDialog;

import java.text.NumberFormat;
import java.util.ArrayList;

public class BudgetFragment extends Fragment {

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private FragmentActivity mActivity;
    private Button mBudgetNameButton;
    private LinearLayout mAccountList;
    private ProgressBar mProgressBar;
    private TextView mBudgeted;
    private TextView mRemaining;
    private boolean mCanDelete;

    private Budget mBudget;

    private final BudgetListener mHandler = new BudgetListener() {

        @Override
        public void handleMessage(Message message) {

            switch (message.what) {

                case BudgetListener.BUDGET_UPDATED:
                case BudgetListener.BUDGET_SAVED:
                    mBudget = mController.getSelectedBudget();
                    setData();
                    break;
            }
        }
    };

    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setHasOptionsMenu(true);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = (FragmentActivity) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.budget, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        mActivity.setTitle("Budget");

        mController.showAd(view);
        mController.createDefaultBudget();
        Budget.register(mHandler);

        mBudget = mController.getSelectedBudget();

        mBudgetNameButton = (Button) view.findViewById(R.id.budgetName);
        mAccountList = (LinearLayout) view.findViewById(R.id.accountList);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressBar);
        mBudgeted = (TextView) view.findViewById(R.id.budgeted);
        mRemaining = (TextView) view.findViewById(R.id.remaining);

        mBudgetNameButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                onBudgetNameClicked();
            }
        });

        setData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Budget.unregister(mHandler);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.budget_menu, menu);

        if (mCanDelete) {
            menu.findItem(R.id.deleteBudget).setVisible(true);
        } else {
            menu.findItem(R.id.deleteBudget).setVisible(false);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.newBudget:
                createBudget();
                return true;
            case R.id.editBudget:
                editBudget();
                return true;
            case R.id.deleteBudget:
                showDeleteConfirmDialog();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void setData() {

        if (mBudget.getId() == 1L) {
            mCanDelete = false;
        } else {
            mCanDelete = true;
        }

        mActivity.invalidateOptionsMenu();

        Drawable greenProgressBar = getResources().getDrawable(R.drawable.green_progress_bar);
        Drawable yellowProgressBar = getResources().getDrawable(R.drawable.yellow_progress_bar);
        Drawable redProgressBar = getResources().getDrawable(R.drawable.red_progress_bar);

        mProgressBar.setProgressDrawable(greenProgressBar);
        mBudgeted.setText(NumberFormat.getCurrencyInstance().format(mBudget.getBudgeted()));
        mRemaining.setText(NumberFormat.getCurrencyInstance().format(mBudget.getRemaining()));
        mProgressBar.setProgress(mBudget.getProgress());

        mBudgetNameButton.setText(mBudget.getName());

        ArrayList<BudgetAccount> accounts = mBudget.getAccounts();

        mAccountList.removeAllViews();

        RelativeLayout layout = null;
        TextView textView = null;
        TextView budgeted = null;
        TextView remaining = null;
        ProgressBar progress = null;

        for (BudgetAccount b : accounts) {

            layout = (RelativeLayout) View.inflate(mActivity, R.layout.budget_account, null);

            textView = (TextView) layout.findViewById(R.id.name);
            textView.setText(b.getName());

            budgeted = (TextView) layout.findViewById(R.id.budgeted);
            budgeted.setText(NumberFormat.getCurrencyInstance().format(b.getBudgeted()));

            remaining = (TextView) layout.findViewById(R.id.remaining);
            remaining.setText(NumberFormat.getCurrencyInstance().format(b.getRemaining()));

            progress = (ProgressBar) layout.findViewById(R.id.progressBar);
            progress.setProgress(b.getProgress());

            if (b.getProgress() <= 50) {
                progress.setProgressDrawable(greenProgressBar);
            } else if (b.getProgress() <= 75) {
                progress.setProgressDrawable(yellowProgressBar);
            } else {
                progress.setProgressDrawable(redProgressBar);
            }
            mAccountList.addView(layout);
        }

        mAccountList.addView(View.inflate(mActivity, R.layout.divider, null));
        mController.setSelectedBudget(mBudget);
    }

    private void onBudgetNameClicked() {
        ArrayList<Budget> budgetList = mController.getBudgets();

        ArrayList<String> data = new ArrayList<String>();
        ArrayList<String> tags = new ArrayList<String>();

        for (Budget b : budgetList) {
            data.add(b.getName());
            tags.add(b.getId().toString());
        }

        GenericDialog dialog = new GenericDialog(mActivity);
        dialog.setTitle("Select Budget");
        dialog.setData(data);
        dialog.setTags(tags);
        dialog.setClickListener(new BudgetSelectedClickListener());
        dialog.display();
    }

    private void createBudget() {
        mController.showCreateBudgetFragment(mActivity);
    }

    private void editBudget() {
        mController.showEditBudgetFragment(mActivity, mBudget.getId());
    }

    private void showDeleteConfirmDialog() {

        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle("Delete?");
        builder.setIcon(R.drawable.money_tracker_main);
        builder.setMessage("Are you sure you want to delete this budget?");
        builder.setCancelable(false);
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                mBudget.delete();
                mBudget = mController.getDefaultBudget();
                setData();
            }
        });
        builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int id) {
                dialog.cancel();
            }
        });

        builder.create().show();
    }

    private class BudgetSelectedClickListener implements GenericDialog.onGenericDialogClickListener {

        @Override
        public void onClick(String text, String tag) {
            mBudgetNameButton.setText(text);
            mBudget = Budget.get(Long.parseLong(tag));
            setData();
        }
    }
}
