package org.jtb.moneytracker.ui.schedule;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.controller.MoneyTrackerEvents;
import org.jtb.moneytracker.data.schedule.ScheduledTransaction;

import java.util.ArrayList;
import java.util.Collections;

public class ScheduleListFragment extends Fragment {

    private MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private ArrayList<ScheduledTransaction> mTransactions;
    private ScheduledTransactionsListAdapter mTransactionAdapter;
    private int mSelectedItem = 0;

    private final Handler mHandler = new Handler() {

	public void handleMessage(Message message) {

	    switch (message.what) {

	    case MoneyTrackerEvents.SCHEDULED_TRANSACTION_UPDATED:
		updateSchedule();
		break;
	    }
	}
    };

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle icicle) {
	super.onCreate(icicle);

	mController.register(mHandler);
	setHasOptionsMenu(true);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	return inflater.inflate(R.layout.schedule_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

	getActivity().setTitle("Scheduled Transactions");
	
	mController.showAd(view);

	mTransactions = mController.getScheduledTransactions();
	Collections.sort(mTransactions);

	mTransactionAdapter = new ScheduledTransactionsListAdapter(getActivity(), 0, mTransactions);

	ListView list = (ListView) view.findViewById(R.id.rssListView);
	list.setAdapter(mTransactionAdapter);

	list.setOnItemClickListener(new OnItemClickListener() {

	    public void onItemClick(AdapterView<?> arg0, View v, int pos, long id) {
		mSelectedItem = pos;
		editSelectedTransaction();
	    }
	});

	list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

	    public boolean onItemLongClick(AdapterView<?> arg0, View v, int pos, long id) {
		mSelectedItem = pos;
		showMenuDialog();
		return false;
	    }
	});
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {

	menu.clear();
	inflater.inflate(R.menu.schedule_list_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

	switch (item.getItemId()) {
	case R.id.action_new:
	    scheduleTransaction();
	    return true;
	}
	return false;
    }

    /**
     * Schedule a new transaction in the database.
     */
    private void scheduleTransaction() {

	mController.showScheduleFragment(getActivity());
    }

    private void showConfirmDeleteDialog() {

	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	builder.setTitle("Delete?");
	builder.setIcon(R.drawable.money_tracker_main);
	builder.setMessage("Delete the selected scheduled transaction?");
	builder.setCancelable(false);

	builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

	    public void onClick(DialogInterface dialog, int x) {
		deleteSelectedTransaction();
	    }
	});

	builder.setNegativeButton("No", new DialogInterface.OnClickListener() {

	    public void onClick(DialogInterface dialog, int x) {
		dialog.cancel();
	    }
	});

	builder.create().show();
    }

    private void showMenuDialog() {

	AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
	builder.setTitle("Select an option");
	builder.setIcon(R.drawable.money_tracker_main);
	ArrayList<String> items = new ArrayList<String>();
	items.add(getString(R.string.delete));
	items.add(getString(R.string.create));
	ArrayAdapter<String> optionsAdapter = new ArrayAdapter<String>(getActivity(),
		android.R.layout.select_dialog_item, items);
	builder.setAdapter(optionsAdapter, new DialogInterface.OnClickListener() {

	    public void onClick(DialogInterface arg0, int arg1) {
		menuSelectedClicked(arg1);
	    }
	});

	builder.create().show();
    }

    /**
     * Delete a transaction from the database.
     */
    private void deleteSelectedTransaction() {

	ScheduledTransaction item = mTransactions.get(mSelectedItem);

	mController.deleteScheduledTransaction(item.getId());
	updateSchedule();
    }

    /**
     * Populates the Schedule Transaction list Activity.
     */
    private void updateSchedule() {

	mTransactions = mController.getScheduledTransactions();
	Collections.sort(mTransactions);
	mTransactionAdapter.clear();
	mTransactionAdapter.addAll(mTransactions);
    }

    private void editSelectedTransaction() {

	// Get both accounts in transaction
	ScheduledTransaction selectedTransaction = mTransactions.get(mSelectedItem);
	mController.showEditScheduleFragment(getActivity(), selectedTransaction.getId());
    }

    private void menuSelectedClicked(int arg1) {
	int a = arg1;

	switch (a) {
	case 0:
	    showConfirmDeleteDialog();
	    break;
	case 1:
	    createTransaction(mSelectedItem);
	    break;
	}
    }

    private void createTransaction(int selectedItem) {
	
	ScheduledTransaction item = mTransactionAdapter.getItem(selectedItem);
	mController.insertTransaction(item);
	updateSchedule();
    }
}
