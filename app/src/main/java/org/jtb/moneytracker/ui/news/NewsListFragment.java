package org.jtb.moneytracker.ui.news;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.controller.MoneyTrackerEvents;
import org.jtb.moneytracker.newsgofer.Article;
import org.jtb.moneytracker.ui.news.ArticleAdapter.ArticleHolder;

public class NewsListFragment extends Fragment {

    private MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private Activity mActivity;
    private ListView mNewsList;
    private View mLoadingView;

    private final Handler mHandler = new Handler() {

        public void handleMessage(Message message) {

            switch (message.what) {

                case MoneyTrackerEvents.NEWS_UPDATED:
                    updateArticles((Article[]) message.obj);
                    break;
            }
        }
    };

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        mController.register(mHandler);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.news_list_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {

        getActivity().setTitle("News");

        mController.showAd(view);

        mLoadingView = view.findViewById(R.id.loadingPanel);

        mNewsList = (ListView) view.findViewById(R.id.list);
        mNewsList.setOnItemClickListener(new OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                showArticle((ArticleHolder) view.getTag());
            }
        });

        mController.getArticles(20);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    private void updateArticles(Article[] articles) {

        mLoadingView.setVisibility(View.GONE);

        if (articles.length > 0) {
            mNewsList.setAdapter(new ArticleAdapter(mActivity, R.layout.news_article_row, articles));
        } else {
            Toast.makeText(mActivity, "Error loading articles.", Toast.LENGTH_SHORT).show();
        }
    }

    private void showArticle(ArticleHolder holder) {

        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(holder.url));
        startActivity(browserIntent);
    }
}
