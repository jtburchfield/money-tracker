/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.ui.account;

import java.util.ArrayList;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.controller.MoneyTrackerController;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

/**
 * 
 * @author jason
 */
class AccountTypesListAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayList<String> mAccountTypes;
    private final ArrayList<String> mAccountShortTypes;
    private final ArrayList<String> mAccountTypeBalances;

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    public AccountTypesListAdapter(Context context) {

        mContext = context;

        mAccountTypes = new ArrayList<String>();
        mAccountTypes.addAll(mController.getAccountTypeNames());

        mAccountShortTypes = new ArrayList<String>();
        mAccountShortTypes.addAll(mController.getShortAccountTypeNames());
        
        mAccountTypeBalances = new ArrayList<String>();
        mAccountTypeBalances.addAll(mController.getAccountTypeBalances());
    }

    public int getCount() {
        return mAccountTypes.size();
    }

    public Object getItem(int arg0) {
        return mAccountTypes.get(arg0);
    }

    public long getItemId(int arg0) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        if (v == null) {

            v = LayoutInflater.from(mContext).inflate(R.layout.account_type_row, parent, false);
        }        

        if (position < mAccountShortTypes.size()) {

            String type = mAccountShortTypes.get(position);
            TextView accountType = (TextView) v.findViewById(R.id.accountType);
            accountType.setText(type);
        }

        if (position < mAccountTypeBalances.size()) {
            
            String balance = mAccountTypeBalances.get(position);
            TextView typeBalance = (TextView) v.findViewById(R.id.balance);
            typeBalance.setText(balance);
        }
        return v;
    }
}
