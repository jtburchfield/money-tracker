package org.jtb.moneytracker.ui;

import java.util.List;

import org.jtb.moneytracker.R;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MenuAdapter extends ArrayAdapter<DrawerButton> {
    
    private int mLayoutResourceId;
    private List<DrawerButton> mItems;

    public MenuAdapter(Context context, int textViewResourceId, List<DrawerButton> items) {
	super(context, textViewResourceId, items);

	mLayoutResourceId = textViewResourceId;
	mItems = items;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	View row = convertView;
	ItemHolder holder = null;

	if (row == null) {
	    LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
	    row = inflater.inflate(mLayoutResourceId, parent, false);

	    holder = new ItemHolder();
	    //holder.image = (ImageView) row.findViewById(R.id.image);
	    holder.text = (TextView) row.findViewById(R.id.text);
	    
	    row.setTag(holder);
	} else {
	    holder = (ItemHolder) row.getTag();
	}

	DrawerButton item = mItems.get(position);
	holder.text.setText(item.getText());
	//holder.image.setImageDrawable(item.getImage());

	return row;
    }

    static class ItemHolder {
	//ImageView image;
	TextView text;
    }

}
