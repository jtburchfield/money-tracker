package org.jtb.moneytracker.ui.account;

import java.util.ArrayList;
import java.util.List;

import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;

import android.content.Context;

public class AccountSelectDialog {

    private AccountSelectedListener mListener = null;
    private Context mContext = null;
    private List<String> mAccountClassList = new ArrayList<String>();
    private List<Account> mRemoveAccountList = new ArrayList<Account>();

    @SuppressWarnings("unused")
    private AccountSelectDialog() {

    }

    public AccountSelectDialog(final Context context, final AccountSelectedListener listener) {

	mContext = context;
	mListener = listener;
    }

    public void show() {

	AccountTypeDialog dialog = new AccountTypeDialog(mContext, new AccountTypeSelectedListener() {

	    @Override
	    public void accountTypeChanged(String accountClass) {

		AccountListDialog dialog = new AccountListDialog(mContext);
		dialog.addAccountSelectedListener(new AccountSelectedListener() {

		    @Override
		    public void accountSelected(Account account) {
			mListener.accountSelected(account);
		    }
		});
		dialog.setTitle("Select Account");
		dialog.addAll(MoneyTrackerController.getInstance().getRootAccounts(accountClass));
		dialog.deleteAll(mRemoveAccountList);
		dialog.show();
	    }
	});

	dialog.addAccountClass(mAccountClassList);
	dialog.show();
    }

    public void removeAccount(final Account account) {

	mRemoveAccountList.add(account);
    }

    public void addAccountClass(String accountClass) {

	mAccountClassList.add(accountClass);
    }

    public void addAccountClass(List<String> accountClass) {

	for (String x : accountClass) {
	    addAccountClass(x);
	}
    }
}
