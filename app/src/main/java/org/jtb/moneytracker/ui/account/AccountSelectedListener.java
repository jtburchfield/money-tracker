package org.jtb.moneytracker.ui.account;

import org.jtb.moneytracker.data.account.Account;

public interface AccountSelectedListener {

    public void accountSelected(Account account);
}
