package org.jtb.moneytracker.ui.news;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.newsgofer.Article;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class ArticleAdapter extends ArrayAdapter<Article> {

    private int mLayoutResourceId;
    private Article[] mArticles;

    public ArticleAdapter(Context context, int layoutResourceId, Article[] articles) {
	super(context, layoutResourceId, articles);

	mLayoutResourceId = layoutResourceId;
	mArticles = articles;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
	View row = convertView;
	ArticleHolder holder = null;

	if (row == null) {
	    LayoutInflater inflater = ((Activity) getContext()).getLayoutInflater();
	    row = inflater.inflate(mLayoutResourceId, parent, false);

	    holder = new ArticleHolder();
	    holder.image = (ImageView) row.findViewById(R.id.image);
	    holder.title = (TextView) row.findViewById(R.id.title);
	    //holder.source = (TextView) row.findViewById(R.id.source);

	    row.setTag(holder);
	} else {
	    holder = (ArticleHolder) row.getTag();
	}

	Article article = mArticles[position];
	holder.title.setText(article.getTitle());
	holder.image.setImageBitmap(article.getImage());
	//holder.source.setText(article.getSource());
	holder.url = article.getUrl();

	return row;
    }

    static class ArticleHolder {
	ImageView image;
	TextView title;
	TextView updated;
	TextView source;
	String url;
    }
}
