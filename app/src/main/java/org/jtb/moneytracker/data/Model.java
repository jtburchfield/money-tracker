package org.jtb.moneytracker.data;

import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jtb.moneytracker.controller.DatabaseManager;
import org.jtb.moneytracker.data.budget.Budget;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public abstract class Model extends NotifyService {

    public static final String KEY_ID = "id";
    public static final int ID_COLUMN = 0;

    protected Long mId = -1L;
    protected static DatabaseManager cDbManager = null;

    private static int cOpenCount = 0;
    private static SQLiteOpenHelper cDbHelper = null;
    private static SQLiteDatabase cDb = null;

    public abstract ContentValues getContentValues();

    public abstract String getTableName();

    protected abstract Object create(Cursor cursor);

    protected void onSave() {

    }

    protected void onUpdate() {

    }

    public static void onCreate(SQLiteDatabase db) {
        Log.e("Model.onCreate", "Static method should be implemented in subclass.");
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.e("Model.onUpgrade", "Static method should be implemented in subclass.");
    }

    public static void initialize(DatabaseManager databaseManager, SQLiteOpenHelper databaseHelper) {
        cDbManager = databaseManager;
        cDbHelper = databaseHelper;
    }

    public Long getId() {
        return mId;
    }

    public boolean save() {

        boolean success = false;

        boolean isNew = false;
        if (mId == -1) {
            isNew = true;
        }

        ContentValues values = getContentValues();

        open();

        try {
            cDb.beginTransaction();

            if (mId == -1) { // new
                mId = cDb.insert(getTableName(), null, values);
                if (mId != -1) {
                    success = true;
                }
            }
            else { // update
                if (cDb.update(getTableName(), values, KEY_ID + "=" + mId, null) > 0) {
                    success = true;
                }
            }

            cDb.setTransactionSuccessful();
        }
        catch (Exception e) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, e);
        }
        finally {
            cDb.endTransaction();
        }

        close();

        if (success && isNew) {
            onSave();
        }
        else if (success) {
            onUpdate();
        }

        return success;
    }

    public void delete() {

        open();

        try {
            cDb.beginTransaction();
            cDb.delete(getTableName(), KEY_ID + "=" + mId, null);
            cDb.setTransactionSuccessful();
            mId = -1L;
        }
        catch (Exception e) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, e);
        }
        finally {
            cDb.endTransaction();
        }

        close();
    }

    /**
     * This method opens the database manager.
     * 
     * @return A new DatabaseManager object.
     * @throws SQLException
     */
    private static void open() throws SQLException {

        try {
            if (cOpenCount == 0) {
                cDb = cDbHelper.getWritableDatabase();
            }
        }
        catch (Exception e) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, e);
        }

        cOpenCount++;
    }

    /**
     * Closes the database.
     */
    private static void close() {

        try {
            if (cOpenCount == 1) {
                cDb.close();
            }
        }
        catch (Exception e) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, e);
        }

        cOpenCount--;
    }

    protected static Object get(Model model, Long id) {

        open();

        Object object = null;

        try {
            Cursor cursor = cDb.query(model.getTableName(), null, (KEY_ID + "=" + id), null, null, null, null);

            if (cursor.moveToFirst()) {
                object = model.create(cursor);
            }

            cursor.close();
        }
        catch (Exception e) {
            Logger.getLogger(Model.class.getName()).log(Level.SEVERE, null, e);
        }

        close();

        return object;
    }

    /**
     * Get all Budgets in database.
     * 
     * @return ArrayList of Budgets
     */
    protected static ArrayList<Object> getAll(Model model) {

        return get(model, "");
    }

    protected static ArrayList<Object> get(Model model, String where) {

        open();

        ArrayList<Object> objectList = null;

        try {
            Cursor cursor = cDb.query(model.getTableName(), null, where, null, null, null, null);

            if (cursor.moveToFirst()) {
                objectList = new ArrayList<Object>();

                do {
                    objectList.add(model.create(cursor));
                }
                while (cursor.moveToNext());
            }

            cursor.close();
        }
        catch (Exception e) {
            Logger.getLogger(Budget.class.getName()).log(Level.SEVERE, null, e);
        }

        close();

        return objectList;
    }
}
