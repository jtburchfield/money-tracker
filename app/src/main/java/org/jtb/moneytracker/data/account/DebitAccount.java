/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.account;

/**
 * 
 * @author jason
 */
public abstract class DebitAccount extends Account {

    DebitAccount(String name, double amount) {
        super(name, amount);
    }

    @Override
    public void debit(double amount) {

        this.mBalance += amount;
    }

    @Override
    public void credit(double amount) {

        this.mBalance -= amount;
    }
}
