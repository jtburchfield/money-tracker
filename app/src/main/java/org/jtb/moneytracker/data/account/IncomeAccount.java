/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.data.account;

/**
 * 
 * @author jason
 */
public class IncomeAccount extends CreditAccount {

    public IncomeAccount(String name, Double amount) {
        super(name, amount);
    }

    @Override
    public String getTypeName() {

        return "Income";
    }
}
