package org.jtb.moneytracker.data.account;

public class TransactionSort implements java.util.Comparator<Transaction> {

    public int compare(Transaction one, Transaction two) {

	return one.getDate().compareTo(two.getDate());
    }
}
