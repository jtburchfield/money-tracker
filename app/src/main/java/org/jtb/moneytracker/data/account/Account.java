/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.account;

import java.util.ArrayList;

/**
 * 
 * @author jason
 */
public abstract class Account {

    protected String mName;
    protected double mBalance;
    protected int mId;
    protected int mParentId;
    protected ArrayList<Account> mSubAccountList;

    public Account(String name, double balance) {

        mName = name;
        mBalance = balance;
        mSubAccountList = new ArrayList<Account>();
    }

    /**
     * This method gets the balance of this account.
     * 
     * @return The balance.
     */
    public double getBalance() {

        double total = mBalance;
        for (Account a : mSubAccountList) {
            total += a.getBalance();
        }

        return total;
    }

    /**
     * This method sets the balance of this account.
     * 
     * @param balance
     *            - The balance to set.
     */
    public void setBalance(double balance) {
        mBalance = balance;
    }

    /**
     * This method gets the name of this account.
     * 
     * @return The name of the account.
     */
    public String getName() {
        return mName;
    }

    /**
     * This method sets the name of this account.
     * 
     * @param name
     *            - The name of the account.
     */
    public void setName(String name) {
        mName = name;
    }

    /**
     * This method gets the unique ID of this account. p
     * 
     * @return The account ID.
     */
    public int getId() {
        return mId;
    }

    /**
     * This method sets the unique ID of this account.
     * 
     * @param id
     *            - The account ID.
     */
    public void setId(int id) {
        mId = id;
    }

    /**
     * This method gets the unique ID of this account. p
     * 
     * @return The account ID.
     */
    public int getParentId() {
        return mParentId;
    }

    /**
     * This method sets the unique ID of this account.
     * 
     * @param id
     *            - The account ID.
     */
    public void setParentId(int id) {
        mParentId = id;
    }

    /**
     * This method gets whether the account is a category.
     * 
     * @return true if this account is a category
     */
    public boolean isCategory() {
        return (mSubAccountList.size() > 0);
    }

    public void addSubAccount(Account account) {

        account.setParentId(getId());
        mSubAccountList.add(account);
    }

    public void addSubAccounts(ArrayList<Account> accounts) {

        for (Account a : accounts) {
            a.setParentId(getId());
        }
        mSubAccountList.addAll(accounts);
    }

    public void clearSubAccounts() {
        mSubAccountList.clear();
    }

    public ArrayList<Account> getSubAccounts() {
        return new ArrayList<Account>(mSubAccountList);
    }

    public abstract String getTypeName();

    public abstract void debit(double amount);

    public abstract void credit(double amount);
}
