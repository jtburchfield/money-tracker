/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.schedule;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author jason
 */
public class SemiMonthlyScheduledTransaction extends ScheduledTransaction {

    public SemiMonthlyScheduledTransaction(int mainAccountId, int secondaryAccountId, int transactionType,
	    String frequency, Date date, String description, double amount, int notifyCreate, int autoCreate,
	    int createDays, int notifyDays) {

	super(mainAccountId, secondaryAccountId, transactionType, frequency, date, description, amount, notifyCreate,
		autoCreate, createDays, notifyDays);
    }

    @Override
    public String getFrequencyDays() {

	StringBuilder days = new StringBuilder();

	try {
	    String[] data = this.mFrequency.split(ScheduledTransaction.SEPARATOR);

	    Integer day1 = Integer.parseInt(data[1]);
	    Integer day2 = Integer.parseInt(data[2]);

	    days.append(day1.toString()).append(" ");
	    days.append(day2.toString());
	} catch (Exception ex) {
	}

	return days.toString();
    }

    @Override
    public void updateDate() {

	Calendar calendar = Calendar.getInstance();

	try {
	    String[] data = this.mFrequency.split(ScheduledTransaction.SEPARATOR);

	    Integer months = Integer.parseInt(data[0]);
	    Integer day1 = Integer.parseInt(data[1]);
	    Integer day2 = Integer.parseInt(data[2]);

	    if (calendar.get(Calendar.DAY_OF_MONTH) >= day2) {
		calendar.add(Calendar.MONTH, months);
		calendar.set(Calendar.DAY_OF_MONTH, day1);
	    } else {
		calendar.set(Calendar.DAY_OF_MONTH, day2);
	    }
	} catch (Exception ex) {
	}

	mDate = calendar.getTime();
    }

    @Override
    public String getFrequencyName() {

	return "Semi-Monthly";
    }
}
