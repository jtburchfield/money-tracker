/**
 * 
 */
package org.jtb.moneytracker.data;

import java.util.ArrayList;

import android.os.Handler;
import android.os.Message;

/**
 * @author jason
 * 
 */
public class NotifyService {

    private static final ArrayList<Handler> listeners = new ArrayList<Handler>();

    public NotifyService() {
    }

    public static void register(Handler h) {

        listeners.add(h);
    }

    public static void unregister(Handler h) {

        listeners.remove(h);
    }

    protected void notify(int property, Object value) {

        for (Handler l : listeners) {

            Message msg = new Message();
            msg.what = property;
            msg.obj = value;
            l.sendMessage(msg);
        }
    }
}
