package org.jtb.moneytracker.data.budget;

import java.util.ArrayList;

import org.jtb.moneytracker.controller.DatabaseManager;
import org.jtb.moneytracker.data.Model;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class Budget extends Model {

    private final static String BUDGET_TABLE = "Budget";
    public final static String KEY_NAME = "name";
    private final static int NAME_COLUMN = 1;

    private String mName;
    private final ArrayList<BudgetAccount> mBudgetAccountList = new ArrayList<BudgetAccount>();
    private final ArrayList<BudgetAccount> mRemoveList = new ArrayList<BudgetAccount>();

    public Budget(String name) {

        mId = -1L;
        mName = name;
    }

    private Budget(Long id, String name) {

        mId = id;
        mName = name;
    }

    private Budget() {

        mId = -1L;
        mName = "";
    }

    public String getName() {
        return mName;
    }
    
    public void setName(String name) {
    	mName = name;
    }

    public void add(BudgetAccount account) {
    	
    	for(BudgetAccount b : mBudgetAccountList) {
    		if(b.getAccount().getId() == account.getAccount().getId()) {
    			return;
    		}
    	}
    	
    	mBudgetAccountList.add(account);
    }

    public void addAll(ArrayList<BudgetAccount> accounts) {
        mBudgetAccountList.addAll(accounts);
    }
    
    public void remove(BudgetAccount account) {

		mRemoveList.add(account);
		mBudgetAccountList.remove(account);
    }

    public final ArrayList<BudgetAccount> getAccounts() {
        return mBudgetAccountList;
    }

    public Double getRemaining() {
        Double total = 0.0;
        for (BudgetAccount b : mBudgetAccountList) {
            total += b.getRemaining();
        }

        return total;
    }

    public Double getBudgeted() {
        Double total = 0.0;
        for (BudgetAccount b : mBudgetAccountList) {
            total += b.getBudgeted();
        }

        return total;
    }

    public int getProgress() {
        return (int) ((getBudgeted() - getRemaining()) / getBudgeted() * 100);
    }

    public static Budget get(Long id) {

        Budget b = (Budget) Model.get(new Budget(), id);
        if (b != null) {
            b.addAll(BudgetAccount.getByBudgetId(b.getId()));
        }

        return b;
    }

    public static ArrayList<Budget> getAll() {

        ArrayList<Budget> list = new ArrayList<Budget>();
        ArrayList<Object> results = Model.getAll(new Budget());
        for (Object o : results) {
            Budget b = (Budget) o;
            b.addAll(BudgetAccount.getByBudgetId(b.getId()));
            list.add(b);
        }

        return list;
    }

    public static ArrayList<String> getNames() {

        ArrayList<Object> list = Model.getAll(new Budget());
        ArrayList<String> names = null;

        if (list != null) {
            names = new ArrayList<String>();

            for (Object b : list) {
                names.add(((Budget) b).getName());
            }
        }

        return names;
    }

    @Override
    public ContentValues getContentValues() {

        ContentValues values = new ContentValues();
        values.put(KEY_NAME, mName);

        return values;
    }

    @Override
    public String getTableName() {
        return BUDGET_TABLE;
    }

    @Override
    protected Object create(Cursor cursor) {
        return new Budget(cursor.getLong(ID_COLUMN), cursor.getString(NAME_COLUMN));
    }

    @Override
    public boolean save() {

        boolean success = super.save();
        
        for(BudgetAccount a : mRemoveList) {
        	a.delete();
        }
        
        if (success) {
            for (BudgetAccount a : mBudgetAccountList) {
                a.setBudgetId(mId);
                success = a.save() && success;
            }
        }

        return success;
    }

    @Override
    protected void onSave() {
        notify(BudgetListener.BUDGET_SAVED, mId);
    }

    @Override
    protected void onUpdate() {
        notify(BudgetListener.BUDGET_UPDATED, mId);
    }

    public static void onCreate(SQLiteDatabase db, DatabaseManager dbManager) {
        Log.d("DatabaseHelper", "Creating Budget");
        String sql = "create table " + BUDGET_TABLE + " (" + KEY_ID + " integer primary key autoincrement, " + KEY_NAME
                        + " text not null" + ");";

        db.beginTransaction();
        db.execSQL(sql);
        db.setTransactionSuccessful();
        db.endTransaction();

        BudgetAccount.onCreate(db);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Log the version upgrade.
        Log.d("DatabaseHelper", "Upgrading budget from version " + oldVersion + " to " + newVersion);

        BudgetAccount.onUpgrade(db, oldVersion, newVersion);
        // Upgrade the existing database to conform to the new version.
        // Multiple previous versions can be handled by comparing
        // _oldVersion and _newVersion values.

        // The simplest case is to drop the old table and create a
        // new one.
        // db.execSQL("DROP TABLE IF EXISTS " + databaseTable);
        // Create a new one.

        while (oldVersion < newVersion) {

            oldVersion++;
        }
    }
}
