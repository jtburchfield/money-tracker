/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.schedule;

import java.util.Date;

/**
 * 
 * @author jason
 */
public abstract class ScheduledTransaction implements Comparable<ScheduledTransaction> {

    protected static final String SEPARATOR = ":";

    /** See Transaction.TRANSACTION_TYPE_* */
    protected int mTransactionType;
    protected String mFrequency;
    protected int mNotifyCreate;
    protected int mAutoCreate;
    protected int mCreateDays;
    protected int mNotifyDays;
    protected long mId;
    protected int mMainAccountId;
    protected int mSecondaryAccountId;
    protected double mAmount;
    protected Date mDate;
    protected String mDescription;

    protected ScheduledTransaction() {

    }

    protected ScheduledTransaction(int mainAccountId, int secondaryAccountId, int transactionType, String frequency,
	    Date date, String description, double amount, int notifyCreate, int autoCreate, int createDays,
	    int notifyDays) {

	mTransactionType = transactionType;
	mFrequency = frequency;
	mNotifyCreate = notifyCreate;
	mAutoCreate = autoCreate;
	mCreateDays = createDays;
	mNotifyDays = notifyDays;
	mMainAccountId = mainAccountId;
	mSecondaryAccountId = secondaryAccountId;
	mDate = date;
	mDescription = description;
	mAmount = amount;
    }

    @Override
    public int compareTo(ScheduledTransaction transaction) {
	return this.getDate().compareTo(transaction.getDate());
    }

    /**
     * @return The number of weeks or months in the frequency.
     */
    public String getFrequencyNum() {

	String[] data = mFrequency.split(ScheduledTransaction.SEPARATOR);

	return data[0];
    }

    public abstract String getFrequencyDays();

    /**
     * Updates the date to the next date corresponding to the frequency type
     * selected for this transaction.
     */
    public abstract void updateDate();

    public abstract String getFrequencyName();

    public int getTransactionType() {
	return mTransactionType;
    }

    public String getFrequency() {
	return mFrequency;
    }

    public boolean getNotifyCreate() {
	return (mNotifyCreate == 1) ? true : false;
    }

    public boolean getAutoCreate() {
	return (mAutoCreate == 1) ? true : false;
    }

    public int getCreateDays() {
	return mCreateDays;
    }

    public int getNotifyDays() {
	return mNotifyDays;
    }

    public void setId(long id) {
	mId = id;
    }

    public long getId() {
	return mId;
    }

    public double getAmount() {
	return mAmount;
    }

    public Date getDate() {
	return mDate;
    }

    public String getDescription() {
	return mDescription;
    }

    public int getMainAccountId() {
	return mMainAccountId;
    }

    public int getSecondaryAccountId() {
	return mSecondaryAccountId;
    }
}
