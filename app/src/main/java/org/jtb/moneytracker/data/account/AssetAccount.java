/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.data.account;

/**
 * 
 * @author jason
 */
public class AssetAccount extends DebitAccount {

    public AssetAccount(String name, Double amount) {
        super(name, amount);
    }

    @Override
    public String getTypeName() {

        return "Asset";
    }
}
