package org.jtb.moneytracker.data.budget;

import android.os.Handler;

public class BudgetListener extends Handler {
    public final static int BUDGET_SAVED = 0;
    public final static int BUDGET_UPDATED = 1;
    public final static int ACCOUNT_ADDED = 2;
}
