package org.jtb.moneytracker.data.account;



public class NewestFirstTransactionSort extends TransactionSort {

    @Override
    public int compare(Transaction one, Transaction two) {

        return two.getDate().compareTo(one.getDate())
                        + Integer.valueOf((int) two.getId()).compareTo(Integer.valueOf((int) one.getId()));
    }
}
