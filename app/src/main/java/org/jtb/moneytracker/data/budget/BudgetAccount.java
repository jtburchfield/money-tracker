package org.jtb.moneytracker.data.budget;

import java.util.ArrayList;
import java.util.Calendar;

import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.Model;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.CreditAccount;
import org.jtb.moneytracker.data.account.DebitAccount;
import org.jtb.moneytracker.data.account.Transaction;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class BudgetAccount extends Model {

    public static final String KEY_ACCOUNT_ID = "accountId";
    public static final int ACCOUNT_ID_COLUMN = 1;
    public static final String KEY_BUDGETED = "budgeted";
    public static final int BUDGETED_COLUMN = 2;
    public static final String KEY_BUDGET_ID = "budgetId";
    public static final int BUDGET_ID_COLUMN = 3;

    private final Account mAccount;
    private Double mBudgeted;
    private Long mBudgetId;

    private static String BUDGET_ACCOUNT_TABLE = "BudgetAccount";

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    public BudgetAccount(Account account, Double budgeted) {
	mAccount = account;
	mBudgeted = budgeted;
	mBudgetId = -1L;
    }

    private BudgetAccount(Long id, Account account, Double budgeted, Long budgetId) {
	mId = id;
	mAccount = account;
	mBudgeted = budgeted;
	mBudgetId = budgetId;
    }

    private BudgetAccount() {
	mAccount = null;
	mBudgeted = 0d;
	mBudgetId = -1L;
    }

    public String getName() {
	return mAccount.getName();
    }

    public Double getBudgeted() {
	return mBudgeted;
    }

    public void setBudgeted(Double budgeted) {
	mBudgeted = budgeted;
    }

    public void setBudgetId(Long id) {
	mBudgetId = id;
    }

    public Account getAccount() {
	return mAccount;
    }

    /**
     * Get the remaining budgeted amount.
     * 
     * @return the remaining amount
     */
    public Double getRemaining() {
	ArrayList<Transaction> transactions = mController.getTransactions(mAccount.getId());
	Calendar currentCal = Calendar.getInstance();
	Calendar transactionCal = Calendar.getInstance();
	Double total = 0.0;

	for (Transaction t : transactions) {
	    transactionCal.setTime(t.getDate());
	    if (transactionCal.get(Calendar.MONTH) == currentCal.get(Calendar.MONTH)
		    && transactionCal.get(Calendar.YEAR) == currentCal.get(Calendar.YEAR)) {
		if ((mAccount instanceof DebitAccount && t.getDebitAccountId() == mAccount.getId())
			|| (mAccount instanceof CreditAccount && t.getCreditAccountId() == mAccount.getId())) {
		    total += t.getAmount();
		}
	    }
	}

	return mBudgeted - total;
    }

    public int getProgress() {
	return (int) ((mBudgeted - getRemaining()) / mBudgeted * 100);
    }

    public static BudgetAccount get(Long id) {
	BudgetAccount account = (BudgetAccount) Model.get(new BudgetAccount(), id);
	return account;
    }

    public static ArrayList<BudgetAccount> get(String where) {
	ArrayList<BudgetAccount> budgetAccountList = new ArrayList<BudgetAccount>();
	ArrayList<Object> data = Model.get(new BudgetAccount(), where);

	if (data != null) {
	    for (Object o : data) {
		budgetAccountList.add((BudgetAccount) o);
	    }
	}

	return budgetAccountList;
    }

    public static ArrayList<BudgetAccount> getByBudgetId(Long id) {
	return BudgetAccount.get(KEY_BUDGET_ID + "=" + id);
    }

    @Override
    public ContentValues getContentValues() {
	ContentValues values = new ContentValues();
	values.put(KEY_ACCOUNT_ID, mAccount.getId());
	values.put(KEY_BUDGETED, mBudgeted);
	values.put(KEY_BUDGET_ID, mBudgetId);

	return values;
    }

    @Override
    public String getTableName() {
	return BUDGET_ACCOUNT_TABLE;
    }

    @Override
    protected Object create(Cursor cursor) {
	Account a = cDbManager.getAccount(cursor.getLong(ACCOUNT_ID_COLUMN));
	return new BudgetAccount(cursor.getLong(ID_COLUMN), a, cursor.getDouble(BUDGETED_COLUMN),
		cursor.getLong(BUDGET_ID_COLUMN));
    }

    public static void onCreate(SQLiteDatabase db) {

	Log.d("BudgetAccount", "Creating Database");
	String sql = "create table " + BUDGET_ACCOUNT_TABLE + " (" + KEY_ID + " integer primary key autoincrement, "
		+ KEY_ACCOUNT_ID + " integer, " + KEY_BUDGETED + " double, " + KEY_BUDGET_ID + " integer" + ");";

	db.beginTransaction();
	db.execSQL(sql);
	db.setTransactionSuccessful();
	db.endTransaction();
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
