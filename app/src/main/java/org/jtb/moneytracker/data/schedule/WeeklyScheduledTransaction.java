/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.schedule;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author jason
 */
public class WeeklyScheduledTransaction extends ScheduledTransaction {

    public WeeklyScheduledTransaction(int mainAccountId, int secondaryAccountId, int transactionType,
	    String frequency, Date date, String description, double amount, int notifyCreate, int autoCreate,
	    int createDays, int notifyDays) {

	super(mainAccountId, secondaryAccountId, transactionType, frequency, date, description, amount, notifyCreate,
		autoCreate, createDays, notifyDays);
    }

    @Override
    public String getFrequencyDays() {

	StringBuilder days = new StringBuilder();

	try {
	    String[] data = this.mFrequency.split(ScheduledTransaction.SEPARATOR);

	    Integer sunday = Integer.parseInt(data[1]);
	    Integer monday = Integer.parseInt(data[2]);
	    Integer tuesday = Integer.parseInt(data[3]);
	    Integer wednesday = Integer.parseInt(data[4]);
	    Integer thursday = Integer.parseInt(data[5]);
	    Integer friday = Integer.parseInt(data[6]);
	    Integer saturday = Integer.parseInt(data[7]);

	    days.append((sunday == 1) ? "Su " : "");
	    days.append((monday == 1) ? "M " : "");
	    days.append((tuesday == 1) ? "T " : "");
	    days.append((wednesday == 1) ? "W " : "");
	    days.append((thursday == 1) ? "H " : "");
	    days.append((friday == 1) ? "F " : "");
	    days.append((saturday == 1) ? "S" : "");
	} catch (Exception e) {
	}
	return days.toString();
    }

    @Override
    public void updateDate() {

	Calendar calendar = Calendar.getInstance();

	try {
	    String[] data = this.mFrequency.split(ScheduledTransaction.SEPARATOR);

	    Integer weeks = Integer.parseInt(data[0]);
	    Integer sunday = Integer.parseInt(data[1]);
	    Integer monday = Integer.parseInt(data[2]);
	    Integer tuesday = Integer.parseInt(data[3]);
	    Integer wednesday = Integer.parseInt(data[4]);
	    Integer thursday = Integer.parseInt(data[5]);
	    Integer friday = Integer.parseInt(data[6]);
	    Integer saturday = Integer.parseInt(data[7]);

	    Integer numDays = (weeks - 1) * 7;

	    calendar.add(Calendar.DATE, 1);

	    boolean addDays = false;

	    Integer dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);

	    Integer day = dayOfWeek;
	    boolean found = false;
	    Integer count = 0;
	    while (!found && count < 8) {

		switch (day) {
		case Calendar.SUNDAY:
		    found = (sunday == 1) ? true : false;
		    break;

		case Calendar.MONDAY:
		    found = (monday == 1) ? true : false;
		    break;

		case Calendar.TUESDAY:
		    found = (tuesday == 1) ? true : false;
		    break;

		case Calendar.WEDNESDAY:
		    found = (wednesday == 1) ? true : false;
		    break;

		case Calendar.THURSDAY:
		    found = (thursday == 1) ? true : false;
		    break;

		case Calendar.FRIDAY:
		    found = (friday == 1) ? true : false;
		    break;

		case Calendar.SATURDAY:
		    found = (saturday == 1) ? true : false;
		    break;
		}

		if (!found) {
		    count++;

		    if (day >= Calendar.SATURDAY) {
			addDays = true;
			day = Calendar.SUNDAY;
		    } else {
			day++;
		    }
		}
	    }

	    if (addDays) {
		calendar.add(Calendar.DATE, numDays);
	    }

	    calendar.add(Calendar.DATE, count);
	} catch (Exception ex) {
	}

	mDate = calendar.getTime();
    }

    @Override
    public String getFrequencyName() {

	return "Weekly";
    }
}
