/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.account;

import java.util.Date;

/**
 * 
 * @author jason
 */
public class Transaction {

    public static int TRANSACTION_TYPE_DEBIT = 0;
    public static int TRANSACTION_TYPE_CREDIT = 1;

    protected long mId = -1;
    protected Date mDate;
    protected double mAmount;
    protected String mDescription;
    protected int mDebitAccountId;
    protected int mCreditAccountId;

    public Transaction(int debitAccountId, int creditAccountId, double amount, String description, Date date) {

	mDebitAccountId = debitAccountId;
	mCreditAccountId = creditAccountId;
	mAmount = amount;
	mDescription = description;
	mDate = date;
    }

    public Transaction(int debitAccountId, int creditAccountId, double amount, String description, Date date,
	    long transactionId) {

	mDebitAccountId = debitAccountId;
	mCreditAccountId = creditAccountId;
	mAmount = amount;
	mDescription = description;
	mDate = date;
	mId = transactionId;
    }

    public Transaction(final Transaction transaction, long id) {

	mDebitAccountId = transaction.getDebitAccountId();
	mCreditAccountId = transaction.getCreditAccountId();
	mAmount = transaction.getAmount();
	mDescription = transaction.getDescription();
	mDate = transaction.getDate();
	mId = id;
    }

    protected Transaction() {

    }

    public int getCreditAccountId() {
	return mCreditAccountId;
    }

    public int getDebitAccountId() {
	return mDebitAccountId;
    }

    public Date getDate() {
	return mDate;
    }

    public long getId() {
	return mId;
    }

    public Double getAmount() {
	return mAmount;
    }

    public String getDescription() {
	return mDescription;
    }
}
