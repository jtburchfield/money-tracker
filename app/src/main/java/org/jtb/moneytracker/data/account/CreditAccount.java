/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.account;

/**
 * 
 * @author jason
 */
public abstract class CreditAccount extends Account {

    CreditAccount(String name, double balance) {
        super(name, balance);
    }

    @Override
    public void debit(double amount) {

        this.mBalance -= amount;
    }

    @Override
    public void credit(double amount) {

        this.mBalance += amount;
    }
}
