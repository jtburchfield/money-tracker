/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.account;


/**
 * 
 * @author jason
 */
public class AccountFactory {

    private static AccountFactory factory = new AccountFactory();

    AccountFactory() {

    }

    public static AccountFactory getInstance() {

	return factory;
    }

    public Account createAccount(String name, double balance, String className) throws ClassNotFoundException {

	if (className.contains("BankAccount")) {
	    return new BankAccount(name, balance);
	} else if (className.contains("ExpenseAccount")) {
	    return new ExpenseAccount(name, balance);
	} else if (className.contains("IncomeAccount")) {
	    return new IncomeAccount(name, balance);
	} else if (className.contains("LiabilityAccount")) {
	    return new LiabilityAccount(name, balance);
	} else if (className.contains("AssetAccount")) {
	    return new AssetAccount(name, balance);
	}

	throw new ClassNotFoundException();
    }

    // public static ArrayList<String> getAccountTypeNames() {
    //
    // ArrayList<String> names = new ArrayList<String>();
    //
    // for (int i = 0; i < accountTypes.size(); ++i) {
    //
    // names.add(accountTypes.get(i).getSimpleName());
    // }
    //
    // return names;
    // }
    //
    // public static ArrayList<String> getAccountTypeNames(Class className) {
    //
    // ArrayList<String> names = new ArrayList<String>();
    //
    // if()
    // for (int i = 0; i < accountTypes.size(); ++i) {
    //
    // names.add(accountTypes.get(i).getSimpleName());
    // }
    //
    // return names;
    // }
}
