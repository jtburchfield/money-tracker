/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.schedule;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author jason
 */
public class DailyScheduledTransaction extends ScheduledTransaction {

    @SuppressWarnings("unused")
    private DailyScheduledTransaction() {

    }

    public DailyScheduledTransaction(int mainAccount, int secondaryAccount, int transactionType,
	    String frequency, Date date, String description, double amount, int notifyCreate, int autoCreate,
	    int createDays, int notifyDays) {

	super(mainAccount, secondaryAccount, transactionType, frequency, date, description, amount, notifyCreate,
		autoCreate, createDays, notifyDays);
    }

    @Override
    public String getFrequencyDays() {
	return "";
    }

    @Override
    public void updateDate() {

	Calendar calendar = Calendar.getInstance();

	Integer numDays = 0;
	try {
	    numDays = Integer.parseInt(this.mFrequency);
	} catch (Exception ex) {
	}

	calendar.add(Calendar.DATE, numDays);
	mDate = calendar.getTime();
    }

    @Override
    public String getFrequencyName() {

	return "Daily";
    }
}
