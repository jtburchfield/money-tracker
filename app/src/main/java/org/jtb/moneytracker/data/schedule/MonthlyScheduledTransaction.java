/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.data.schedule;

import java.util.Calendar;
import java.util.Date;

/**
 * 
 * @author jason
 */
public class MonthlyScheduledTransaction extends ScheduledTransaction {

    public MonthlyScheduledTransaction(int mainAccountId, int secondaryAccountId, int transactionType,
	    String frequency, Date date, String description, double amount, int notifyCreate, int autoCreate,
	    int createDays, int notifyDays) {

	super(mainAccountId, secondaryAccountId, transactionType, frequency, date, description, amount, notifyCreate,
		autoCreate, createDays, notifyDays);
    }

    @Override
    public String getFrequencyDays() {

	StringBuilder days = new StringBuilder();

	try {
	    String[] data = this.mFrequency.split(ScheduledTransaction.SEPARATOR);

	    Integer day = Integer.parseInt(data[1]);

	    days.append(day.toString());
	} catch (Exception ex) {
	}

	return days.toString();
    }

    @Override
    public void updateDate() {

	Calendar calendar = Calendar.getInstance();

	try {
	    String[] data = this.mFrequency.split(ScheduledTransaction.SEPARATOR);

	    Integer months = Integer.parseInt(data[0]);
	    Integer day = Integer.parseInt(data[1]);

	    calendar.add(Calendar.MONTH, months);
	    calendar.set(Calendar.DAY_OF_MONTH, day);
	} catch (Exception ex) {
	}

	mDate = calendar.getTime();
    }

    @Override
    public String getFrequencyName() {

	return "Monthly";
    }
}
