/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.data.account;

/**
 * 
 * @author jason
 */
public class LiabilityAccount extends CreditAccount {

    public LiabilityAccount(String name, Double amount) {
        super(name, amount);
    }

    @Override
    public String getTypeName() {

        return "Liability";
    }
}
