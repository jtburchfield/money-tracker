/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.jtb.moneytracker.controller;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;

import com.mobclix.android.sdk.MobclixMMABannerXLAdView;

import org.jtb.moneytracker.R;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.data.schedule.ScheduledTransaction;
import org.jtb.moneytracker.ui.HelperActivity;
import org.jtb.moneytracker.ui.account.AccountListFragment;
import org.jtb.moneytracker.ui.account.AccountTypesFragment;
import org.jtb.moneytracker.ui.account.CreateAccountFragment;
import org.jtb.moneytracker.ui.account.EditAccountFragment;
import org.jtb.moneytracker.ui.account.TransactionFragment;
import org.jtb.moneytracker.ui.account.TransactionListFragment;
import org.jtb.moneytracker.ui.budget.BudgetFragment;
import org.jtb.moneytracker.ui.budget.EditBudgetFragment;
import org.jtb.moneytracker.ui.schedule.ScheduleFragment;
import org.jtb.moneytracker.ui.schedule.ScheduleListFragment;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

//import org.apache.http.Header;
//import org.apache.http.HttpResponse;
//import org.apache.http.client.methods.HttpGet;
//import android.net.http.AndroidHttpClient;

/**
 * @author jason
 */
public class MoneyTrackerController extends Controller {

    // The database name.
    private static final String DATABASE_NAME = "MoneyTracker.db";

    // The database version.
    public static final int DATABASE_VERSION = 5;

    private final static String PREFS_FILE = "PrefsFile";
    private final static String PREF_SELECTED_ACCOUNT_ID = "selectedAccountId";
    private final static String PREF_TRANSACTION_SORT = "transactionSort";
    private final static String PREF_SELECTED_BUDGET_ID = "selectedBudgetId";
    private final static String DATE_FORMAT = "MM-dd-yyyy";

    private static MoneyTrackerController mMoneyController = new MoneyTrackerController();
    private DatabaseManager mDatabaseManager;
    private Context mContext;
    private final AccountSort mAccountSort = new AccountSort();

    protected MoneyTrackerController() {

    }

    /**
     * Gets the instance of the controller.
     *
     * @return the controller.
     */
    public static MoneyTrackerController getInstance() {

        return mMoneyController;
    }

    public static Account getWidgetAccount(Context context) {

        long value = 0;

        // Restore preferences
        SharedPreferences settings = context.getSharedPreferences(PREFS_FILE, 0);
        value = settings.getLong(PREF_SELECTED_ACCOUNT_ID, 30);
        DatabaseManager db = new DatabaseManager(context, MoneyTrackerController.DATABASE_NAME,
                MoneyTrackerController.DATABASE_VERSION, DATE_FORMAT);

        Account acct = db.getAccount(value);

        return acct;
    }

    public static int getDatabaseVersion() {
        return DATABASE_VERSION;
    }

    public static String getDateFormat() {
        return DATE_FORMAT;
    }

    /**
     * Sets the main context for the application.
     *
     * @param context the context
     */
    public void initialize(Context context) {

        mContext = context;
        initialize(mContext, DATABASE_NAME, DATABASE_VERSION);
    }

    public void initialize(Context context, String databaseName, int databaseVersion) {

        mContext = context;
        mDatabaseManager = new DatabaseManager(context, databaseName, databaseVersion, DATE_FORMAT);
    }

    public Comparator<Account> getAccountComparator() {

        return mAccountSort;
    }

    public ArrayList<String> getAccountTypeNames() {

        ArrayList<String> names = new ArrayList<String>();
        names.add(AssetAccount.class.getName());
        names.add(BankAccount.class.getName());
        names.add(ExpenseAccount.class.getName());
        names.add(IncomeAccount.class.getName());
        names.add(LiabilityAccount.class.getName());

        return names;
    }

    public ArrayList<String> getAccountTypeBalances() {

        ArrayList<String> balances = new ArrayList<String>();
        balances.add(NumberFormat.getCurrencyInstance().format((getAccountTypeBalance(AssetAccount.class.toString()))));
        balances.add(NumberFormat.getCurrencyInstance().format(getAccountTypeBalance(BankAccount.class.toString())));
        balances.add(NumberFormat.getCurrencyInstance().format(getAccountTypeBalance(ExpenseAccount.class.toString())));
        balances.add(NumberFormat.getCurrencyInstance().format(getAccountTypeBalance(IncomeAccount.class.toString())));
        balances.add(NumberFormat.getCurrencyInstance()
                .format(getAccountTypeBalance(LiabilityAccount.class.toString())));

        return balances;
    }

    public List<String> getAccountTypes() {

        List<String> types = new ArrayList<String>();
        types.add(AssetAccount.class.toString());
        types.add(BankAccount.class.toString());
        types.add(ExpenseAccount.class.toString());
        types.add(IncomeAccount.class.toString());
        types.add(LiabilityAccount.class.toString());
        return types;
    }

    /**
     * Gets the full class type name of an account from the base type name.
     *
     * @param shortName The short account type name (BankAccount)
     * @return The long account type name (org.jtb.skeeba.account.BankAccount)
     */
    public String getAccountTypeName(final String shortName) {

        String longName = "";
        if (shortName != null && shortName.isEmpty() == false) {
            ArrayList<String> names = getAccountTypeNames();

            for (int i = 0; i < names.size(); ++i) {

                if (names.get(i).contains(shortName)) {
                    longName = names.get(i);
                }
            }
        }
        return longName;
    }

    public ArrayList<String> getShortAccountTypeNames() {

        ArrayList<String> names = getAccountTypeNames();

        for (int i = 0; i < names.size(); ++i) {

            String type = names.get(i);
            type = type.substring(type.lastIndexOf(".") + 1).replaceAll("Account", "");
            names.set(i, type);
        }

        return names;
    }

    public Double getAccountTypeBalance(String accountType) {

        Double balance = 0.0;
        List<Account> accounts = getRootAccounts(accountType);
        for (Account a : accounts) {
            balance += a.getBalance();
        }

        return balance;
    }

    /**
     * Removes all characters to the last "." in the string, then replaces
     * "Account" with the empty string for the rest of the string.
     * <p/>
     * <p>
     * If a "." and "Account" string are not found then the original string is
     * returned.
     * </p>
     *
     * @param className
     * @return The short account name
     */
    public String getShortAccountTypeName(final String className) {

        String type = className.substring(className.lastIndexOf(".") + 1).replaceAll("Account", "");
        return type;
    }

    public void setSelectedAccount(final Account acct) {

        setPreference(PREF_SELECTED_ACCOUNT_ID, acct.getId());
        updateWidget();
        notify(MoneyTrackerEvents.MAIN_ACCOUNT, acct);
    }

    public Account getAccount(long id) {

        Account account = mDatabaseManager.getAccount(id);
        return account;
    }

    public Account getAccountByName(final String name) {

        Account account = mDatabaseManager.getAccountByName(name);
        return account;
    }

    public Account getSelectedAccount() {

        long id = getPreference(PREF_SELECTED_ACCOUNT_ID, 30);
        Account acct = getAccount(id);

        return acct;
    }

    public List<Account> getRootAccounts(String aClassName) {

        ArrayList<Account> accounts = mDatabaseManager.getRootAccounts(aClassName);
        Collections.sort(accounts, mAccountSort);
        return accounts;
    }

    public void setTransactionSort(final String sortMethod) {

        setPreference(PREF_TRANSACTION_SORT, sortMethod);
    }

    public String getTransactionSort() {

        return getPreference(PREF_TRANSACTION_SORT, "");
    }

    public int insertAccount(final Account account) {

        int id = mDatabaseManager.insertAccount(account);
        account.setId(id);
        notify(MoneyTrackerEvents.ACCOUNT_INSERTED, account);

        return id;
    }

    public void updateAccount(final Account account) {

        // update the account
        mDatabaseManager.updateAccount(account);
        notify(MoneyTrackerEvents.ACCOUNT_UPDATED, account);
    }

    public void deleteAccount(final Account account) {

        mDatabaseManager.deleteAccount(account);
        updateWidget();
        notify(MoneyTrackerEvents.ACCOUNT_DELETED, account);
    }

    public Transaction insertTransaction(final Transaction transaction) {

        long id = mDatabaseManager.insertTransaction(transaction);

        Account debitAccount = mDatabaseManager.getAccount(transaction.getDebitAccountId());
        Account creditAccount = mDatabaseManager.getAccount(transaction.getCreditAccountId());

        debitAccount.debit(transaction.getAmount());
        creditAccount.credit(transaction.getAmount());

        mDatabaseManager.updateAccount(debitAccount);
        mDatabaseManager.updateAccount(creditAccount);

        updateWidget();
        notify(MoneyTrackerEvents.TRANSACTION_CREATED, transaction);

        return new Transaction(transaction, id);
    }

    public Transaction insertTransaction(ScheduledTransaction st) {

        Account mainAccount = mDatabaseManager.getAccount(st.getMainAccountId());
        Account secondaryAccount = mDatabaseManager.getAccount(st.getSecondaryAccountId());

        Account debitAccount = null;
        Account creditAccount = null;

        if (st.getTransactionType() == Transaction.TRANSACTION_TYPE_DEBIT) {
            if (mainAccount.getClass() == AssetAccount.class || mainAccount.getClass() == BankAccount.class) {
                debitAccount = mainAccount;
                creditAccount = secondaryAccount;
            } else {
                debitAccount = secondaryAccount;
                creditAccount = mainAccount;
            }
        } else {
            if (mainAccount.getClass() == AssetAccount.class || mainAccount.getClass() == BankAccount.class) {
                debitAccount = secondaryAccount;
                creditAccount = mainAccount;
            } else {
                debitAccount = mainAccount;
                creditAccount = secondaryAccount;
            }
        }

        Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), st.getAmount(),
                st.getDescription(), st.getDate());

        return insertTransaction(transaction);
    }

    public void deleteTransaction(Transaction transaction) {

        mDatabaseManager.deleteTransaction(transaction);
        updateWidget();
        notify(MoneyTrackerEvents.TRANSACTION_DELETED, transaction);
    }

    public Transaction updateTransaction(final Transaction oldTransaction, final Transaction newTransaction) {

        deleteTransaction(oldTransaction);
        Transaction transaction = insertTransaction(newTransaction);

        updateWidget();
        notify(MoneyTrackerEvents.TRANSACTION_UPDATED, newTransaction);
        return transaction;
    }

    public ArrayList<Transaction> getTransactions(int accountId) {

        ArrayList<Transaction> transactions = mDatabaseManager.getTransactions(accountId);
        return transactions;
    }

    public Transaction getTransaction(long transactionId) {

        Transaction transaction = mDatabaseManager.getTransaction(transactionId);
        return transaction;
    }

    public ArrayList<ScheduledTransaction> getScheduledTransactions() {

        ArrayList<ScheduledTransaction> transactions = mDatabaseManager.getScheduledTransactions();
        return transactions;
    }

    public ScheduledTransaction getScheduledTransaction(long transactionId) {

        ScheduledTransaction transaction = mDatabaseManager.getScheduledTransaction(transactionId);
        return transaction;
    }

    public void insertScheduledTransaction(ScheduledTransaction transaction) {

        mDatabaseManager.insertScheduledTransaction(transaction);
    }

    public ScheduledTransaction updateScheduledTransaction(final ScheduledTransaction transaction) {

        mDatabaseManager.deleteScheduledTransaction(transaction.getId());
        long id = mDatabaseManager.insertScheduledTransaction(transaction);

        transaction.setId(id);

        notify(MoneyTrackerEvents.SCHEDULED_TRANSACTION_UPDATED, transaction);
        return transaction;
    }

    public void deleteScheduledTransaction(long id) {

        mDatabaseManager.deleteScheduledTransaction(id);
    }

    public ArrayList<Budget> getBudgets() {

        return Budget.getAll();
    }

    public Budget getSelectedBudget() {

        long id = getPreference(PREF_SELECTED_BUDGET_ID, 1);
        Budget budget = Budget.get(id);
        return budget;
    }

    public Budget getDefaultBudget() {

        Budget budget = Budget.get(1L);
        return budget;
    }

    public void setSelectedBudget(Budget budget) {

        setPreference(PREF_SELECTED_BUDGET_ID, budget.getId());
    }

    public void showAccountTypesFragment(final FragmentActivity activity) {
        showFragment(AccountTypesFragment.class, null);
    }

    public void showCreateAccountFragment(final FragmentActivity activity) {
        showFragment(CreateAccountFragment.class, null);
    }

    public void showEditAccountFragment(final FragmentActivity activity, final Account account) {
        Bundle args = new Bundle();
        args.putInt("Account", account.getId());
        showFragment(EditAccountFragment.class, args);
    }

    public void showAccountListFragment(final FragmentActivity activity, final String accountType) {
        Bundle args = new Bundle();
        args.putString("AccountType", accountType);
        showFragment(AccountListFragment.class, args);
    }

    public void showAccountListFragment(final FragmentActivity activity, int categoryId) {
        Bundle args = new Bundle();
        args.putInt("ParentId", categoryId);
        showFragment(AccountListFragment.class, args);
    }

    public void showTransactionListFragment(final FragmentActivity activity, final Account account) {
        Bundle args = new Bundle();
        args.putInt("Account", account.getId());
        showFragment(TransactionListFragment.class, args);
    }

    public void showTransactionFragment(final Account account, int transactionType,
                                        final FragmentActivity activity) {
        Bundle args = new Bundle();
        args.putInt("Account", account.getId());
        args.putInt("TransactionType", transactionType);
        showFragment(TransactionFragment.class, args);
    }

    public void showTransactionFragment(final FragmentActivity activity, final Account account,
                                        final Transaction transaction, int transactionType) {
        Bundle args = new Bundle();
        args.putInt("Account", account.getId());
        args.putLong("Transaction", transaction.getId());
        args.putInt("TransactionType", transactionType);
        showFragment(TransactionFragment.class, args);
    }

    public void showScheduleListFragment(final FragmentActivity activity) {
        showFragment(ScheduleListFragment.class, null);
    }

    public void showScheduleFragment(final FragmentActivity activity) {
        showFragment(ScheduleFragment.class, null);
    }

    public void showEditScheduleFragment(final FragmentActivity activity, long id) {
        Bundle args = new Bundle();
        args.putLong("Transaction", id);
        showFragment(ScheduleFragment.class, args);
    }

    public void startRateActivity(final FragmentActivity activity) {
        String url = "market://details?id=org.jtb.moneytracker";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        activity.startActivity(i);
    }

    public void showBudgetFragment(final FragmentActivity activity) {
        showFragment(BudgetFragment.class, null);
    }

    public void showEditBudgetFragment(FragmentActivity activity, long id) {
        Bundle args = new Bundle();
        args.putLong("BudgetId", id);
        showFragment(EditBudgetFragment.class, args);
    }

    public void showCreateBudgetFragment(FragmentActivity activity) {
        showFragment(EditBudgetFragment.class, null);
    }

    private class AccountSort implements Comparator<Account> {

        @Override
        public int compare(Account one, Account two) {
            return one.getName().compareTo(two.getName());
        }
    }

    /**
     * Creates all of the scheduled transactions that are scheduled for today or
     * earlier and have been selected to auto created.
     */
    public void autoCreateScheduled() {

        ArrayList<ScheduledTransaction> transactions = getScheduledTransactions();

        Integer count = 0;
        Integer total = 0;
        boolean first = true;

        while (first || count > 0) {

            first = false;
            total += count;
            count = 0;

            for (int x = 0; x < transactions.size(); x++) {

                ScheduledTransaction t = transactions.get(x);

                Calendar cal = Calendar.getInstance();
                Date today = cal.getTime();

                SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
                Date transactionDate = t.getDate();

                cal = df.getCalendar();
                cal.add(Calendar.DATE, -(t.getCreateDays()));
                Date advanceCreateDate = cal.getTime();

                if (t.getAutoCreate()
                        && (transactionDate.before(today) || transactionDate.equals(today) || advanceCreateDate
                        .equals(today))) {

                    // create the transaction
                    createTransaction(t);

                    // update the scheduled transaction
                    t.updateDate();
                    updateScheduledTransaction(t);

                    count++;
                }
            }
        }

        if (total > 0) {
            notify(MoneyTrackerEvents.NOTIFY_SCHEDULED_CREATED, total);
        }

    }

    /**
     * Displays a notification that a transaction is scheduled and needs to be
     * created.
     */
    public void requestNotifyScheduled() {

        ArrayList<ScheduledTransaction> transactions = getScheduledTransactions();

        Integer count = 0;
        for (ScheduledTransaction t : transactions) {

            Calendar cal = Calendar.getInstance();
            Date today = cal.getTime();

            SimpleDateFormat df = new SimpleDateFormat(DATE_FORMAT);
            Date transactionDate = t.getDate();

            cal = df.getCalendar();
            cal.add(Calendar.DATE, -(t.getCreateDays()));
            Date advanceCreateDate = cal.getTime();

            if (t.getNotifyCreate()
                    && (transactionDate.before(today) || transactionDate.equals(today) || advanceCreateDate
                    .equals(today))) {

                count++;
            }
        }

        if (count > 0) {

            notify(MoneyTrackerEvents.NOTIFY_SCHEDULED, count);
        }
    }

    private String getPreference(final String tag, final String defaultValue) {

        // Restore preferences
        SharedPreferences settings = mContext.getSharedPreferences(PREFS_FILE, 0);
        String value = settings.getString(tag, defaultValue);

        return value;
    }

    private void setPreference(final String tag, final String value) {

        // Store preference
        SharedPreferences settings = mContext.getSharedPreferences(PREFS_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(tag, value);
        editor.commit();

        updateWidget();
    }

    private long getPreference(final String tag, long defaultValue) {

        long value = 0;

        // Restore preferences
        if (mContext != null) {
            SharedPreferences settings = mContext.getSharedPreferences(PREFS_FILE, 0);
            value = settings.getLong(tag, defaultValue);
        }

        return value;
    }

    private void setPreference(final String tag, long value) {

        // Store preference
        SharedPreferences settings = mContext.getSharedPreferences(PREFS_FILE, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(tag, value);
        editor.commit();

        updateWidget();
    }

    private void updateWidget() {
        // int num = getPreference(context, PREF_NUM_WIDGETS, 0);
        // int[] ids = new int[num];

        // for(Integer x = 0; x < num; x++) {
        // ids[x] = getPreference(context, "Widget" + x.toString(), 0);
        // }

        Intent widgetIntent = new Intent();
        widgetIntent.setAction(AppWidgetManager.ACTION_APPWIDGET_UPDATE);
        // widgetIntent.putExtra(AppWidgetManager.EXTRA_APPWIDGET_IDS, ids);

        // Send a broadcast to update the widget
        mContext.sendBroadcast(widgetIntent);
    }

    /**
     * Creates the transaction and adds it to the database. This method also
     * removes the scheduled transaction from the database, and inserts a copy'
     * with an updated date to the scheduled transactions database.
     *
     * @param item The scheduled transaction to create.
     */
    private void createTransaction(ScheduledTransaction scheduledTransaction) {

        Transaction t = new Transaction(scheduledTransaction.getMainAccountId(),
                scheduledTransaction.getSecondaryAccountId(), scheduledTransaction.getAmount(),
                scheduledTransaction.getDescription(), scheduledTransaction.getDate());

        insertTransaction(t);
    }

    public void createDefaultBudget() {
        mDatabaseManager.createDefaultBudget();
    }

    /**
     * Get a budget with the given unique id.
     *
     * @param id The budget id.
     * @return A budget or null if not found.
     */
    public Budget getBudget(Long id) {

        return Budget.get(id);
    }

    public void getArticle() {

        //new ArticleSelectionTask().execute("http://www.newsgofer.com/news/api/category/business/1");
    }

    public void getArticles(int count) {

        //new ArticleSelectionTask().execute("http://www.newsgofer.com/news/api/category/business/" + count);
    }

    public void showAd(View view) {

        LinearLayout layout = (LinearLayout) view.findViewById(R.id.adLayout);
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        LinearLayout ad_view = (LinearLayout) inflater.inflate(R.layout.mobclix_ad, null);

        MobclixMMABannerXLAdView adview_banner = (MobclixMMABannerXLAdView) ad_view
                .findViewById(R.id.advertising_banner_view);

        adview_banner.getAd();
        ad_view.removeAllViews();

        layout.addView(adview_banner);
    }

    private void showFragment(Class fragmentClass, Bundle bundle) {

        if (bundle == null) {
            bundle = new Bundle();
        }

        Intent intent = new Intent(mContext, HelperActivity.class);
        intent.putExtra("fragment", fragmentClass);
        intent.putExtra("bundle", bundle);
        mContext.startActivity(intent);
//        FragmentTransaction transaction = activity.getSupportFragmentManager().beginTransaction();
//        transaction.replace(android.R.id.content, fragment);
//        transaction.addToBackStack(null);
//        transaction.commit();
    }

    // Use UrlConnection Class instead of apache.net
//    private class ArticleSelectionTask extends AsyncTask<String, Void, Article[]> {
//
//        @Override
//        protected Article[] doInBackground(String... params) {
//
//            AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
//
//            Article[] articles = null;
//
//            try {
//                JSONArray json = new JSONArray();
//                URL url = new URL(params[0]);
//                HttpGet request = new HttpGet(url.toURI());
//                HttpResponse response = client.execute(request);
//                Header redirect = response.getFirstHeader("Location");
//
//                if (redirect != null) {
//                    url = new URL(redirect.getValue());
//                    request = new HttpGet(url.toURI());
//                    response = client.execute(request);
//                }
//
//                BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(),
//                        "UTF-8"));
//                StringBuilder builder = new StringBuilder();
//
//                for (String line = null; (line = reader.readLine()) != null; ) {
//                    builder.append(line).append("\n");
//                }
//
//                JSONTokener tokener = new JSONTokener(builder.toString());
//                json = new JSONArray(tokener);
//
//                articles = Article.create(json);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (JSONException e) {
//                e.printStackTrace();
//            } catch (URISyntaxException e) {
//                e.printStackTrace();
//            } finally {
//                client.close();
//            }
//
//            return articles;
//        }
//
//        @Override
//        protected void onPostExecute(Article[] result) {
//            MoneyTrackerController.this.notify(MoneyTrackerEvents.NEWS_UPDATED, result);
//        }
//
//        @Override
//        protected void onPreExecute() {
//        }
//
//        @Override
//        protected void onProgressUpdate(Void... values) {
//        }
//    }
}
