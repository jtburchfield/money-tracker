/*
 * To change this template, choose Tools | Templates and open the template in
 * the editor.
 */
package org.jtb.moneytracker.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.jtb.moneytracker.data.Model;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AccountFactory;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.data.budget.BudgetAccount;
import org.jtb.moneytracker.data.schedule.DailyScheduledTransaction;
import org.jtb.moneytracker.data.schedule.MonthlyScheduledTransaction;
import org.jtb.moneytracker.data.schedule.ScheduledTransaction;
import org.jtb.moneytracker.data.schedule.SemiMonthlyScheduledTransaction;
import org.jtb.moneytracker.data.schedule.WeeklyScheduledTransaction;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * 
 * @author jason
 */
public class DatabaseManager {

	private static boolean createDefaultBudget = false;

	// The tables to manage.
	private static final String TRANSACTIONS_TABLE = "Transactions";
	private static final String ACCOUNT_TABLE = "Accounts";
	private static final String SCHEDULE_TABLE = "Schedule";

	// The index (key) column name for use in where clauses.
	public static final String KEY_ID = "id";
	public static final int ID_COLUMN = 0;

	// The name and column index of each column in the Transactions table.
	public static final String KEY_DEBIT_ACCOUNT = "debitAccount";
	public static final int DEBIT_ACCOUNT_COLUMN = 1;
	public static final String KEY_CREDIT_ACCOUNT = "creditAccount";
	public static final int CREDIT_ACCOUNT_COLUMN = 2;
	public static final String KEY_DESCRIPTION = "description";
	public static final int DESCRIPTION_COLUMN = 3;
	public static final String KEY_AMOUNT = "amount";
	public static final int AMOUNT_COLUMN = 4;
	public static final String KEY_DATE = "date";
	public static final int DATE_COLUMN = 5;

	// The name and column index of each column in the Accounts table
	public static final String KEY_ACCOUNT_NAME = "name";
	public static final int ACCOUNT_NAME_COLUMN = 1;
	public static final String KEY_ACCOUNT_TYPE = "type";
	public static final int ACCOUNT_TYPE_COLUMN = 2;
	public static final String KEY_ACCOUNT_BALANCE = "balance";
	public static final int ACCOUNT_BALANCE_COLUMN = 3;
	public static final String KEY_ACCOUNT_PARENT_ID = "parentId";
	public static final int ACCOUNT_PARENT_ID_COLUMN = 4;
	// public static final String KEY_ACCOUNT_IS_CATEGORY = "isCategory";
	// public static final int ACCOUNT_IS_CATEGORY_COLUMN = 5;

	// The name and column index of each column in the Schedule table
	public static final String KEY_SCHEDULE_DATE = "date";
	public static final int SCHEDULE_DATE_COLUMN = 1;
	public static final String KEY_SCHEDULE_AMOUNT = "amount";
	public static final int SCHEDULE_AMOUNT_COLUMN = 2;
	public static final String KEY_SCHEDULE_FREQUENCY_TYPE = "frequencyType";
	public static final int SCHEDULE_FREQUENCY_TYPE_COLUMN = 3;
	public static final String KEY_SCHEDULE_DEBIT_ACCOUNT = "debit";
	public static final int SCHEDULE_DEBIT_ACCOUNT_COLUMN = 4;
	public static final String KEY_SCHEDULE_CREDIT_ACCOUNT = "credit";
	public static final int SCHEDULE_CREDIT_ACCOUNT_COLUMN = 5;
	public static final String KEY_SCHEDULE_TRANSACTION_TYPE = "transactionType";
	public static final int SCHEDULE_TRANSACTION_TYPE_COLUMN = 6;
	public static final String KEY_SCHEDULE_DESCRIPTION = "description";
	public static final int SCHEDULE_DESCRIPTION_COLUMN = 7;
	public static final String KEY_SCHEDULE_FREQUENCY = "frequency";
	public static final int SCHEDULE_FREQUENCY_COLUMN = 8;
	public static final String KEY_SCHEDULE_CREATE = "autoCreate";
	public static final int SCHEDULE_CREATE_COLUMN = 9;
	public static final String KEY_SCHEDULE_CREATE_DAYS = "numDaysCreate";
	public static final int SCHEDULE_CREATE_DAYS_COLUMN = 10;
	public static final String KEY_SCHEDULE_NOTIFY = "notify";
	public static final int SCHEDULE_NOTIFY_COLUMN = 11;
	public static final String KEY_SCHEDULE_NOTIFY_DAYS = "numDaysNotify";
	public static final int SCHEDULE_NOTIFY_DAYS_COLUMN = 12;

	// SQL Statement to create a new tables.
	private final String DATABASE_CREATE_ACCOUNTS = "create table " + ACCOUNT_TABLE + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_ACCOUNT_NAME + " text not null, " + KEY_ACCOUNT_TYPE
			+ " integer, " + KEY_ACCOUNT_BALANCE + " double, " + KEY_ACCOUNT_PARENT_ID + " integer);";

	// private final String DATABASE_ALTER_ACCOUNT_TABLE_1 = "alter table " +
	// ACCOUNT_TABLE + " add column " + ";

	// private final String DATABASE_ALTER_ACCOUNT_TABLE_2 = "alter table " +
	// ACCOUNT_TABLE + " add column "
	// + KEY_ACCOUNT_IS_CATEGORY + " integer";

	private final String DATABASE_CREATE_TRANSACTIONS = "create table " + TRANSACTIONS_TABLE + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_DEBIT_ACCOUNT + " integer, " + KEY_CREDIT_ACCOUNT
			+ " integer, " + KEY_DESCRIPTION + " text not null, " + KEY_AMOUNT + " double, " + KEY_DATE
			+ " text not null);";

	private final String DATABASE_CREATE_SCHEDULED = "create table " + SCHEDULE_TABLE + " (" + KEY_ID
			+ " integer primary key autoincrement, " + KEY_SCHEDULE_DATE + " text not null, " + KEY_SCHEDULE_AMOUNT
			+ " double, " + KEY_SCHEDULE_FREQUENCY_TYPE + " integer, " + KEY_SCHEDULE_DEBIT_ACCOUNT + " integer, "
			+ KEY_SCHEDULE_CREDIT_ACCOUNT + " integer, " + KEY_SCHEDULE_TRANSACTION_TYPE + " integer, "
			+ KEY_SCHEDULE_DESCRIPTION + " text not null, " + KEY_SCHEDULE_FREQUENCY + " text not null, "
			+ KEY_SCHEDULE_CREATE + " integer, " + KEY_SCHEDULE_CREATE_DAYS + " integer, " + KEY_SCHEDULE_NOTIFY
			+ " integer, " + KEY_SCHEDULE_NOTIFY_DAYS + " integer);";

	// Default Accounts
	private static final String[] DATABASE_INCOME_ACCOUNTS = { "Salary", "Other Income", "Interest Income",
			"Gifts Received", "Bonus" };

	private static final String[] DATABASE_EXPENSE_ACCOUNTS = { "Utilities", "Taxes", "Subscriptions",
			"Public Transportation", "Phone", "Pets", "Personal", "Miscellaneous", "Medical", "Laundry/Dry Cleaning",
			"Interest", "Insurance", "Household", "Home Repair", "Hobbies", "Groceries", "Fitness", "Entertainment",
			"Education", "Dining", "Clothes", "Charity", "Cash Withdrawal", "Cable", "Books", "A" + "uto Repair", "Gas",
			"Parking", "Rent" };

	private static final String[] DATABASE_BANK_ACCOUNTS = { "Checking", "Savings", "Money Market" };
	// private String[] DATABASE_ASSET_ACCOUNTS = {"Investments", "Vehicle",
	// "House"};
	// private String[] DATABASE_LIABILITIES_ACCOUNTS = {"Loans",
	// "Credit Card"};

	private static final int FREQUENCY_TYPE_DAILY = 0;
	private static final int FREQUENCY_TYPE_WEEKLY = 1;
	private static final int FREQUENCY_TYPE_SEMI_MONTHLY = 2;
	private static final int FREQUENCY_TYPE_MONTHLY = 3;

	// Variable to hold the database instance
	private static SQLiteDatabase db;

	// Database open/upgrade helper
	private final DatabaseHelper dbHelper;

	private final String mDateFormat;

	private static int openCount = 0;

	public DatabaseManager(Context context, String databaseName, int databaseVersion, final String dateFormat) {
		dbHelper = new DatabaseHelper(context, databaseName, null, databaseVersion);
		mDateFormat = dateFormat;
		Model.initialize(this, dbHelper);
	}

	public void createDefaultBudget() {
		if (createDefaultBudget) {
			Budget b = new Budget("Default");

			Account charity = getAccountByName("Charity");
			Account savings = getAccountByName("Savings");
			Account rent = getAccountByName("Rent");
			Account utilities = getAccountByName("Utilities");
			Account dining = getAccountByName("Dining");
			Account transportation = getAccountByName("Public Transportation");
			Account clothes = getAccountByName("Clothes");
			Account medical = getAccountByName("Medical");
			Account personal = getAccountByName("Personal");
			Account entertainment = getAccountByName("Entertainment");

			BudgetAccount charityBudgetAccount = new BudgetAccount(charity, 115d);
			BudgetAccount savingsBudgetAccount = new BudgetAccount(savings, 230d);
			BudgetAccount rentBudgetAccount = new BudgetAccount(rent, 460d);
			BudgetAccount utilitiesBudgetAccount = new BudgetAccount(utilities, 115d);
			BudgetAccount diningBudgetAccount = new BudgetAccount(dining, 115d);
			BudgetAccount transportationBudgetAccount = new BudgetAccount(transportation, 230d);
			BudgetAccount clothesBudgetAccount = new BudgetAccount(clothes, 45d);
			BudgetAccount medicalBudgetAccount = new BudgetAccount(medical, 115d);
			BudgetAccount personalBudgetAccount = new BudgetAccount(personal, 115d);
			BudgetAccount entertainmentBudgetAccount = new BudgetAccount(entertainment, 115d);

			b.add(charityBudgetAccount);
			b.add(savingsBudgetAccount);
			b.add(rentBudgetAccount);
			b.add(utilitiesBudgetAccount);
			b.add(diningBudgetAccount);
			b.add(transportationBudgetAccount);
			b.add(clothesBudgetAccount);
			b.add(medicalBudgetAccount);
			b.add(personalBudgetAccount);
			b.add(entertainmentBudgetAccount);

			b.save();

			createDefaultBudget = false;
		}
	}

	public Budget getBudget(Long id) {
		return Budget.get(id);
	}

	public static String[] getBankAccountNames() {
		return DATABASE_BANK_ACCOUNTS;
	}

	public static String[] getExpenseAccountNames() {
		return DATABASE_EXPENSE_ACCOUNTS;
	}

	public static String[] getIncomeAccountNames() {
		return DATABASE_INCOME_ACCOUNTS;
	}

	/**
	 * This method opens the database manager.
	 * 
	 * @return A new DatabaseManager object.
	 * @throws SQLException
	 */
	private DatabaseManager open() throws SQLException {

		try {
			if (openCount == 0) {
				db = dbHelper.getWritableDatabase();
			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		openCount++;

		return this;
	}

	/**
	 * Closes the database.
	 */
	private void close() {

		try {
			if (openCount == 1) {
				db.close();
			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		openCount--;
	}

	/**
	 * This method inserts a new account into the database.
	 * 
	 * @param account
	 *            - The account to insert.
	 * @return The id of the new account, -1 if unsuccessful
	 */
	public int insertAccount(Account account) {

		open();

		int id = -1;

		try {
			if (account != null) {

				ContentValues contentValues = this.getAccountContentValues(account);

				db.beginTransaction();
				id = (int) db.insert(ACCOUNT_TABLE, null, contentValues);
				db.setTransactionSuccessful();

			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			db.endTransaction();
		}

		close();
		return id;
	}

	/**
	 * This method updates an account in the database.
	 * 
	 * @param account
	 *            The account to update, recognized by the unique ID.
	 * @return The row number of the updated account in the database, -1 if
	 *         failure.
	 */
	public boolean updateAccount(Account account) {

		open();
		boolean result = false;

		try {
			if (account != null) {

				ContentValues clearSubAccounts = new ContentValues();
				clearSubAccounts.put(KEY_ACCOUNT_PARENT_ID, 0);

				ContentValues accountContentValues = getAccountContentValues(account);

				ContentValues subAccountContentValues = new ContentValues();
				subAccountContentValues.put(KEY_ACCOUNT_PARENT_ID, account.getId());

				String subAccountWhere = getSubAccountWhereClause(account.getSubAccounts());

				db.beginTransaction();
				db.update(ACCOUNT_TABLE, clearSubAccounts, KEY_ACCOUNT_PARENT_ID + "=" + account.getId(), null);
				db.update(ACCOUNT_TABLE, accountContentValues, KEY_ID + "=" + account.getId(), null);

				if (subAccountWhere.isEmpty() == false) {
					db.update(ACCOUNT_TABLE, subAccountContentValues, subAccountWhere, null);
				}

				db.setTransactionSuccessful();

				result = true;
			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		} finally {
			db.endTransaction();
		}

		close();

		return result;
	}

	private String getSubAccountWhereClause(ArrayList<Account> subAccounts) {

		StringBuffer subAccountWhere = new StringBuffer();

		for (int x = 0; x < subAccounts.size(); x++) {
			subAccountWhere.append(KEY_ID + "=" + subAccounts.get(x).getId());
			if (x < subAccounts.size() - 1) {
				subAccountWhere.append(" or ");
			}
		}

		return subAccountWhere.toString();
	}

	/**
	 * This method removes an account.
	 * 
	 * @param account
	 *            - The Account to remove.
	 * @return returns true if successful.
	 */
	public boolean deleteAccount(Account account) {

		open();

		boolean result = true;
		long id = account.getId();

		String where = KEY_DEBIT_ACCOUNT + "=" + id + " or " + KEY_CREDIT_ACCOUNT + "=" + id;
		String subAccountWhere = getSubAccountWhereClause(account.getSubAccounts());

		try {
			Cursor cursor = db.query(TRANSACTIONS_TABLE, null, where, null, null, null, null);

			boolean go = cursor.moveToFirst();
			while (go) {

				Transaction t = getTransaction(cursor);
				deleteTransaction(t);
				go = cursor.moveToNext();
			}

			cursor.close();

			db.beginTransaction();
			db.delete(ACCOUNT_TABLE, KEY_ID + "=" + id, null);

			if (subAccountWhere.isEmpty() == false) {
				db.delete(ACCOUNT_TABLE, subAccountWhere, null);
			}

			db.delete(SCHEDULE_TABLE, KEY_SCHEDULE_DEBIT_ACCOUNT + "=" + id, null);
			db.delete(SCHEDULE_TABLE, KEY_SCHEDULE_CREDIT_ACCOUNT + "=" + id, null);
			db.setTransactionSuccessful();
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
			result = false;
		} finally {
			db.endTransaction();
		}

		close();

		return result;
	}

	/**
	 * This method inserts a new transaction.
	 * 
	 * @param transaction
	 *            - The new transaction.
	 * @return the id of the new transaction.
	 */
	public long insertTransaction(Transaction transaction) {

		open();

		long id = -1;
		try {
			if (transaction != null) {

				ContentValues contentValues = getTransactionContentValues(transaction);

				id = db.insert(TRANSACTIONS_TABLE, null, contentValues);
			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		close();

		return id;
	}

	/**
	 * This method removes a transaction.
	 * 
	 * @param transaction
	 *            - The transaction to remove.
	 * @return true if successful.
	 */
	public boolean deleteTransaction(Transaction transaction) {

		open();
		boolean result = true;

		try {
			if (transaction != null) {

				Account creditAccount = this.getAccount(transaction.getCreditAccountId());
				Account debitAccount = this.getAccount(transaction.getDebitAccountId());

				creditAccount.debit(transaction.getAmount());
				debitAccount.credit(transaction.getAmount());

				this.updateAccount(debitAccount);
				this.updateAccount(creditAccount);

				db.delete(TRANSACTIONS_TABLE, KEY_ID + "=" + transaction.getId(), null);
			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
			result = false;
		}

		close();

		return result;
	}

	/**
	 * This method gets a list of transactions given the account id.
	 * 
	 * @param accountId
	 *            - The account id.
	 * @return A list of transactions.
	 */
	public ArrayList<Transaction> getTransactions(int accountId) {

		open();

		ArrayList<Transaction> transactions = new ArrayList<Transaction>();

		String where = KEY_DEBIT_ACCOUNT + "=" + accountId + " or " + KEY_CREDIT_ACCOUNT + "=" + accountId;

		try {
			Cursor cursor = db.query(TRANSACTIONS_TABLE, null, where, null, null, null, null);

			boolean go = cursor.moveToFirst();
			while (go) {

				Transaction t = this.getTransaction(cursor);

				transactions.add(t);
				go = cursor.moveToNext();
			}

			cursor.close();
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		close();
		return transactions;
	}

	/**
	 * This method gets a list of accounts that are of the given type.
	 * 
	 * @param accountType
	 *            - The account type.
	 * @return A list of accounts.
	 */
	public ArrayList<Account> getRootAccounts(String accountClass) {

		open();

		String[] parts = accountClass.split("\\.");
		String className = parts[parts.length - 1];

		String where = KEY_ACCOUNT_TYPE + " like \"%" + className + "%\" and " + KEY_ACCOUNT_PARENT_ID + "=0";

		Cursor cursor = db.query(ACCOUNT_TABLE, null, where, null, null, null, null);

		ArrayList<Account> accounts = new ArrayList<Account>();

		boolean go = cursor.moveToFirst();
		while (go) {

			Account a = getAccount(cursor);

			a.addSubAccounts(getSubAccounts(a.getId()));
			accounts.add(a);

			go = cursor.moveToNext();
		}

		cursor.close();
		close();

		return accounts;
	}

	/**
	 * This method gets a list of all top level accounts.
	 * 
	 * @return A list of all accounts.
	 */
	public ArrayList<Account> getRootAccounts() {

		open();

		ArrayList<Account> accounts = new ArrayList<Account>();

		try {
			accounts = getRootAccounts(BankAccount.class.toString());
			accounts.addAll(getRootAccounts(AssetAccount.class.toString()));
			accounts.addAll(getRootAccounts(LiabilityAccount.class.toString()));
			accounts.addAll(getRootAccounts(ExpenseAccount.class.toString()));
			accounts.addAll(getRootAccounts(IncomeAccount.class.toString()));
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		close();

		return accounts;
	}

	/**
	 * Get an account from the database.
	 * 
	 * @param acctId
	 *            - The id of the account.
	 * @return The account identified by the given id.
	 */
	public Account getAccount(long acctId) {

		open();

		Account account = null;

		try {
			Cursor cursor = db.query(ACCOUNT_TABLE, null, (KEY_ID + "=" + acctId), null, null, null, null);

			if (cursor.moveToFirst()) {

				account = getAccount(cursor);
			}

			cursor.close();
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		if (account != null) {
			account.addSubAccounts(getSubAccounts(account.getId()));
		}

		close();

		return account;
	}

	public Account getAccountByName(final String name) {

		open();

		Account account = null;

		try {
			Cursor cursor = db.query(ACCOUNT_TABLE, null, (KEY_ACCOUNT_NAME + "='" + name + "'"), null, null, null,
					null);

			if (cursor.moveToFirst()) {

				account = getAccount(cursor);
			}

			cursor.close();
		} catch (Exception e) {
			Log.e(DatabaseManager.class.getName(), e.getMessage());
		}

		close();

		return account;
	}

	/**
	 * This method gets a transaction with the given id.
	 * 
	 * @param transactionId
	 *            - The unique id of the desired transaction.
	 * @return The Transaction.
	 */
	public Transaction getTransaction(long transactionId) {

		open();
		Transaction transaction = null;

		try {
			Cursor cursor = db.query(TRANSACTIONS_TABLE, null, (KEY_ID + "=" + transactionId), null, null, null, null);

			if (cursor.moveToFirst()) {
				transaction = this.getTransaction(cursor);
			}

			cursor.close();
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		close();

		return transaction;
	}

	/**
	 * This method gets all scheduled transactions.
	 * 
	 * @return A list of scheduled transactions.
	 */
	public ArrayList<ScheduledTransaction> getScheduledTransactions() {

		ArrayList<ScheduledTransaction> transactions = new ArrayList<ScheduledTransaction>();

		open();

		try {
			Cursor cursor = db.query(SCHEDULE_TABLE, null, null, null, null, null, null);

			boolean go = cursor.moveToFirst();
			while (go) {

				ScheduledTransaction t = getScheduledTransaction(cursor);

				transactions.add(t);
				go = cursor.moveToNext();
			}

			cursor.close();
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		close();

		return transactions;
	}

	public ScheduledTransaction getScheduledTransaction(long transactionId) {

		ScheduledTransaction transaction = null;
		String where = KEY_ID + "=" + transactionId;

		open();

		try {
			Cursor cursor = db.query(SCHEDULE_TABLE, null, where, null, null, null, null);

			cursor.moveToFirst();

			transaction = getScheduledTransaction(cursor);
		} catch (Exception e) {
			Log.e(DatabaseManager.class.getName(), "");
		}
		close();

		return transaction;
	}

	/**
	 * This method inserts a new scheduled transaction into the database.
	 * 
	 * @param st
	 *            The new scheduled transaction.
	 * @return The row id of the new transaction, -1 on error.
	 */
	public long insertScheduledTransaction(ScheduledTransaction st) {

		long id = -1;
		open();

		try {
			if (st != null) {
				ContentValues contentValues = getScheduledTransactionContentValues(st);

				id = db.insert(SCHEDULE_TABLE, null, contentValues);
			}
		} catch (Exception e) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, e);
		}

		close();

		return id;
	}

	/**
	 * This method removes a scheduled transaction with the given id.
	 * 
	 * @param id
	 *            The unique id of the scheduled transaction to remove.
	 * @return
	 */
	public boolean deleteScheduledTransaction(long id) {

		open();
		db.delete(SCHEDULE_TABLE, KEY_ID + "=" + id, null);
		close();

		return true;
	}

	/**
	 * This method gets a list of accounts that are sub accounts to the given
	 * parent.
	 * 
	 * @param parentId
	 *            - The id of the parent for the accounts.
	 * @return List of Accounts
	 */
	private ArrayList<Account> getSubAccounts(long parentId) {

		String where = KEY_ACCOUNT_PARENT_ID + "=" + parentId;

		Cursor cursor = db.query(ACCOUNT_TABLE, null, where, null, null, null, null);

		ArrayList<Account> accounts = new ArrayList<Account>();

		boolean go = cursor.moveToFirst();
		while (go) {

			Account a = this.getAccount(cursor);

			accounts.add(a);
			go = cursor.moveToNext();
		}

		cursor.close();

		return accounts;
	}

	/**
	 * @param transaction
	 * @return
	 */
	private ContentValues getTransactionContentValues(Transaction transaction) {

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_DEBIT_ACCOUNT, transaction.getDebitAccountId());
		contentValues.put(KEY_CREDIT_ACCOUNT, transaction.getCreditAccountId());
		contentValues.put(KEY_AMOUNT, transaction.getAmount());
		contentValues.put(KEY_DESCRIPTION, transaction.getDescription());
		contentValues.put(KEY_DATE, new SimpleDateFormat(mDateFormat).format(transaction.getDate()));

		return contentValues;
	}

	/**
	 * @param cursor
	 * @return
	 */
	private Account getAccount(Cursor cursor) {

		Account account = null;

		try {

			// TODO remove .replace in version 30
			account = AccountFactory.getInstance().createAccount(cursor.getString(ACCOUNT_NAME_COLUMN),
					cursor.getDouble(ACCOUNT_BALANCE_COLUMN), cursor.getString(ACCOUNT_TYPE_COLUMN));

			account.setId(cursor.getInt(ID_COLUMN));
			account.setParentId(cursor.getInt(ACCOUNT_PARENT_ID_COLUMN));
		} catch (ClassNotFoundException ex) {
			Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
		}

		return account;
	}

	/**
	 * @param cursor
	 * @return
	 */
	private Transaction getTransaction(Cursor cursor) {

		Transaction transaction = null;

		try {
			double amount = cursor.getDouble(AMOUNT_COLUMN);
			int creditAccountId = cursor.getInt(CREDIT_ACCOUNT_COLUMN);
			int debitAccountId = cursor.getInt(DEBIT_ACCOUNT_COLUMN);
			String dateString = cursor.getString(DATE_COLUMN);
			String description = cursor.getString(DESCRIPTION_COLUMN);
			int id = cursor.getInt(ID_COLUMN);

			Date date = new SimpleDateFormat(mDateFormat).parse(dateString);

			transaction = new Transaction(debitAccountId, creditAccountId, amount, description, date, id);
		} catch (Exception ex) {
			Log.d(DatabaseManager.class.toString(), ex.getMessage());
		}
		return transaction;
	}

	/**
	 * @param cursor
	 * @return
	 */
	private ScheduledTransaction getScheduledTransaction(Cursor cursor) {

		ScheduledTransaction scheduledTransaction = null;

		if (cursor != null) {

			double amount = cursor.getDouble(SCHEDULE_AMOUNT_COLUMN);
			int secondaryAccountId = cursor.getInt(SCHEDULE_CREDIT_ACCOUNT_COLUMN);
			int mainAccountId = cursor.getInt(SCHEDULE_DEBIT_ACCOUNT_COLUMN);
			String dateString = cursor.getString(SCHEDULE_DATE_COLUMN);
			String description = cursor.getString(SCHEDULE_DESCRIPTION_COLUMN);
			int id = cursor.getInt(ID_COLUMN);

			Date date = null;

			try {
				date = new SimpleDateFormat(mDateFormat).parse(dateString);
			} catch (ParseException ex) {
				Log.d(DatabaseManager.class.toString(), ex.getMessage());
				return null;
			}

			int frequencyType = cursor.getInt(SCHEDULE_FREQUENCY_TYPE_COLUMN);
			int transactionType = cursor.getInt(SCHEDULE_TRANSACTION_TYPE_COLUMN);
			String frequency = cursor.getString(SCHEDULE_FREQUENCY_COLUMN);
			int notify = cursor.getInt(SCHEDULE_NOTIFY_COLUMN);
			int create = cursor.getInt(SCHEDULE_CREATE_COLUMN);
			int createDays = cursor.getInt(SCHEDULE_CREATE_DAYS_COLUMN);
			int notifyDays = cursor.getInt(SCHEDULE_NOTIFY_DAYS_COLUMN);

			switch (frequencyType) {
			case FREQUENCY_TYPE_DAILY:
				scheduledTransaction = new DailyScheduledTransaction(mainAccountId, secondaryAccountId, transactionType,
						frequency, date, description, amount, notify, create, createDays, notifyDays);
				break;

			case FREQUENCY_TYPE_MONTHLY:
				scheduledTransaction = new MonthlyScheduledTransaction(mainAccountId, secondaryAccountId,
						transactionType, frequency, date, description, amount, notify, create, createDays, notifyDays);
				break;

			case FREQUENCY_TYPE_SEMI_MONTHLY:
				scheduledTransaction = new SemiMonthlyScheduledTransaction(mainAccountId, secondaryAccountId,
						transactionType, frequency, date, description, amount, notify, create, createDays, notifyDays);
				break;

			case FREQUENCY_TYPE_WEEKLY:
				scheduledTransaction = new WeeklyScheduledTransaction(mainAccountId, secondaryAccountId,
						transactionType, frequency, date, description, amount, notify, create, createDays, notifyDays);
				break;
			}

			scheduledTransaction.setId(id);
		}

		return scheduledTransaction;
	}

	/**
	 * @param account
	 */
	private ContentValues getAccountContentValues(Account account) {

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_ACCOUNT_NAME, account.getName());
		contentValues.put(KEY_ACCOUNT_BALANCE, account.getBalance());
		contentValues.put(KEY_ACCOUNT_TYPE, account.getClass().getName());
		contentValues.put(KEY_ACCOUNT_PARENT_ID, account.getParentId());
		// contentValues.put(KEY_ACCOUNT_IS_CATEGORY, account.getIsCategory());

		// if (account.IsCategory()) {
		// contentValues.put(KEY_ACCOUNT_IS_CATEGORY, 1);
		// }
		// else {
		// contentValues.put(KEY_ACCOUNT_IS_CATEGORY, 0);
		// }

		return contentValues;
	}

	private ContentValues getScheduledTransactionContentValues(ScheduledTransaction st) {

		int frequencyType = -1;
		if (st.getClass() == DailyScheduledTransaction.class) {
			frequencyType = FREQUENCY_TYPE_DAILY;
		} else if (st.getClass() == WeeklyScheduledTransaction.class) {
			frequencyType = FREQUENCY_TYPE_WEEKLY;
		} else if (st.getClass() == MonthlyScheduledTransaction.class) {
			frequencyType = FREQUENCY_TYPE_MONTHLY;
		} else if (st.getClass() == SemiMonthlyScheduledTransaction.class) {
			frequencyType = FREQUENCY_TYPE_SEMI_MONTHLY;
		}

		ContentValues contentValues = new ContentValues();
		contentValues.put(KEY_SCHEDULE_AMOUNT, st.getAmount());
		contentValues.put(KEY_SCHEDULE_CREDIT_ACCOUNT, st.getSecondaryAccountId());
		contentValues.put(KEY_SCHEDULE_DATE, new SimpleDateFormat(mDateFormat).format(st.getDate()));
		contentValues.put(KEY_SCHEDULE_DEBIT_ACCOUNT, st.getMainAccountId());
		contentValues.put(KEY_SCHEDULE_DESCRIPTION, st.getDescription());
		contentValues.put(KEY_SCHEDULE_FREQUENCY, st.getFrequency());
		contentValues.put(KEY_SCHEDULE_FREQUENCY_TYPE, frequencyType);
		contentValues.put(KEY_SCHEDULE_CREATE, ((st.getAutoCreate()) ? 1 : 0));
		contentValues.put(KEY_SCHEDULE_CREATE_DAYS, st.getCreateDays());
		contentValues.put(KEY_SCHEDULE_NOTIFY, ((st.getNotifyCreate()) ? 1 : 0));
		contentValues.put(KEY_SCHEDULE_NOTIFY_DAYS, st.getNotifyDays());
		contentValues.put(KEY_SCHEDULE_TRANSACTION_TYPE, st.getTransactionType());

		return contentValues;
	}

	/**
	 * This is the database helper. It manages the creation and all updates to
	 * the database.
	 * 
	 * @author jason
	 * 
	 */
	private class DatabaseHelper extends SQLiteOpenHelper {

		public DatabaseHelper(Context context, String name, CursorFactory factory, int version) {
			super(context, name, factory, version);
		}

		// Called when no database exists in
		// disk and the helper class needs
		// to create a new one.
		@Override
		public void onCreate(SQLiteDatabase db) {

			Log.d("DatabaseHelper", "Creating Database");

			createDefaultBudget = true;

			db.execSQL(DATABASE_CREATE_TRANSACTIONS); // v1
			db.execSQL(DATABASE_CREATE_ACCOUNTS); // v1
			db.execSQL(DATABASE_CREATE_SCHEDULED); // v2
			// db.execSQL(DATABASE_ALTER_ACCOUNT_TABLE_1); // v3 1.5.7
			// db.execSQL(DATABASE_ALTER_ACCOUNT_TABLE_2); // v3 1.5.7

			DatabaseManager.db = db;

			db.beginTransaction();

			// Insert default expense accounts into database.
			for (int x = 0; x < DATABASE_EXPENSE_ACCOUNTS.length; x++) {

				Account a = new ExpenseAccount(DATABASE_EXPENSE_ACCOUNTS[x], 0.0);
				ContentValues contentValues = getAccountContentValues(a);
				db.insert(ACCOUNT_TABLE, null, contentValues);
			}

			// Insert default bank accounts into database.
			for (int x = 0; x < DATABASE_BANK_ACCOUNTS.length; x++) {

				Account a = new BankAccount(DATABASE_BANK_ACCOUNTS[x], 0.0);
				ContentValues contentValues = getAccountContentValues(a);
				db.insert(ACCOUNT_TABLE, null, contentValues);
			}

			// Insert default income accounts into database
			for (int x = 0; x < DATABASE_INCOME_ACCOUNTS.length; x++) {

				Account a = new IncomeAccount(DATABASE_INCOME_ACCOUNTS[x], 0.0);
				ContentValues contentValues = getAccountContentValues(a);
				db.insert(ACCOUNT_TABLE, null, contentValues);
			}

			db.setTransactionSuccessful();
			db.endTransaction();

			Budget.onCreate(db, DatabaseManager.this);
		}

		// Called when there is a database version mismatch meaning that
		// the version of the database on disk needs to be upgraded to
		// the current version.
		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// Log the version upgrade.
			Log.d("DatabaseHelper", "Upgrading from version " + oldVersion + " to " + newVersion);

			if (newVersion == 5) { // 1.7
				Budget.onCreate(db);
			} else {
				Budget.onUpgrade(db, oldVersion, newVersion);
			}
		}
		// private void relabelClassIds(SQLiteDatabase db) {
		//
		// // Account Types
		// final int ACCOUNT_TYPE_ASSET = 0;
		// final int ACCOUNT_TYPE_LIABILITY = 1;
		// final int ACCOUNT_TYPE_EXPENSE = 2;
		// final int ACCOUNT_TYPE_BANK = 3;
		// final int ACCOUNT_TYPE_INCOME = 4;
		//
		// // String where = KEY_ACCOUNT_TYPE + "=*";
		//
		// Cursor cursor = db.query(ACCOUNT_TABLE, null, null, null, null, null,
		// null);
		//
		// boolean go = cursor.moveToFirst();
		// while (go) {
		//
		// Account a = null;
		//
		// switch (cursor.getInt(ACCOUNT_TYPE_COLUMN)) {
		//
		// case ACCOUNT_TYPE_ASSET:
		// a = new AssetAccount(cursor.getString(ACCOUNT_NAME_COLUMN),
		// cursor.getDouble(ACCOUNT_BALANCE_COLUMN));
		// break;
		//
		// case ACCOUNT_TYPE_LIABILITY:
		// a = new LiabilityAccount(cursor.getString(ACCOUNT_NAME_COLUMN),
		// cursor.getDouble(ACCOUNT_BALANCE_COLUMN));
		// break;
		//
		// case ACCOUNT_TYPE_EXPENSE:
		// a = new ExpenseAccount(cursor.getString(ACCOUNT_NAME_COLUMN),
		// cursor.getDouble(ACCOUNT_BALANCE_COLUMN));
		// break;
		//
		// case ACCOUNT_TYPE_BANK:
		// a = new BankAccount(cursor.getString(ACCOUNT_NAME_COLUMN),
		// cursor.getDouble(ACCOUNT_BALANCE_COLUMN));
		// break;
		//
		// case ACCOUNT_TYPE_INCOME:
		// a = new IncomeAccount(cursor.getString(ACCOUNT_NAME_COLUMN),
		// cursor.getDouble(ACCOUNT_BALANCE_COLUMN));
		// break;
		// }
		//
		// if (a != null) {
		//
		// a.setId(cursor.getInt(ID_COLUMN));
		// a.setParentId(cursor.getInt(ACCOUNT_PARENT_ID_COLUMN));
		//
		// ContentValues clearSubAccounts = new ContentValues();
		// clearSubAccounts.put(KEY_ACCOUNT_PARENT_ID, 0);
		//
		// ContentValues accountContentValues = getAccountContentValues(a);
		//
		// ContentValues subAccountContentValues = new ContentValues();
		// subAccountContentValues.put(KEY_ACCOUNT_PARENT_ID, a.getId());
		//
		// String subAccountWhere =
		// getSubAccountWhereClause(a.getSubAccounts());
		//
		// db.beginTransaction();
		// db.update(ACCOUNT_TABLE, clearSubAccounts, KEY_ACCOUNT_PARENT_ID +
		// "=" + a.getId(), null);
		// db.update(ACCOUNT_TABLE, accountContentValues, KEY_ID + "=" +
		// a.getId(), null);
		//
		// if (subAccountWhere.isEmpty() == false) {
		// db.update(ACCOUNT_TABLE, subAccountContentValues, subAccountWhere,
		// null);
		// }
		//
		// db.setTransactionSuccessful();
		// db.endTransaction();
		// }
		//
		// go = cursor.moveToNext();
		// }
		//
		// cursor.close();
		// }
	}
}
