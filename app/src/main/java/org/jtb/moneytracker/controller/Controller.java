/**
 * 
 */
package org.jtb.moneytracker.controller;

import java.util.ArrayList;

import android.os.Handler;
import android.os.Message;

/**
 * @author jason
 * 
 */
public class Controller {

    private final ArrayList<Handler> listeners = new ArrayList<Handler>();

    public Controller() {
    }

    public void register(Handler h) {

        this.listeners.add(h);
    }

    public void remove(Handler h) {

        this.listeners.remove(h);
    }

    public void initialize() {
    }

    public void notify(int property, Object value) {

        for (Handler l : listeners) {

            Message msg = new Message();
            msg.what = property;
            msg.obj = value;
            l.sendMessage(msg);
        }
    }
}
