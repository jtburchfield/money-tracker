/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package org.jtb.moneytracker.controller;

/**
 * 
 * @author jason
 */
public abstract class MoneyTrackerEvents {

    public static final int MAIN_ACCOUNT = 0;
    public static final int TRANSACTION_CREATED = 1;
    public static final int TRANSACTION_DELETED = 2;
    public static final int TRANSACTION_UPDATED = 3;
    public static final int ACCOUNT_INSERTED = 4;
    public static final int ACCOUNT_DELETED = 5;
    public static final int ACCOUNT_UPDATED = 6;
    public static final int NOTIFY_SCHEDULED = 7;
    public static final int NOTIFY_SCHEDULED_CREATED = 8;
    public static final int SCHEDULED_TRANSACTION_UPDATED = 9;
    public static final int NEWS_UPDATED = 10;
}
