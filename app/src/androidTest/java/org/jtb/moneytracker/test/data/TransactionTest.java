package org.jtb.moneytracker.test.data;

import java.util.Calendar;
import java.util.Date;

import org.jtb.moneytracker.data.account.Transaction;

import junit.framework.Assert;
import junit.framework.TestCase;

public class TransactionTest extends TestCase {

    public void testTransactionConstructor() {
        
        int creditAccountId = 1;
        int debitAccountId = 2;
        double amount = 100d;
        String description = "This is a great description!";
        Date date = Calendar.getInstance().getTime();
        
        Transaction transaction = new Transaction(debitAccountId, creditAccountId, amount, description, date);
        
        Assert.assertEquals(debitAccountId, transaction.getDebitAccountId());
        Assert.assertEquals(creditAccountId, transaction.getCreditAccountId());
        Assert.assertEquals(amount, transaction.getAmount());
        Assert.assertEquals(description, transaction.getDescription());
        Assert.assertEquals(date.toString(), transaction.getDate().toString());
        Assert.assertEquals(-1, transaction.getId());
    }
    
    public void testTransactionConstructor2() {
        
        int creditAccountId = 1;
        int debitAccountId = 2;
        double amount = 100d;
        String description = "This is a better description";
        Date date = Calendar.getInstance().getTime();
        
        Transaction transaction = new Transaction(debitAccountId, creditAccountId, amount, description, date);
        
        int id = 1;
        Transaction transaction2 = new Transaction(transaction, id);
        
        Assert.assertEquals(debitAccountId, transaction2.getDebitAccountId());
        Assert.assertEquals(creditAccountId, transaction2.getCreditAccountId());
        Assert.assertEquals(amount, transaction2.getAmount());
        Assert.assertEquals(description, transaction2.getDescription());
        Assert.assertEquals(date.toString(), transaction2.getDate().toString());
        Assert.assertEquals(id, transaction2.getId());
    }
    
    public void testTransactionContructor3() {
        
        int creditAccountId = 1;
        int debitAccountId = 2;
        double amount = 100d;
        String description = "This is an even better description.";
        Date date = Calendar.getInstance().getTime();
        int id = 1;
        
        Transaction transaction = new Transaction(debitAccountId, creditAccountId, amount, description, date, id);
        
        Assert.assertEquals(debitAccountId, transaction.getDebitAccountId());
        Assert.assertEquals(creditAccountId, transaction.getCreditAccountId());
        Assert.assertEquals(amount, transaction.getAmount());
        Assert.assertEquals(description, transaction.getDescription());
        Assert.assertEquals(date.toString(), transaction.getDate().toString());
        Assert.assertEquals(id, transaction.getId());
    }
}
