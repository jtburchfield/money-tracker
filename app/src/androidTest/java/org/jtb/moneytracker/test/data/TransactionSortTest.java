package org.jtb.moneytracker.test.data;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.NewestFirstTransactionSort;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.account.TransactionSort;

public class TransactionSortTest extends TestCase {

    Account mBankAccount = null;
    Account mExpenseAccount = null;
    Account mIncomeAccount = null;

    public void setUp() {

	mBankAccount = new BankAccount("Bank", 0.0);
	mExpenseAccount = new ExpenseAccount("Expense", 0.0);
	mIncomeAccount = new IncomeAccount("Income", 0.0);
    }

    public void testSortTransactions() {

	Calendar c = Calendar.getInstance();

	Transaction t1 = new Transaction(mBankAccount.getId(), mIncomeAccount.getId(), 1000.0, "first", c.getTime());

	c.add(Calendar.DAY_OF_MONTH, -1);
	Transaction t2 = new Transaction(mExpenseAccount.getId(), mBankAccount.getId(), 23.33, "second", c.getTime());

	ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	transactions.add(t1);
	transactions.add(t2);

	Collections.sort(transactions, new TransactionSort());

	Assert.assertTrue(transactions.get(0).getDescription() == "second");
	Assert.assertTrue(transactions.get(1).getDescription() == "first");
    }

    public void testSortNewestFirstTransactionSort() {

	Calendar c = Calendar.getInstance();
	
	Transaction t1 = new Transaction(mBankAccount.getId(), mIncomeAccount.getId(), 1000.0, "first", c.getTime());

	c.add(Calendar.DAY_OF_MONTH, -1);
	Transaction t2 = new Transaction(mExpenseAccount.getId(), mBankAccount.getId(), 23.33, "second", c.getTime());

	ArrayList<Transaction> transactions = new ArrayList<Transaction>();
	transactions.add(t1);
	transactions.add(t2);

	Collections.sort(transactions, new NewestFirstTransactionSort());

	Assert.assertTrue(transactions.get(0).getDescription() == "first");
	Assert.assertTrue(transactions.get(1).getDescription() == "second");
    }
}
