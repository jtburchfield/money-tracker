package org.jtb.moneytracker.test.data;

import java.util.ArrayList;
import java.util.Calendar;

import junit.framework.Assert;

import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.data.budget.BudgetAccount;

import android.test.AndroidTestCase;

public class BudgetAccountTest extends AndroidTestCase {

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();
    private final String TEST_DATABASE_NAME = "BudgetAccountTest.db";
    private final int TEST_DATABASE_VERSION = 1;

    private final Long INITIAL_BUDGET_ACCOUNT_ID = -1L;

    @Override
    public void setUp() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);

        mController.initialize(getContext(), TEST_DATABASE_NAME, TEST_DATABASE_VERSION);
    }

    @Override
    public void tearDown() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);
    }

    public void testConstructor() {
        Account gasAccount = mController.getAccountByName("Gas");
        BudgetAccount budgetAccount = new BudgetAccount(gasAccount, 100d);

        Assert.assertEquals(INITIAL_BUDGET_ACCOUNT_ID, budgetAccount.getId());
        Assert.assertEquals(gasAccount.getName(), budgetAccount.getName());
        Assert.assertEquals(100d, budgetAccount.getBudgeted());
    }

    public void testSave() {

        Account gasAccount = mController.getAccountByName("Gas");
        BudgetAccount budgetAccount = new BudgetAccount(gasAccount, 100d);
        budgetAccount.save();

        Assert.assertTrue(budgetAccount.getId() != -1);

        BudgetAccount testAccount = BudgetAccount.get(budgetAccount.getId());

        Assert.assertTrue(testAccount != null);
        Assert.assertEquals(budgetAccount.getId(), testAccount.getId());
        Assert.assertEquals(budgetAccount.getName(), testAccount.getName());
        Assert.assertEquals(budgetAccount.getBudgeted(), testAccount.getBudgeted());
    }

    public void testGet() {

        Account acct1 = mController.getAccountByName("Gas");
        Account acct2 = mController.getAccountByName("Groceries");
        Account acct3 = mController.getAccountByName("Savings");

        BudgetAccount ba1 = new BudgetAccount(acct1, 20.0);
        BudgetAccount ba2 = new BudgetAccount(acct2, 202.0);
        BudgetAccount ba3 = new BudgetAccount(acct3, 2033.0);

        ba1.save();
        ba2.save();
        ba3.save();

        ArrayList<BudgetAccount> accounts = BudgetAccount.get(BudgetAccount.KEY_ID + "=" + ba1.getId() + " or "
                        + BudgetAccount.KEY_ID + "=" + ba2.getId());

        Assert.assertTrue(accounts != null);
        Assert.assertTrue(accounts.size() == 2);

        Assert.assertTrue(ba1.getId().equals(accounts.get(0).getId()));
        Assert.assertTrue(ba1.getName().equals(accounts.get(0).getName()));
        Assert.assertTrue(ba1.getBudgeted().equals(accounts.get(0).getBudgeted()));

        Assert.assertTrue(ba2.getId().equals(accounts.get(1).getId()));
        Assert.assertTrue(ba2.getName().equals(accounts.get(1).getName()));
        Assert.assertTrue(ba2.getBudgeted().equals(accounts.get(1).getBudgeted()));
    }

    public void testDelete() {

        Account gasAccount = mController.getAccountByName("Gas");
        BudgetAccount budgetAccount = new BudgetAccount(gasAccount, 100d);
        long id = budgetAccount.getId();

        budgetAccount.delete();

        Assert.assertTrue(budgetAccount.getId() == -1L);

        BudgetAccount testAccount = BudgetAccount.get(id);

        Assert.assertTrue(testAccount == null);
    }

    public void testGetByBudgetId() {

        Account gasAccount = mController.getAccountByName("Charity");

        Budget a = new Budget("stop");
        a.save();

        Budget b = new Budget("boom");
        b.save();

        b.add(new BudgetAccount(gasAccount, 1000.0));
        b.save();

        ArrayList<BudgetAccount> data = BudgetAccount.getByBudgetId(b.getId());

        Assert.assertTrue(data != null);
        Assert.assertTrue(data.size() == 1);
    }

    public void testGetRemaining() {

        Double budgeted = 1000d;
        Account checkingAccount = mController.getAccountByName("Checking");
        Account charityAccount = mController.getAccountByName("Charity");
        BudgetAccount budgetAccount = new BudgetAccount(charityAccount, budgeted);

        Calendar calendar = Calendar.getInstance();
        Transaction transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 10, "Boom", calendar.getTime());
        mController.insertTransaction(transaction);

        transaction = new Transaction(checkingAccount.getId(), charityAccount.getId(), 25, "Bam", calendar.getTime());
        mController.insertTransaction(transaction);

        calendar.set(Calendar.DAY_OF_MONTH, 1);
        transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 125, "Bammo", calendar.getTime());
        mController.insertTransaction(transaction);

        calendar.add(Calendar.YEAR, -1);
        transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 250, "Kaboom", calendar.getTime());
        mController.insertTransaction(transaction);

        Assert.assertTrue(budgetAccount.getRemaining().equals(budgeted - 10 - 125));
    }

    public void testGetProgress() {

        Double budgeted = 1000d;
        Account checkingAccount = mController.getAccountByName("Checking");
        Account charityAccount = mController.getAccountByName("Charity");
        BudgetAccount budgetAccount = new BudgetAccount(charityAccount, budgeted);

        Calendar calendar = Calendar.getInstance();
        Transaction transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 100, "Boom",
                        calendar.getTime());
        mController.insertTransaction(transaction);

        transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 250, "Bam", calendar.getTime());
        mController.insertTransaction(transaction);

        Assert.assertTrue(budgetAccount.getProgress() == 35);
    }

    public void testSetGetBudgeted() {
        Account charityAccount = mController.getAccountByName("Charity");
        BudgetAccount budgetAccount = new BudgetAccount(charityAccount, 0d);
        budgetAccount.setBudgeted(1000d);

        Assert.assertTrue(budgetAccount.getBudgeted().equals(1000d));
    }
    
    public void testGetAccount() {
    	
    	Account charityAccount = mController.getAccountByName("Charity");
        BudgetAccount budgetAccount = new BudgetAccount(charityAccount, 0d);
        
        Assert.assertTrue(budgetAccount.getAccount().getId() == charityAccount.getId());
    }
}
