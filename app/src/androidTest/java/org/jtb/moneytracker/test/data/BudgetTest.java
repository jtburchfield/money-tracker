package org.jtb.moneytracker.test.data;

import java.util.ArrayList;
import java.util.Calendar;

import junit.framework.Assert;

import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.data.budget.BudgetAccount;

import android.test.AndroidTestCase;

public class BudgetTest extends AndroidTestCase {

    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();
    private final String TEST_DATABASE_NAME = "BudgetTest.db";
    private final int TEST_DATABASE_VERSION = MoneyTrackerController.getDatabaseVersion();

    private final String TEST_BUDGET_NAME = "TestBudget";
    private final Long INITIAL_BUDGET_ID = -1L;
    private Budget mTestBudget = null;

    @Override
    public void setUp() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);

        mController.initialize(getContext(), TEST_DATABASE_NAME, TEST_DATABASE_VERSION);
        mTestBudget = new Budget(TEST_BUDGET_NAME);
    }

    @Override
    public void tearDown() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);

        mTestBudget = null;
    }

    public void testConstructor() {

        Assert.assertEquals(INITIAL_BUDGET_ID, mTestBudget.getId());
        Assert.assertEquals(TEST_BUDGET_NAME, mTestBudget.getName());
    }

    public void testSave() {

        String charity = "Charity";
        String savings = "Savings";
        String dining = "Dining";

        Account charityAccount = mController.getAccountByName(charity);
        Account savingsAccount = mController.getAccountByName(savings);
        Account diningAccount = mController.getAccountByName(dining);

        mTestBudget.add(new BudgetAccount(charityAccount, 100.0));
        mTestBudget.add(new BudgetAccount(savingsAccount, 100.0));
        mTestBudget.add(new BudgetAccount(diningAccount, 100.0));

        boolean saved = mTestBudget.save();

        boolean charityFound = false;
        boolean savingsFound = false;
        boolean diningFound = false;

        ArrayList<BudgetAccount> budgetAccountList = mTestBudget.getAccounts();
        for (BudgetAccount budgetAccount : budgetAccountList) {

            if (budgetAccount.getName().equals(charity)) {
                charityFound = true;
            }
            else if (budgetAccount.getName().equals(savings)) {
                savingsFound = true;
            }
            else if (budgetAccount.getName().equals(dining)) {
                diningFound = true;
            }
        }

        if (!charityFound || !savingsFound || !diningFound) {
            Assert.assertTrue(false);
        }

        // Make sure the budget is stored
        Assert.assertTrue(saved);

        Budget testBudget = Budget.get(mTestBudget.getId());

        Assert.assertTrue(testBudget != null);
        Assert.assertEquals(mTestBudget.getId(), testBudget.getId());
        Assert.assertEquals(mTestBudget.getName(), testBudget.getName());
        Assert.assertEquals(mTestBudget.getBudgeted(), testBudget.getBudgeted());

        ArrayList<BudgetAccount> retrievedAccountList = testBudget.getAccounts();

        Assert.assertEquals(budgetAccountList.size(), retrievedAccountList.size());
        for (BudgetAccount budgetAccount : retrievedAccountList) {

            if (budgetAccount.getName().equals(charity)) {
                charityFound = true;
            }
            else if (budgetAccount.getName().equals(savings)) {
                savingsFound = true;
            }
            else if (budgetAccount.getName().equals(dining)) {
                diningFound = true;
            }
        }

        if (!charityFound || !savingsFound || !diningFound) {
            Assert.assertTrue(false);
        }
    }

    public void testGetAll() {

        Budget a = new Budget("Boom");
        Budget b = new Budget("Taco");

        BudgetAccount acct = new BudgetAccount(mController.getAccount(30L), 100.0);
        b.add(acct);

        a.save();
        b.save();

        ArrayList<Budget> budgetList = Budget.getAll();

        Assert.assertTrue(budgetList != null);
        Assert.assertTrue(budgetList.size() == 2);
        Assert.assertTrue(budgetList.get(0).getName().equals("Boom"));
        Assert.assertTrue(budgetList.get(1).getAccounts().size() == 1);
    }

    public void testGetBudgetNames() throws Throwable {

        Budget test = new Budget("test");
        Budget test2 = new Budget("test2");

        test.save();
        test2.save();

        ArrayList<String> names = Budget.getNames();

        Assert.assertTrue(names != null);
        Assert.assertTrue(names.size() == 2);
        Assert.assertTrue(names.get(0).equals("test"));
        Assert.assertTrue(names.get(1).equals("test2"));
    }

    public void testBudgetDoesNotExist() {

        Budget b = Budget.get(1000L);
        Assert.assertTrue(b == null);
    }

    public void testGetRemaining() {

        Account charityAccount = mController.getAccountByName("Charity");
        Account checkingAccount = mController.getAccountByName("Checking");
        Account gasAccount = mController.getAccountByName("Gas");

        mTestBudget.add(new BudgetAccount(charityAccount, 120.0));
        mTestBudget.add(new BudgetAccount(gasAccount, 300.22));

        Calendar calendar = Calendar.getInstance();

        Transaction transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 100, "Boom",
                        calendar.getTime());
        mController.insertTransaction(transaction);

        transaction = new Transaction(gasAccount.getId(), checkingAccount.getId(), 75.25, "Slap", calendar.getTime());
        mController.insertTransaction(transaction);

        Assert.assertTrue(mTestBudget.getRemaining().equals(300.22 + 120 - 75.25 - 100));
    }

    public void testGetBudgeted() {

        Account charityAccount = mController.getAccountByName("Charity");
        Account checkingAccount = mController.getAccountByName("Checking");
        Account gasAccount = mController.getAccountByName("Gas");

        mTestBudget.add(new BudgetAccount(charityAccount, 120.0));
        mTestBudget.add(new BudgetAccount(gasAccount, 300.22));

        Calendar calendar = Calendar.getInstance();

        Transaction transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 100, "Boom",
                        calendar.getTime());
        mController.insertTransaction(transaction);

        transaction = new Transaction(gasAccount.getId(), checkingAccount.getId(), 75.25, "Slap", calendar.getTime());
        mController.insertTransaction(transaction);

        Assert.assertTrue(mTestBudget.getBudgeted().equals(420.22));
    }

    public void testGetProgress() {

        Account charityAccount = mController.getAccountByName("Charity");
        Account checkingAccount = mController.getAccountByName("Checking");
        Account gasAccount = mController.getAccountByName("Gas");

        mTestBudget.add(new BudgetAccount(charityAccount, 120.0));
        mTestBudget.add(new BudgetAccount(gasAccount, 300.22));

        Calendar calendar = Calendar.getInstance();

        Transaction transaction = new Transaction(charityAccount.getId(), checkingAccount.getId(), 100, "Boom",
                        calendar.getTime());
        mController.insertTransaction(transaction);

        transaction = new Transaction(gasAccount.getId(), checkingAccount.getId(), 75.25, "Slap", calendar.getTime());
        mController.insertTransaction(transaction);

        Assert.assertTrue(mTestBudget.getProgress() == 41);
    }
    
    public void testSetName() {
    	
    	mTestBudget.setName("Slam");
    	Assert.assertTrue(mTestBudget.getName().equals("Slam"));
    }
    
    public void testDelete() {
    	
    	Long id = mTestBudget.getId();
    	mTestBudget.delete();
    	Assert.assertTrue(mTestBudget.getId() == -1);
    	
    	Budget deleted = Budget.get(id);
    	Assert.assertTrue(deleted == null);
    	
    }
    
    public void testRemove() {
    	
    	Account charityAccount = mController.getAccountByName("Charity");
        Account gasAccount = mController.getAccountByName("Gas");

        BudgetAccount acct = new BudgetAccount(gasAccount, 300.22);
        
        mTestBudget.add(new BudgetAccount(charityAccount, 120.0));
        mTestBudget.add(acct);
        
        mTestBudget.save();        
        mTestBudget.remove(acct);
        
        Assert.assertTrue(mTestBudget.getAccounts().size() == 1);
        
        mTestBudget.save();        
        mTestBudget = Budget.get(mTestBudget.getId());
        
        Assert.assertTrue(mTestBudget.getAccounts().size() == 1);
    }
    
    public void testMultipleAccountsWithSameNameInvalid() {
    	
    	Account charityAccount = mController.getAccountByName("Charity");
    	mTestBudget.add(new BudgetAccount(charityAccount, 0d));
    	mTestBudget.add(new BudgetAccount(charityAccount, 0d));
    	
    	Assert.assertTrue(mTestBudget.getAccounts().size() == 1);
    }
}
