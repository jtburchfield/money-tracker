package org.jtb.moneytracker.test.data;

import android.test.AndroidTestCase;

public class AccountTest extends AndroidTestCase {

    private final String TEST_DATABASE_NAME = "AccountTest.db";

    // private final int TEST_DATABASE_VERSION =
    // MoneyTrackerController.getDatabaseVersion();
    // private final String DATE_FORMAT =
    // MoneyTrackerController.getDateFormat();

    @Override
    public void setUp() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);

        // new DatabaseManager(getContext(), TEST_DATABASE_NAME,
        // TEST_DATABASE_VERSION, DATE_FORMAT);
        // mTestBudget = new Budget(TEST_BUDGET_NAME, TEST_BUDGET_AMOUNT);
    }

    @Override
    public void tearDown() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);

        // mTestBudget = null;
    }
}
