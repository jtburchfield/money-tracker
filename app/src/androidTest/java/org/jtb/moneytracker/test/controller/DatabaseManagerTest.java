package org.jtb.moneytracker.test.controller;

import junit.framework.Assert;

import org.jtb.moneytracker.controller.DatabaseManager;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.data.budget.Budget;

import android.test.AndroidTestCase;

public class DatabaseManagerTest extends AndroidTestCase {

    private DatabaseManager mDatabaseManager = null;
    private final String TEST_DATABASE_NAME = "DatabaseManagerTest.db";
    private final int TEST_DATABASE_VERSION = MoneyTrackerController.getDatabaseVersion();
    private final String DATE_FORMAT = MoneyTrackerController.getDateFormat();

    @Override
    public void setUp() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);
        mDatabaseManager = new DatabaseManager(getContext(), TEST_DATABASE_NAME, TEST_DATABASE_VERSION, DATE_FORMAT);
    }

    @Override
    public void tearDown() {
        getContext().deleteDatabase(TEST_DATABASE_NAME);
    }

    public void testCreateDefaultBudget() {

        Budget b = mDatabaseManager.getBudget(1L);

        Assert.assertTrue(b == null);

        mDatabaseManager.createDefaultBudget();
        b = mDatabaseManager.getBudget(1L);

        Assert.assertTrue(b != null);
        Assert.assertTrue(b.getName().equals("Default"));
        Assert.assertTrue(b.getAccounts().size() == 10);
    }

}
