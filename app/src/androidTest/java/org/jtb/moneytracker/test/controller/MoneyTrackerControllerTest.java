package org.jtb.moneytracker.test.controller;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import junit.framework.Assert;

import org.jtb.moneytracker.controller.DatabaseManager;
import org.jtb.moneytracker.controller.MoneyTrackerController;
import org.jtb.moneytracker.controller.MoneyTrackerEvents;
import org.jtb.moneytracker.data.account.Account;
import org.jtb.moneytracker.data.account.AssetAccount;
import org.jtb.moneytracker.data.account.BankAccount;
import org.jtb.moneytracker.data.account.ExpenseAccount;
import org.jtb.moneytracker.data.account.IncomeAccount;
import org.jtb.moneytracker.data.account.LiabilityAccount;
import org.jtb.moneytracker.data.account.Transaction;
import org.jtb.moneytracker.data.budget.Budget;
import org.jtb.moneytracker.newsgofer.Article;

import android.os.Handler;
import android.os.Message;
import android.test.AndroidTestCase;

public class MoneyTrackerControllerTest extends AndroidTestCase {

    private final String TEST_DATABASE = "MoneyTrackerControllerTest.db";
    private final MoneyTrackerController mController = MoneyTrackerController.getInstance();

    private ArrayList<Article> mNewsArticles = new ArrayList<Article>();

    private final Handler mHandler = new Handler() {

	@SuppressWarnings("unchecked")
	public void handleMessage(Message message) {

	    switch (message.what) {

	    case MoneyTrackerEvents.MAIN_ACCOUNT:
		break;

	    case MoneyTrackerEvents.TRANSACTION_CREATED:
		break;

	    case MoneyTrackerEvents.TRANSACTION_DELETED:
		break;

	    case MoneyTrackerEvents.TRANSACTION_UPDATED:
		break;

	    case MoneyTrackerEvents.ACCOUNT_INSERTED:
		break;

	    case MoneyTrackerEvents.ACCOUNT_DELETED:
		break;

	    case MoneyTrackerEvents.ACCOUNT_UPDATED:
		break;

	    case MoneyTrackerEvents.NOTIFY_SCHEDULED:
		break;

	    case MoneyTrackerEvents.NOTIFY_SCHEDULED_CREATED:
		break;

	    case MoneyTrackerEvents.SCHEDULED_TRANSACTION_UPDATED:
		break;

	    case MoneyTrackerEvents.NEWS_UPDATED:
		mNewsArticles = (ArrayList<Article>) message.obj;
		break;
	    }
	}
    };

    public MoneyTrackerControllerTest() {

    }

    @Override
    public void setUp() {
	getContext().deleteDatabase(TEST_DATABASE);

	mController.initialize(getContext(), TEST_DATABASE, MoneyTrackerController.DATABASE_VERSION);
	mNewsArticles.clear();
    }

    @Override
    public void tearDown() {
	getContext().deleteDatabase(TEST_DATABASE);
    }

    public void testInsertAccount() throws Throwable {

	BankAccount account = new BankAccount("test", 40d);
	mController.insertAccount(account);
	Account result = mController.getAccount(account.getId());

	Assert.assertEquals(account.getName(), result.getName());
	Assert.assertEquals(account.getBalance(), result.getBalance());
	Assert.assertEquals(account.getId(), result.getId());
	Assert.assertEquals(account.getParentId(), result.getParentId());
	Assert.assertEquals(account.getTypeName(), result.getTypeName());
	Assert.assertEquals(account.getSubAccounts().size(), result.getSubAccounts().size());
	Assert.assertEquals(BankAccount.class, account.getClass());
    }

    public void testGetAccountByName() {

	Account account = mController.getAccountByName("Checking");

	Assert.assertNotNull(account);
	Assert.assertEquals("Checking", account.getName());
    }

    /**
     * Tests for invalid name.
     */
    public void testGetAccountByName2() {

	Account account = mController.getAccountByName("tube");

	Assert.assertNull(account);
    }

    // public void testInsertAccountWithSubAccounts() throws Throwable {
    //
    // mController.setMainContext(getContext(), MONEY_TRACKER_TEST_DATABASE,
    // DatabaseManager.DATABASE_VERSION);
    //
    // final int SUB_ACCOUNT_SIZE = 20;
    // final int SUB_SUB_ACCOUNT_SIZE = 5;
    //
    // BankAccount account = new BankAccount("test", 400d);
    // for (int x = 0; x < SUB_ACCOUNT_SIZE; x++) {
    // Account sub = new BankAccount("boom" + Integer.toString(x), (double) x);
    // account.addSubAccount(sub);
    // for (int y = 0; y < SUB_SUB_ACCOUNT_SIZE; y++) {
    // sub.addSubAccount(new BankAccount("subsub" + Integer.toString(y),
    // (double) (y + x)));
    // }
    // }
    // mController.insertAccount(account);
    // Account result = mController.getAccount(account.getId());
    //
    // Assert.assertEquals(SUB_ACCOUNT_SIZE, result.getSubAccounts().size());
    //
    // for (int z = 0; z < result.getSubAccounts().size(); z++) {
    // Assert.assertEquals(SUB_SUB_ACCOUNT_SIZE,
    // result.getSubAccounts().get(z).getSubAccounts());
    // }
    // }

    public void testDeleteAccount() throws Throwable {

	BankAccount account = new BankAccount("test", 10d);
	mController.insertAccount(account);
	mController.deleteAccount(account);
	Account result = mController.getAccount(account.getId());

	Assert.assertNull(result);
    }

    public void testUpdateAccount() throws Throwable {

	BankAccount account = new BankAccount("boom", 300d);
	mController.insertAccount(account);
    }

    public void testInsertUpdateWithSubAccount() throws Throwable {

	BankAccount account = new BankAccount("corn", 4000d);
	mController.insertAccount(account);

	BankAccount subAccount = new BankAccount("cornSub", 500d);
	mController.insertAccount(subAccount);

	account.addSubAccount(subAccount);
	mController.updateAccount(account);

	Account result = mController.getAccount(account.getId());

	Assert.assertEquals(1, result.getSubAccounts().size());
    }

    public void testGetRootAcconts() throws Throwable {

	List<Account> bankAccountList = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccountList = mController.getRootAccounts(ExpenseAccount.class.toString());
	List<Account> incomeAccountList = mController.getRootAccounts(IncomeAccount.class.toString());
	List<Account> assetAccountList = mController.getRootAccounts(AssetAccount.class.toString());
	List<Account> liabilityAccountList = mController.getRootAccounts(LiabilityAccount.class.toString());

	// Make sure we have the same number of accounts
	Assert.assertEquals(DatabaseManager.getBankAccountNames().length, bankAccountList.size());
	Assert.assertEquals(DatabaseManager.getExpenseAccountNames().length, expenseAccountList.size());
	Assert.assertEquals(DatabaseManager.getIncomeAccountNames().length, incomeAccountList.size());
	Assert.assertEquals(0, assetAccountList.size());
	Assert.assertEquals(0, liabilityAccountList.size());
    }

    public void testGetAccountTypeName_EmptyName() throws Throwable {

	String name = mController.getAccountTypeName("");

	Assert.assertNotNull(name);
	Assert.assertEquals("", name);
    }

    public void testGetAccountTypeName() throws Throwable {

	String expected = BankAccount.class.getName();
	String name = mController.getAccountTypeName("BankAccount");

	Assert.assertNotNull(name);
	Assert.assertEquals(expected, name);
    }

    public void testInsertTransaction() throws Throwable {

	List<Account> bankAccounts = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccounts = mController.getRootAccounts(ExpenseAccount.class.toString());

	Account debitAccount = expenseAccounts.get(0);
	Account creditAccount = bankAccounts.get(0);

	Date date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse("10-04-2011");
	Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 100d,
		"This is my description", date);

	Transaction resultTransaction = mController.insertTransaction(transaction);
	Transaction result = mController.getTransaction(resultTransaction.getId());

	Assert.assertEquals(debitAccount.getId(), result.getDebitAccountId());
	Assert.assertEquals(creditAccount.getId(), result.getCreditAccountId());
	Assert.assertEquals(transaction.getDate(), result.getDate());
	Assert.assertEquals(transaction.getAmount(), result.getAmount());
	Assert.assertEquals(transaction.getDescription(), result.getDescription());
    }

    public void testInsertTransactionMultiple() throws Throwable {

	List<Account> bankAccounts = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccounts = mController.getRootAccounts(ExpenseAccount.class.toString());
	List<Account> incomeAccounts = mController.getRootAccounts(IncomeAccount.class.toString());

	Account creditAccount = incomeAccounts.get(0);
	Account debitAccount = bankAccounts.get(0);

	String description = "This is my description";
	Date date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse("10-04-2011");

	Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 1000d, description, date);
	mController.insertTransaction(transaction);

	creditAccount = debitAccount;
	debitAccount = expenseAccounts.get(0);

	transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 22.35, description, date);
	mController.insertTransaction(transaction);

	transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 15.13, description, date);
	mController.insertTransaction(transaction);

	Account result = mController.getAccount(bankAccounts.get(0).getId());
	Assert.assertEquals(962.52, result.getBalance());
    }

    public void testInsertTransactionMultipleWithSubAccount() throws Throwable {

	List<Account> bankAccounts = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccounts = mController.getRootAccounts(ExpenseAccount.class.toString());
	List<Account> incomeAccounts = mController.getRootAccounts(IncomeAccount.class.toString());

	Account creditAccount = incomeAccounts.get(0);
	Account debitAccount = bankAccounts.get(0);

	String description = "This is my description";
	Date date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse("10-04-2011");

	Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 1000d, description, date);
	mController.insertTransaction(transaction);

	creditAccount = debitAccount;
	debitAccount = expenseAccounts.get(0);

	transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 22.35, description, date);
	mController.insertTransaction(transaction);

	transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 15.13, description, date);
	mController.insertTransaction(transaction);

	Account result = mController.getAccount(bankAccounts.get(0).getId());
	Assert.assertEquals(962.52, result.getBalance());
    }

    public void testDeleteTransaction() throws Throwable {

	List<Account> bankAccounts = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccounts = mController.getRootAccounts(ExpenseAccount.class.toString());

	Account debitAccount = expenseAccounts.get(0);
	Account creditAccount = bankAccounts.get(0);

	String description = "This is my description";
	Date date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse("10-04-2011");

	Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 100d, description, date);

	mController.insertTransaction(transaction);
	mController.deleteTransaction(transaction);
	Transaction result = mController.getTransaction(transaction.getId());

	Assert.assertNull(result);
    }

    public void testUpdateTransaction() throws Throwable {

	List<Account> bankAccounts = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccounts = mController.getRootAccounts(ExpenseAccount.class.toString());

	Account debitAccount = expenseAccounts.get(0);
	Account creditAccount = bankAccounts.get(0);

	String description = "This is my description";
	Date date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse("10-04-2011");

	Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 100d, description, date);

	mController.insertTransaction(transaction);

	Account debitAccount2 = expenseAccounts.get(1);
	Account creditAccount2 = bankAccounts.get(1);

	Transaction newTransaction = new Transaction(debitAccount2.getId(), creditAccount2.getId(), 150d, description
		+ "2", date);

	Transaction updatedTransaction = mController.updateTransaction(transaction, newTransaction);
	Transaction result = mController.getTransaction(updatedTransaction.getId());

	Assert.assertEquals(debitAccount2.getId(), result.getDebitAccountId());
	Assert.assertEquals(creditAccount2.getId(), result.getCreditAccountId());
	Assert.assertEquals(newTransaction.getDate(), result.getDate());
	Assert.assertEquals(newTransaction.getAmount(), result.getAmount());
	Assert.assertEquals(newTransaction.getDescription(), result.getDescription());
    }

    public void testGetTransactions() throws Throwable {

	List<Account> bankAccounts = mController.getRootAccounts(BankAccount.class.toString());
	List<Account> expenseAccounts = mController.getRootAccounts(ExpenseAccount.class.toString());

	Account debitAccount = expenseAccounts.get(0);
	Account creditAccount = bankAccounts.get(0);

	String description = "This is my description";
	Date date = new SimpleDateFormat(MoneyTrackerController.getDateFormat()).parse("10-04-2011");

	Transaction transaction = new Transaction(debitAccount.getId(), creditAccount.getId(), 100d, description, date);

	mController.insertTransaction(transaction);
	mController.insertTransaction(transaction);

	ArrayList<Transaction> debitAccountTransactionList = mController.getTransactions(debitAccount.getId());
	ArrayList<Transaction> creditAccountTransactionList = mController.getTransactions(creditAccount.getId());

	Assert.assertEquals(2, debitAccountTransactionList.size());
	Assert.assertEquals(2, creditAccountTransactionList.size());
    }

    public void testSetSelectedAccount() throws Throwable {

	Account account = new BankAccount("selected", 120d);
	account.setId(18);
	mController.setSelectedAccount(account);
	Account result = mController.getSelectedAccount();

	Assert.assertEquals(account.getId(), result.getId());
    }

    public void testGetShortAccountTypeName() throws Throwable {

	String bankShort = mController.getShortAccountTypeName(BankAccount.class.toString());
	String expenseShort = mController.getShortAccountTypeName(ExpenseAccount.class.toString());

	Assert.assertEquals("Bank", bankShort);
	Assert.assertEquals("Expense", expenseShort);
    }

    public void testGetShortAccountTypeName_Invalid() throws Throwable {

	String testString = "String";
	String badShort = mController.getShortAccountTypeName(String.class.toString());

	Assert.assertEquals(testString, badShort);
    }

    public void testGetBudget() {

	mController.createDefaultBudget();
	Budget budget = mController.getBudget(1L);

	Assert.assertTrue(budget != null);
    }

    public void testGetAccountTypes() {

	List<String> accountTypes = mController.getAccountTypes();

	Assert.assertTrue(accountTypes != null);
	Assert.assertTrue(accountTypes.size() == 5);
	Assert.assertTrue(accountTypes.get(0).equals(AssetAccount.class.toString()));
	Assert.assertTrue(accountTypes.get(1).equals(BankAccount.class.toString()));
	Assert.assertTrue(accountTypes.get(2).equals(ExpenseAccount.class.toString()));
	Assert.assertTrue(accountTypes.get(3).equals(IncomeAccount.class.toString()));
	Assert.assertTrue(accountTypes.get(4).equals(LiabilityAccount.class.toString()));
    }

    public void testGetShortAccountTypeNames() throws Throwable {

	// ArrayList<String> typeNamesList =
	// mController.getShortAccountTypeNames();

    }

    public void testInsertDailyScheduledTransaction() throws Throwable {

	// mController.setMainContext(getContext(), MONEY_TRACKER_TEST_DATABASE,
	// DatabaseManager.DATABASE_VERSION);
	//
	// ArrayList<Account> bankAccounts =
	// mController.getRootAccounts(BankAccount.class);
	// ArrayList<Account> expenseAccounts =
	// mController.getRootAccounts(ExpenseAccount.class);
	//
	// Account debitAccount = expenseAccounts.get(0);
	// Account creditAccount = bankAccounts.get(0);
	// SimpleDateFormat dateFormat = new
	// SimpleDateFormat(Constants.DATE_FORMAT);
	// String date = dateFormat.format(Calendar.getInstance().getTime());
	// String description = "This is a description";
	//
	// DailyScheduledTransaction scheduledTransaction = new
	// DailyScheduledTransaction();
	// scheduledTransaction.setAmount(100d);
	// scheduledTransaction.setDate(date);
	// scheduledTransaction.setDescription(description);
	// scheduledTransaction.setCreditAccountId(creditAccount.getId());
	// scheduledTransaction.setDebitAccountId(debitAccount.getId());
	//
	// mController.insertScheduledTransaction(scheduledTransaction);
	//
	// ScheduledTransaction result =
	// mController.getScheduledTransaction(scheduledTransaction.getId());
	//
	// Assert.assertEquals(scheduledTransaction.getCreditAccountId(),
	// result.getCreditAccountId());
	// Assert.assertEquals(scheduledTransaction.getDate(),
	// result.getDate());
	// Assert.assertEquals(scheduledTransaction.getDebitAccountId(),
	// result.getDebitAccountId());
	// Assert.assertEquals(scheduledTransaction.getDescription(),
	// result.getDescription());
	// Assert.assertEquals(scheduledTransaction.getFrequencyDays(),
	// result.getFrequencyDays());
	// Assert.assertEquals(scheduledTransaction.getFrequencyNum(),
	// result.getFrequencyNum());
    }

    public void testGetArticle() {

	// mController.register(mHandler);
	// mController.getArticle();
	//
	// int time = 0;
	// while(mNewsArticles.size() == 0 && time < 5) {
	// try {
	// Thread.sleep(1000);
	// } catch (InterruptedException e) {
	// e.printStackTrace();
	// }
	// ++time;
	// }
	//
	// Assert.assertTrue(mNewsArticles.size() == 1);
    }

    public void testGetArticles() {
	// Assert.assertTrue(false);
    }

}
