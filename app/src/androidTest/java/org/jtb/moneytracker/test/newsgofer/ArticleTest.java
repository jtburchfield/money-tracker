package org.jtb.moneytracker.test.newsgofer;

import junit.framework.Assert;
import junit.framework.TestCase;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.jtb.moneytracker.newsgofer.Article;

public class ArticleTest extends TestCase {
		
	private String mSource = "google";
	private String mTitle = "\"Japan Posts Bigger-Than-Forecast Trade Deficit for February\"";
	private String mUpdated = "\"2014-03-19T01:44:13Z\"";
	private String mCategory = "Business";
	private String mUrl = "\"http://news.google.com/news/url?sa=t&fd=R&usg=AFQjCNFLt_VCZfoeaGWjeh8reN1CtqMi4w&cid=c3a7d30bb8a4878e06b80cf16b898331&url=http://www.bloomberg.com/news/2014-03-19/japan-reports-bigger-than-forecast-trade-deficit-for-february.html\"";
	private String mImage = "\"http://t1.gstatic.com/images?q=tbn:ANd9GcQD3fPxSOTAPqXKM7Fm_HupCItpty-KO3PLDS8wIrxD6FKIoR-QDwM8w1eednq0RE_gWbrBBLC3\"";
	private String mPubDate = "\"2014-03-19\"";
			
	public void testCreateFromJSON() {
		
		JSONObject json = createJsonObject();		 
		
		Article article = Article.create(json);
		
		Assert.assertTrue(!article.getImage().equals(null));
		Assert.assertTrue(article.getSource().equals(mSource));
		Assert.assertTrue(article.getTitle().equals(mTitle.replace("\"", "")));
		Assert.assertTrue(article.getUpdated().equals(mUpdated.replace("\"", "")));
	}
	
	public void testCreateFromJSON_Null() {
		
		JSONObject json = null;
		
		Article article = Article.create(json);
		
		Assert.assertTrue(article != null);
		Assert.assertTrue(article.getTitle().equals(""));
	}
		
	public void testCreateFromJSONArray() {
		
		JSONArray json = null;
		
		try {
			json = new JSONArray("[" + createJsonObject() + "," + createJsonObject() + "]");
			
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		Article[] articles = Article.create(json);
		
		Assert.assertTrue(articles.length == 2);
	}
	
	private JSONObject createJsonObject() {
		
		JSONObject object = null;
		
		try {
			object = new JSONObject("{" +
					" \"source\": " + mSource + "," + 
					" \"category\": " + mCategory + "," +
					" \"url\": " + mUrl + "," + 
					" \"image\": " + mImage + ", " + 
					" \"title\": " + mTitle + ", " + 
					" \"pub_date\": " + mPubDate + ", " + 
					" \"updated\": " + mUpdated +
					"}");
		
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		return object;
	}
}
